/*
 *
 * Account
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';
import selectAccount from './selectors';

class Account extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={styles.antLayoutContainer}>
        {this.props.children}
      </div>
    );
  }
}

Account.propTypes = {
  children: React.PropTypes.node,
};

const mapStateToProps = selectAccount();


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);
