/*
 *
 * StepsBar
 *
 */

import React from 'react';
import styles from './styles.css';
import { Steps } from 'antd';
import { translate } from 'react-i18next';

const Step = Steps.Step;

class StepsBar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { t } = this.props;
    const steps = [{
      title: t('Client_shipment'),
      description: '',
    }, {
      title: t('Client_quotes'),
      description: '',
    }, {
      title: t('Client_order'),
      description: '',
    }].map((s, i) => <Step key={i} title={s.title} description={s.description} />);

    return (
      <div className={styles.stepsBar}>
        <Steps current={this.props.current}>{steps}</Steps>
      </div>
    );
  }
}

StepsBar.propTypes = {
  current: React.PropTypes.number,
  t: React.PropTypes.func,
};

export default translate()(StepsBar);
