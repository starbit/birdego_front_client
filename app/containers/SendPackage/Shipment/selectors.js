import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

/**
 * Direct selector to the shipment state domain
 */
const selectShipmentDomain = () => (state) => state.get('shipment');

/**
 * Other specific selectors
 */

const selectFromCountryLoading = () => createSelector(
  selectShipmentDomain(),
  (substate) => substate.get('fromCountryLoading')
);

const selectWarehouses = () => createSelector(
  selectShipmentDomain(),
  (substate) => (substate ? substate.get('warehouses') : false)
);

const selectShipmentForm = () => createSelector(
  selectShipmentDomain(),
  (substate) => substate.get('shipmentForm')
);

export {
  selectShipmentDomain,
  selectWarehouses,
  selectShipmentForm,
  selectFromCountryLoading,
};
