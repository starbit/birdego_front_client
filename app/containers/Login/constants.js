/*
 *
 * Login constants
 *
 */

export const LOGIN = 'Client/Login/LOGIN';
export const LOGIN_SUCCESS = 'Client/Login/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'Client/Login/LOGIN_ERROR';
