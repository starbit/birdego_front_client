import { createSelector } from 'reselect';

/**
 * Direct selector to the createOrder state domain
 */
const selectCreateOrderDomain = () => (state) => state.get('createOrder');

/**
 * Other specific selectors
 */
const selectAddressListOpen = () => createSelector(
  selectCreateOrderDomain(),
  (substate) => substate.get('addressListOpen')
);

const selectAddresses = () => createSelector(
  selectCreateOrderDomain(),
  (substate) => substate.get('addresses')
);

const selectAddressChosen = () => createSelector(
  selectCreateOrderDomain(),
  (substate) => substate.get('addressChosen')
);
/**
 * Default selector used by Order
 */


const selectCreateSuccess = () => createSelector(
  selectCreateOrderDomain(),
  (substate) => substate.get('createSuccess')
);

export {
  selectCreateOrderDomain,
  selectCreateSuccess,
  selectAddressListOpen,
  selectAddresses,
  selectAddressChosen,
};
