/*
 *
 * Orders constants
 *
 */

export const LOAD_ORDER_COUNTS = 'Client/Orders/LOAD_ORDER_COUNTS';
export const LOAD_ORDER_COUNTS_SUCCESS = 'Client/Orders/LOAD_ORDER_COUNTS_SUCCESS';
export const LOAD_ORDER_COUNTS_ERROR = 'Client/Orders/LOAD_ORDER_COUNTS_ERROR';

export const LOAD_ORDERS = 'Client/Orders/LOAD_ORDERS';
export const LOAD_ORDERS_SUCCESS = 'Client/Orders/LOAD_ORDERS_SUCCESS';
export const LOAD_ORDERS_ERROR = 'Client/Orders/LOAD_ORDERS_ERROR';

export const PRINT_BARCODE = 'Client/Orders/PRINT_BARCODE';

export const LOAD_ORDER_DETAIL = 'Client/Orders/LOAD_ORDER_DETAIL';
export const LOAD_ORDER_SUCCESS = 'Client/Orders/LOAD_ORDER_SUCCESS';
export const LOAD_ORDER_ERROR = 'Client/Orders/LOAD_ORDER_ERROR';

export const REMOVE_ORDER = 'Client/Orders/REMOVE_ORDER';
export const REMOVE_ORDER_SUCCESS = 'Client/Orders/REMOVE_ORDER_SUCCESS';
export const REMOVE_ORDER_ERROR = 'Client/Orders/REMOVE_ORDER_ERROR';

export const CANCEL_ORDER = 'Client/Orders/CANCEL_ORDER';
export const CANCEL_ORDER_SUCCESS = 'Client/Orders/CANCEL_ORDER_SUCCESS';
export const CANCEL_ORDER_ERROR = 'Client/Orders/CANCEL_ORDER_ERROR';

export const PAGE_LIMIT = 10;

export const SHOW_WAREHOUSE_ADDRESS_MODAL = 'Client/Orders/SHOW_WAREHOUSE_ADDRESS_MODAL';
export const HIDE_WAREHOUSE_ADDRESS_MODAL = 'Client/Orders/HIDE_WAREHOUSE_ADDRESS_MODAL';

export const LOAD_ORDER_TRACE = 'Client/OrderTrace/LOAD_ORDER_TRACE';
export const LOAD_ORDER_TRACE_SUCCESS = 'Client/OrderTrace/LOAD_ORDER_TRACE_SUCCESS';
export const LOAD_ORDER_TRACE_ERROR = 'Client/OrderTrace/LOAD_ORDER_TRACE_ERROR';
