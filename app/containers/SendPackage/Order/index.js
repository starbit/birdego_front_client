/*
 *
 * Order
 *
 */

import React from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';
import { Link, browserHistory } from 'react-router';
import lodash from 'lodash';
import {
  selectFromCountryName,
  selectToCountryName,
  selectPackages,
  selectDeliveryService,
  selectOrderPayload,
  selectToCountryIso,
} from '../selectors';
import { selectCreateSuccess, selectAddressListOpen, selectAddressChosen } from './selectors';
import styles from './styles.css';
import StepsBar from 'components/StepsBar';
import { Form, Col, Button, Row, Input, Icon, message, Popconfirm } from 'antd';
import HorizontalLine from 'components/HorizontalLine';
import {
  createOrder,
  openAddressList,
  closeAddressList,
  createAddress,
  saveAddress,
  unchooseAddress,
} from './actions';
import { translate } from 'react-i18next';
import { setStep, changeOrderForm } from '../actions';
import { loadCountries } from 'containers/App/actions';
import WarehouseAddressCard from 'containers/WarehouseAddressCard';
import Collapse from 'react-collapse';
import AddressCards from './AddressCards';

const FormItem = Form.Item;

class Order extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.setStep(3);
    this.props.loadCountries();
  }

  setFormFields = (fields) => {
    this.props.form.setFieldsValue(fields);
  }

  resetFormFields = () => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue(this.props.form.getFieldsValue());
  }

  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        this.props.createOrder(formData);
      }
    });
  };

  toggleAddressList = () => {
    if (this.props.addressListOpen) {
      this.props.closeAddressList();
    } else {
      this.props.openAddressList();
    }
  }

  confirmCreateAddress = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        this.props.createOrder(formData);
        formData.to_country_iso = this.props.toCountryIso;
        this.props.createAddress(formData);
      }
    });
  }

  notCreateAddress = () => {
    this.handleSubmit();
  }

  confirmSaveAddress = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        this.props.createOrder(formData);
        formData.to_country_iso = this.props.toCountryIso;
        formData.id = this.props.addressChosen.id;
        this.props.saveAddress(formData);
      }
    });
  }

  notSaveAddress = () => {
    this.handleSubmit();
  }

  isAddressModified = () => {
    const { first_name, last_name, email, city, county, post_code, telephone, address_line1, address_line2, address_line3 } = this.props.addressChosen;
    const addressChosen = { first_name, last_name, email, city, county, post_code, telephone, address_line1, address_line2, address_line3 };
    return !lodash.isEqual(addressChosen, this.props.form.getFieldsValue());
  }

  doNotUseAddressBook = () => {
    this.props.unchooseAddress();
    this.props.closeAddressList();
    this.resetFormFields();
  }

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;

    if (this.props.createSuccess) {
      return (
        <div className={styles.result}>
          <p className={styles.resultTitle}>{t('Client_create_order_success')} {t('Client_please_send_to_this_warehouse')}</p>
          <WarehouseAddressCard />
          <Button type="primary" className={styles.resultButton}><Link to="/client/orders">{t('Client_go_to_orders_page')}</Link></Button>
          <Button type="primary"><Link to="/client/shipment">{t('Client_send_another_package')}</Link></Button>
        </div>
      );
    }

    const packages = this.props.packages.map((p, index) => (
      <Row key={index} type="flex" >
        <Col span={2}>
          <span className={styles.label}>{t('Client_quantity')}</span>
          {p.quantity ? p.quantity.value : ''}
        </Col>
        <Col span={3}>
          <span className={styles.label}>{t('Client_weight')}</span>
          {p.weight ? p.weight.value : ''} kg
        </Col>
        <Col offset={6}>
          <span className={styles.label}>{t('Client_size')}</span>
          {p.length ? p.length.value : ''} cm
        </Col>
        <Col span="1" className={styles.times}>
          <span>&#215;</span>
        </Col>
        <Col>{p.width ? p.width.value : ''} cm</Col>
        <Col span="1" className={styles.times}>
          <span>&#215;</span>
        </Col>
        <Col>{p.depth ? p.depth.value : ''} cm</Col>
      </Row>
    ));
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    const emailProps = getFieldProps('email', {
      validate: [{
        rules: [
          { required: true, message: t('Client_hint_required') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { type: 'email', message: t('Client_hint_valid_email') },
        ],
        trigger: ['onBlur', 'onChange'],
      }],
    });

    let chooseHint = null;
    if (this.props.addressChosen.id) {
      chooseHint = (
        <p className={styles.chooseHint}>
          {t('Client_using_address_from_address_book')}<Icon type="caret-up" className={styles.chooseHintIcon} />
          <span className={styles.unchooseAddress} onClick={this.doNotUseAddressBook}>{t('Client_do_not_use_any_addresses_from_address_book')}</span>
        </p>
      );
    }

    let popconfirm = (
      <Popconfirm
        title={t('Client_save_to_address_book')}
        onConfirm={this.confirmCreateAddress}
        onCancel={this.notCreateAddress}
      >
        <Button type="primary" size="large">
          {t('Client_create_order')}
        </Button>
      </Popconfirm>
    );

    if (this.props.addressChosen.id) {
      if (this.isAddressModified()) {
        popconfirm = (
          <Popconfirm
            title={t('Client_need_update_address')}
            onConfirm={this.confirmSaveAddress}
            onCancel={this.notSaveAddress}
          >
            <Button type="primary" size="large">
              {t('Client_create_order')}
            </Button>
          </Popconfirm>
        );
      } else {
        popconfirm = (
          <Button type="primary" size="large" onClick={this.handleSubmit}>
            {t('Client_create_order')}
          </Button>
        );
      }
    }

    return (
      <div>
        <StepsBar current={2} />
        <div className={styles.order}>
          <p className={styles.title}>{t('Client_delivery_info')}</p>
          <div className={styles.box}>
            <Row>
              <Col span={10}><span className={styles.label}>{t('Client_from')}</span>{this.props.fromCountryName}</Col>
              <Col span={10} offset={1}><span className={styles.label}>{t('Client_to')}</span>{this.props.toCountryName}</Col>
            </Row>
            <HorizontalLine />
            <div>
              <p className={styles.subTitle}>{t('Client_packages')}</p>
              {packages}
            </div>
            <HorizontalLine />
            <div>
              <p className={styles.subTitle}>{t('Client_delivery_service_info')}</p>
              <Row>
                <Col span={10}>
                  <Icon type="check" className={styles.icon} />
                  {this.props.deliveryService.delivery_service_name}
                </Col>
                <Col span={6} offset={1}>
                  <Icon type="shopping-cart" className={styles.icon} />
                  {this.props.deliveryService.total_speed} {t('Client_day')}
                </Col>
                <Col offset={1}>
                  {this.props.deliveryService.currency} {this.props.deliveryService.total_fee}
                </Col>
              </Row>
              <Button
                className={styles.changeCarrierButton}
                type="primary"
                onClick={(e) => {
                  if (e && e.preventDefault) e.preventDefault();
                  browserHistory.push('/client/quotes');
                }}
              >
                {t('Client_change_a_carrier')}
              </Button>
            </div>
          </div>
          <Row type="flex" justify="space-between">
            <Col>
              <p className={styles.title}>{t('Client_consignee_info')}</p>
            </Col>
            <Col>
              <Button type="primary" onClick={this.toggleAddressList}>
                <Icon type="book" />
                {t('Client_select_from_address_book')}
              </Button>
            </Col>
          </Row>
          <Collapse isOpened={this.props.addressListOpen}>
            <AddressCards setFormFields={this.setFormFields} resetFormFields={this.resetFormFields} />
          </Collapse>
          <Row className={styles.form}>
            <Col span="16">
              {chooseHint}
              <Form horizontal>
                <Row>
                  <Col span={7}>
                    <FormItem
                      label={t('Client_first_name')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('first_name', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 20, message: t('Client_hint_invalid_length') },
                        ] })}
                      />
                    </FormItem>
                  </Col>
                  <Col span={7} offset={2}>
                    <FormItem
                      label={t('Client_last_name')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('last_name', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 20, message: t('Client_hint_invalid_length') },
                        ] })}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={7}>
                    <FormItem
                      label={t('Client_county')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('county', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 50, message: t('Client_hint_invalid_length') }] })}
                      />
                    </FormItem>
                  </Col>
                  <Col span={7} offset={2}>
                    <FormItem
                      label={t('Client_city')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('city', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 50, message: t('Client_hint_invalid_length') }] })}
                      />
                    </FormItem>
                  </Col>
                  <Col span={6} offset={2}>
                    <FormItem
                      label={t('Client_post_code')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('post_code', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 10, message: t('Client_hint_invalid_length') }] })}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <FormItem
                  label={t('Client_address_line1')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('address_line1', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
                <FormItem
                  label={t('Client_address_line2')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('address_line2', { rules: [{ type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
                <FormItem
                  label={t('Client_address_line3')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('address_line3', { rules: [{ type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
                <Row>
                  <Col span={7}>
                    <FormItem
                      label={t('Client_telephone')}
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps('telephone', { rules: [
                          { required: true, message: t('Client_hint_required') },
                          { type: 'string', max: 20, message: t('Client_hint_telephone_invalid_length') },
                        ] })}
                      />
                    </FormItem>
                  </Col>
                  <Col span={7} offset={2}>
                    <FormItem
                      label={t('Client_email')}
                      {...formItemLayout}
                    >
                      <Input
                        type="email"
                        {...emailProps}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <Button
                  className={styles.button}
                  type="ghost"
                  size="large"
                  onClick={(e) => {
                    if (e && e.preventDefault) e.preventDefault();
                    browserHistory.push('/client/quotes');
                  }}
                >
                  <Icon type="left" />
                  {t('Client_previous_step')}
                </Button>
                {popconfirm}
              </Form>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

Order.propTypes = {
  fromCountryName: React.PropTypes.string,
  toCountryName: React.PropTypes.string,
  deliveryService: React.PropTypes.object,
  packages: React.PropTypes.array,
  form: React.PropTypes.object,
  createOrder: React.PropTypes.func,
  setStep: React.PropTypes.func,
  loadCountries: React.PropTypes.func,
  t: React.PropTypes.func,
  orderCreated: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  createSuccess: React.PropTypes.bool,
  openAddressList: React.PropTypes.func,
  closeAddressList: React.PropTypes.func,
  addressListOpen: React.PropTypes.bool,
  addressChosen: React.PropTypes.object,
  createAddress: React.PropTypes.func,
  saveAddress: React.PropTypes.func,
  toCountryIso: React.PropTypes.string,
  unchooseAddress: React.PropTypes.func,
};
const mapStateToProps = createSelector(
  selectFromCountryName(),
  selectToCountryName(),
  selectDeliveryService(),
  selectPackages(),
  selectOrderPayload(),
  selectCreateSuccess(),
  selectAddressListOpen(),
  selectAddressChosen(),
  selectToCountryIso(),
  (fromCountryName, toCountryName, deliveryService, packages, orderPayload, createSuccess, addressListOpen, addressChosen, toCountryIso) => ({
    fromCountryName,
    toCountryName,
    deliveryService: deliveryService || {},
    packages: packages.toJS(),
    orderPayload,
    createSuccess,
    addressListOpen,
    addressChosen,
    toCountryIso,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    setStep: (step) => dispatch(setStep(step)),
    loadCountries: () => dispatch(loadCountries()),
    createOrder: (formData) => dispatch(createOrder(formData)),
    openAddressList: () => dispatch(openAddressList()),
    closeAddressList: () => dispatch(closeAddressList()),
    createAddress: (formData) => dispatch(createAddress(formData)),
    saveAddress: (formData) => dispatch(saveAddress(formData)),
    unchooseAddress: () => dispatch(unchooseAddress()),
    dispatch,
  };
}

const newOrder = Form.create({
  mapPropsToFields: (props) => props.orderPayload.toJS(),
  onFieldsChange: (props, fields) => {
    props.dispatch(changeOrderForm(fromJS(fields)));
  },

})(Order);

export { newOrder as Order };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newOrder));
