import expect from 'expect';
import shipmentReducer from '../reducer';
import { fromJS } from 'immutable';

describe('shipmentReducer', () => {
  it('returns the initial state', () => {
    expect(shipmentReducer(undefined, {})).toEqual(fromJS({}));
  });
});
