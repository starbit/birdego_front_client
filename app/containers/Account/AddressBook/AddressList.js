/*
 *
 * AddressList
 *
 */

import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { selectAddresses, selectAddressListLoading } from './selectors';
import styles from './styles.css';
import { Row, Col, Spin } from 'antd';
import { createSelector } from 'reselect';
import { loadAddressList, editAddress, removeAddress } from './actions';
import { Icon } from 'react-fa';

class AddressList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadAddressList();
  }

  render() {
    const { t } = this.props;
    let addresses = null;
    if (this.props.addresses.length < 1) {
      addresses = (
        <p className={styles.emptyList}>{t('Client_empty_address_list')}</p>
      );
    } else {
      addresses = this.props.addresses.map(address => (
        <Row type="flex" align="middle" key={address.id} className={styles.addressItem}>
          <Col span={2} className={styles.addressButtonWrapper}>
            <Icon name="map-marker" className={styles.addressIcon} />
          </Col>
          <Col span={18} className={styles.addressContent}>
            <p>
              <span className={styles.addressName}>{address.first_name}&nbsp;
              {address.last_name}&nbsp;
              </span>
              {address.email}&nbsp;
              {address.telephone}&nbsp;
            </p>
            <p>
              {address.to_country}&nbsp;
              {address.county}&nbsp;
              {address.city}&nbsp;
              {address.post_code}&nbsp;
            </p>
            <p>
              {address.address_line1}&nbsp;
              {address.address_line2}&nbsp;
              {address.address_line3}&nbsp;
            </p>
          </Col>
          <Col>
            <span className={styles.addressOperationIcon} onClick={() => this.props.editAddress(address)}><Icon name="pencil" className={styles.addressEditIcon} /></span>
            <span className={styles.separator} />
            <span className={styles.addressOperationIcon} onClick={() => this.props.removeAddress(address.id)}><Icon name="trash-o" className={styles.addressDeleteIcon} /></span>
          </Col>
        </Row>
      ));
    }

    return (
      <div>
        <Spin spinning={this.props.addressListLoading}>
          {addresses}
        </Spin>
      </div>
    );
  }
}

AddressList.propTypes = {
  t: React.PropTypes.func,
  form: React.PropTypes.object,
  loadAddressList: React.PropTypes.func,
  addresses: React.PropTypes.array,
  editAddress: React.PropTypes.func,
  removeAddress: React.PropTypes.func,
  addressListLoading: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectAddresses(),
  selectAddressListLoading(),
  (addresses, addressListLoading) => ({ addresses: addresses || [], addressListLoading })
);

function mapDispatchToProps(dispatch) {
  return {
    loadAddressList: () => dispatch(loadAddressList()),
    removeAddress: (id) => dispatch(removeAddress(id)),
    editAddress: (address) => dispatch(editAddress(address)),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(AddressList));
