import React from 'react';
import { connect } from 'react-redux';
import { Row, Spin, Steps, Card, Timeline, Button } from 'antd';
import { createSelector } from 'reselect';
import { selectOrderTrace, selectLoadingOrderTrace } from '../selectors';
import { loadOrderTrace } from '../actions';
import { translate } from 'react-i18next';
import TraceItem from './TraceItem';
import styles from './styles.css';
import { Link } from 'react-router';

class OrderRoute extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { params, orderTrace } = this.props;
    if (params && params.orderId && !orderTrace[params.orderId]) {
      this.props.loadOrderTrace(params.orderId);
    }
  }

  getTraceStatuses(traces) {
    // status enum for antd Steps: wait, process, success
    const statuses = {
      // preparing: 'process',
      // delivering: 'process',
      // done: 'wait',
      current: 0,
    };
    if (traces && traces.length) {
      if (!(traces[0].status === 'WAIT_RECEIVE')) {
        // statuses.preparing = 'success';
        statuses.current = 1;
      }
      if (traces[traces.length - 1].status === 'DISPATCHED') {
        // these statuses could get more complicated if the backend could return more info
        // statuses.delivering = 'success';
        // statuses.done = 'success';
        statuses.current = 2;
      }
    }

    return statuses;
  }

  createTraces(traces) {
    const { t } = this.props;
    const actualTraces = [];
    // warehouse-name
    // status
    // receive_time
    // dispatch_time
    const filteredTraces = traces; // .filter(trace => trace.receive_time);
    let waitingWarehouseAdded = false;

    filteredTraces.forEach((trace, idx) => {
      if (trace.dispatch_time) { // first, try dispatch info
        actualTraces.push(
          <Timeline.Item key={`${idx}trace_dispatch`} color="green">
            <TraceItem
              // warehouse={trace['warehouse-name']}
              message={
                t(
                  'Client_trace_timeline_warehouse_dispatched',
                  { warehouseName: trace['warehouse-name'] }
                )
              }
            />
          </Timeline.Item>
        );
      } else if (trace.receive_time) { // second, try receive info
        actualTraces.push(
          <Timeline.Item key={`${idx}trace_receive`} color="green">
            <TraceItem
              warehouse={trace['warehouse-name']}
              timeString={trace.receive_time}
              message={
                t(
                  'Client_trace_timeline_warehouse_receive',
                  { warehouseName: trace['warehouse-name'] }
                )
              }
            />
          </Timeline.Item>
        );
      } else if (!waitingWarehouseAdded) { // third, no info, try add a waiting message
        actualTraces.push(
          <Timeline.Item key={`${idx}trace_wait`} color="red">
            <TraceItem
              warehouse={trace['warehouse-name']}
              message={
                t(
                  'Client_trace_timeline_warehouse_waiting',
                  { warehouseName: trace['warehouse-name'] }
                )
              }
            />
          </Timeline.Item>
        );

        waitingWarehouseAdded = true;
      }
    });

    // actualTraces.reverse();

    return actualTraces;
  }

  render() {
    const { params, orderTrace, loadingTrace, t } = this.props;
    const traces = orderTrace[params.orderId] || [];
    const statuses = this.getTraceStatuses(traces);

    return (
      <div className={styles.traceContainer}>
        <Spin spinning={loadingTrace}>
          <Row className={styles.traceSteps}>
            <Steps current={statuses.current}>
              <Steps.Step title={t('Client_trace_steps_pending')} icon="environment-o" />
              <Steps.Step title={t('Client_trace_steps_delivering')} icon="shopping-cart" />
              <Steps.Step title={t('Client_trace_steps_done')} icon="heart-o" />
            </Steps>
          </Row>
          <Row>
            <Card bodyStyle={{ background: '#f6f6f6' }}>
              <Timeline>
              {this.createTraces(traces)}
              </Timeline>
            </Card>
          </Row>
          <Button className={styles.backButton} type="primary"><Link to="/client/orders">{t('Client_back_to_orders_page')}</Link></Button>
        </Spin>
      </div>
    );
  }
}

OrderRoute.propTypes = {
  orderTrace: React.PropTypes.object,
  loadingTrace: React.PropTypes.bool,
  params: React.PropTypes.object,
  loadOrderTrace: React.PropTypes.func,
  t: React.PropTypes.func.isRequired,
};

const mapStateToProps = createSelector(
  selectOrderTrace(),
  selectLoadingOrderTrace(),
  (orderTrace, loadingTrace) => ({
    orderTrace: orderTrace.toJS(),
    loadingTrace,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadOrderTrace: (orderId) => dispatch(loadOrderTrace(orderId)),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(OrderRoute));
