import React from 'react';
import { Row, Col } from 'antd';

const TraceItem = ({ warehouse, timeString, message }) => (
  <Row>
    <Col span={4}>{warehouse}</Col>
    <Col span={6}>{timeString}</Col>
    <Col span={14}>
      {message}
    </Col>
  </Row>
);

TraceItem.propTypes = {
  warehouse: React.PropTypes.string,
  timeString: React.PropTypes.string,
  message: React.PropTypes.string,
};

export default TraceItem;
