/*
 *
 * Signup
 *
 */

import React from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import styles from './styles.css';
import {
  Form,
  Input,
  Row,
  Button,
  Card,
  Select,
  Col,
  Icon,
  message,
  Spin,
} from 'antd';
import request from 'utils/request';
import { translate } from 'react-i18next';
import {
  signup,
  loadCaptcha,
  loadCompanies,
  hidePasswordBar,
  hideRePasswordBar,
  setPasswordStrength,
  setRePasswordStrength,
} from './actions';
import {
  selectCaptchaUrl,
  selectCompanies,
  selectLoadingCompanies,
  selectPasswordBarShow,
  selectPasswordStrength,
  selectRePasswordStrength,
  selectRePasswordBarShow,
} from './selectors';
import classNames from 'classnames/bind';

const FormItem = Form.Item;
const Option = Select.Option;
const cx = classNames.bind(styles);

class Signup extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadCaptcha();
    this.props.loadCompanies();
  }

  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFieldsAndScroll((error, value) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        delete formData.rePassword;
        this.props.handleSubmit(formData);
      }
    });
  };

  checkPass = (rule, value, callback) => {
    const { validateFields, getFieldValue } = this.props.form;
    this.getPassStrength(value, 'password');

    if (value && getFieldValue('rePassword')) {
      validateFields(['rePassword'], { force: true });
    }
    callback();
  };

  checkPass2 = (rule, value, callback) => {
    const checkPass2Timeout = this.checkPass2Timeout;
    this.getPassStrength(value, 'rePassword');

    if (checkPass2Timeout) {
      clearTimeout(checkPass2Timeout);
    }

    this.checkPass2Timeout = setTimeout(() => {
      const { t } = this.props;
      const { getFieldValue } = this.props.form;
      if (value && value !== getFieldValue('password')) {
        callback(t('Client_hint_passwords_not_match'));
      } else {
        callback();
      }
    }, 500);
  };

  getPassStrength = (value, type) => {
    if (value) {
      let strength;
      if (value.length < 6) {
        return;
      }
      if (/^\d+$/.test(value)) {
        strength = 'L';
      } else if (/^[0-9a-zA-Z]+$/.test(value)) {
        strength = 'M';
      } else {
        strength = 'H';
      }
      if (type === 'password') {
        this.props.setPasswordStrength(strength);
      } else {
        this.props.setRePasswordStrength(strength);
      }
    } else if (type === 'password') {
      this.props.hidePasswordBar();
    } else {
      this.props.hideRePasswordBar();
    }
  }

  renderPassStrengthBar = (type) => {
    const { t } = this.props;
    const strength = type === 'password' ? this.props.passwordStrength : this.props.rePasswordStrength;
    const classSet = cx({
      pwdStrength: true,
      pwdStrengthLow: strength === 'L',
      pwdStrengthMedium: strength === 'M',
      pwdStrengthHigh: strength === 'H',
    });
    const level = {
      L: t('Client_low'),
      M: t('Client_medium'),
      H: t('Client_high'),
    };

    return (
      <div>
        <ul className={classSet}>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem1}`}></li>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem2}`}></li>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem3}`}></li>
          <span className="form-text">
            {level[strength]}
          </span>
        </ul>
      </div>
    );
  }

  checkUsernameValid = (rule, value, callback) => {
    const { t } = this.props;
    if (value && !/^[a-zA-Z0-9]+(-?[a-zA-Z0-9])+$/.test(value)) {
      callback(t('Client_hint_invalid_username'));
    } else {
      callback();
    }
  };

  checkUsernameExist = (rule, value, callback) => {
    const { t } = this.props;
    if (value) {
      request(`/public/check-username?username=${value}`).then((response) => {
        if (response.success) {
          if (response.data.exist) {
            callback(t('Client_username_exist'));
          } else {
            callback();
          }
        } else {
          console.log('check username failed');
        }
      }, () => {
        console.log('check username failed');
      });
    } else {
      callback();
    }
  };

  checkEmailExist = (rule, value, callback) => {
    const { t } = this.props;
    if (value) {
      request(`/public/check-email?email=${value}`).then((response) => {
        if (response.success) {
          if (response.data.exist) {
            callback(t('Client_email_exist'));
          } else {
            callback();
          }
        } else {
          console.log('check email failed');
        }
      }, () => {
        console.log('check email failed');
      });
    } else {
      callback();
    }
  };

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    const companies = this.props.companies.map(company => <Option key={company.id} value={company.id}>{company.site_name}</Option>);

    const emailProps = getFieldProps('email', {
      validate: [{
        rules: [
          { required: true, message: t('Client_hint_required') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { type: 'email', message: t('Client_hint_valid_email') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { validator: this.checkEmailExist },
        ],
        trigger: 'onBlur',
      }],
    });

    const passwordProps = getFieldProps('password', {
      validate: [{
        rules: [
          { type: 'string', min: 6, max: 16, message: t('Client_hint_password_invalid_length') },
        ],
        trigger: ['onChange', 'onBlur'],
      }, {
        rules: [
          { required: true, whitespace: true, message: t('Client_hint_required') },
          { validator: this.checkPass },
        ],
        trigger: ['onBlur', 'onChange'],
      }],
    });

    const rePasswordProps = getFieldProps('rePassword', {
      validate: [{
        rules: [
          { required: true, whitespace: true, message: t('Client_hint_required') },
          { validator: this.checkPass2 },
        ],
        trigger: ['onChange', 'onBlur'],
      }],
    });

    const usernameProps = getFieldProps('username', {
      validate: [{
        rules: [
          { required: true, whitespace: false, message: t('Client_hint_required') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { type: 'string', max: 50, min: 3, message: t('Client_hint_invalid_username_length') },
          { validator: this.checkUsernameValid },
        ],
        trigger: ['onChange', 'onBlur'],
      }, {
        rules: [
          { validator: this.checkUsernameExist },
        ],
        trigger: 'onBlur',
      }],
    });

    return (
      <div className={styles.signup}>
        <Row type="flex" justify="center">
          <Col span="12">
            <Card title={t('Client_signup')}>
              <Form horizontal onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label={<span className="ant-form-item-required">{t('Client_site')}</span>}
                >
                  <Spin spinning={this.props.loadingCompanies} size="small">
                    <Select
                      {...getFieldProps('company_id', { rules: [{ required: true, message: t('Client_hint_required') }] })}
                      size="large"
                      showSearch
                      optionFilterProp="children"
                      placeholder={t('Client_select_a_site')}
                    >
                    {companies}
                    </Select>
                  </Spin>
                </FormItem>

                <FormItem
                  hasFeedback
                  {...formItemLayout}
                  label={t('Client_username')}
                >
                  <Input
                    {...usernameProps}
                  />
                </FormItem>

                <Row type="flex" justify="space-between">
                  <Col span="16">
                    <FormItem
                      {...formItemLayout}
                      hasFeedback
                      label={t('Client_password')}
                    >
                      <Input
                        {...passwordProps}
                        type="password"
                        autoComplete="off"
                      />
                    </FormItem>
                  </Col>
                  <Col className={styles.passwordBar}>
                    {this.props.passwordBarShow ? this.renderPassStrengthBar('password') : null}
                  </Col>
                </Row>

                <Row type="flex" justify="space-between">
                  <Col span="16">
                    <FormItem
                      {...formItemLayout}
                      hasFeedback
                      label={t('Client_confirm_password')}
                    >
                      <Input
                        {...rePasswordProps}
                        type="password"
                        autoComplete="off"
                      />
                    </FormItem>
                  </Col>
                  <Col className={styles.passwordBar}>
                    {this.props.rePasswordBarShow ? this.renderPassStrengthBar('rePassword') : null}
                  </Col>
                </Row>

                <FormItem
                  hasFeedback
                  {...formItemLayout}
                  label={t('Client_nickname')}
                >
                  <Input
                    {...getFieldProps('name', { rules: [
                      { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                    ] })}
                  />
                </FormItem>

                <FormItem
                  hasFeedback
                  {...formItemLayout}
                  label={t('Client_email')}
                >
                  <Input
                    type="email"
                    {...emailProps}
                  />
                </FormItem>

                <Row align="bottom" type="flex">
                  <Col span={8}>
                    <FormItem
                      {...formItemLayout}
                      label={t('Client_captcha')}
                    >
                      <Input
                        {...getFieldProps('captcha', { rules: [{ required: true, message: t('Client_hint_required') }] })}
                      />
                    </FormItem>
                  </Col>
                  <Col span={6} offset={1}>
                    <Spin spinning={this.props.captchaUrl === ''} size="small">
                      <img alt={t('Client_captcha_image')} src={this.props.captchaUrl} className={styles.captcha} />
                    </Spin>
                  </Col>
                  <Col span={2}>
                    <Icon type="reload" onClick={this.props.loadCaptcha} className={styles.reloadIcon} />
                  </Col>
                </Row>

                <FormItem>
                  <Button type="primary" htmlType="submit">{t('Client_signup')}</Button>
                </FormItem>

              </Form>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

Signup.propTypes = {
  form: React.PropTypes.object,
  handleSubmit: React.PropTypes.func,
  loadCaptcha: React.PropTypes.func,
  captchaUrl: React.PropTypes.string,
  companies: React.PropTypes.array,
  loadCompanies: React.PropTypes.func,
  t: React.PropTypes.func,
  loadingCompanies: React.PropTypes.bool,
  passwordBarShow: React.PropTypes.bool,
  rePasswordBarShow: React.PropTypes.bool,
  passwordStrength: React.PropTypes.string,
  rePasswordStrength: React.PropTypes.string,
  setPasswordStrength: React.PropTypes.func,
  setRePasswordStrength: React.PropTypes.func,
  hidePasswordBar: React.PropTypes.func,
  hideRePasswordBar: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCaptchaUrl(),
  selectCompanies(),
  selectLoadingCompanies(),
  selectPasswordBarShow(),
  selectRePasswordBarShow(),
  selectPasswordStrength(),
  selectRePasswordStrength(),
  (captchaUrl, companies, loadingCompanies, passwordBarShow, rePasswordBarShow, passwordStrength, rePasswordStrength) => ({
    captchaUrl,
    companies: companies || [],
    loadingCompanies,
    passwordBarShow,
    rePasswordBarShow,
    passwordStrength,
    rePasswordStrength,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (formData) => {
      dispatch(signup(formData));
    },
    loadCompanies: () => dispatch(loadCompanies()),
    loadCaptcha: () => dispatch(loadCaptcha()),
    hidePasswordBar: () => dispatch(hidePasswordBar()),
    hideRePasswordBar: () => dispatch(hideRePasswordBar()),
    setPasswordStrength: (strength) => dispatch(setPasswordStrength(strength)),
    setRePasswordStrength: (strength) => dispatch(setRePasswordStrength(strength)),
    dispatch,
  };
}

const newSignup = Form.create({})(Signup);

export { newSignup as Signup };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newSignup));
