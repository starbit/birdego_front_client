/*
 *
 * ChangePassword constants
 *
 */

export const CHANGE_PASSWORD = 'Client/Account/ChangePassword/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'Client/Account/ChangePassword/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_ERROR = 'Client/Account/ChangePassword/CHANGE_PASSWORD_ERROR';

export const HIDE_PASSWORD_BAR = 'Client/Account/ChangePassword/HIDE_PASSWORD_BAR';
export const HIDE_REPASSWORD_BAR = 'Client/Account/ChangePassword/HIDE_REPASSWORD_BAR';

export const SET_PASSWORD_STRENGTH = 'Client/Account/ChangePassword/SET_PASSWORD_STRENGTH';
export const SET_REPASSWORD_STRENGTH = 'Client/Account/ChangePassword/SET_REPASSWORD_STRENGTH';
