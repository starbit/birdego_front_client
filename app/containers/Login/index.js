/*
 *
 * Login
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';
import { Form, Input, Row, Button, Card, Col } from 'antd';
import { translate } from 'react-i18next';
import { login } from './actions';
import { Link } from 'react-router';
import selectLogin from './selectors';

const FormItem = Form.Item;

class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (evt) => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    const formData = this.props.form.getFieldsValue();
    this.props.handleSubmit(formData);
  };

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    return (
      <div className={styles.login}>
        <Row type="flex" justify="center">
          <Card title={t('Client_login')} className={styles.card}>
            <Form horizontal onSubmit={this.handleSubmit}>
              <FormItem
                {...formItemLayout}
                label={t('Client_username')}
              >
                <Input
                  {...getFieldProps('username')}
                />
              </FormItem>
              <FormItem
                {...formItemLayout}
                label={t('Client_password')}
              >
                <Input
                  type="password"
                  {...getFieldProps('password')}
                />
              </FormItem>
              <FormItem>
                <Row justify="space-between" type="flex">
                  <Col>
                    <Button type="primary" htmlType="submit">{t('Client_login')}</Button>
                  </Col>
                  <Col>
                    <Link to="/client/signup">{t('Client_go_to_signup')}</Link>
                  </Col>
                </Row>
              </FormItem>
            </Form>
          </Card>
        </Row>
      </div>
    );
  }
}

Login.propTypes = {
  form: React.PropTypes.object,
  handleSubmit: React.PropTypes.func,
  t: React.PropTypes.func,
};

const mapStateToProps = selectLogin();

function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (formData) => {
      dispatch(login(formData));
    },
    dispatch,
  };
}

const newLogin = Form.create({})(Login);

export { newLogin as Login };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newLogin));

