import React from 'react';
import { Row, Col, Button } from 'antd';
import { translate } from 'react-i18next';
import styles from './styles.css';

/**
 * OrderCollapseContent is a part of the Collapse in OrderList.
 * It displays order detail and supports these actions:
 *   1. show warehouse address
 *   2. delete order
 *   3. cancel order
 */
class OrderCollapseContent extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { detail, pagination } = this.props;
    const { removeOrder, cancelOrder, t, printBarcode } = this.props;
    const { showWarehouseAddress, showRemove, showCancel } = this.props;

    return (
      <div className={styles.content}>
        <Row type="flex" align="middle">
          <Col span={20} className={styles.contentCol}>
            <Row>
              <Col span={4} className={styles.label}>
              {t('Client_consignee')}
              </Col>
              <Col className={styles.text}>
              {detail.first_name} {detail.last_name}
              </Col>
            </Row>
            <Row>
              <Col span={4} className={styles.label}>
                {t('Client_email')}
              </Col>
              <Col span={8} className={styles.text}>
                {detail.email}
              </Col>
              <Col span={4} className={styles.label}>
                {t('Client_telephone')}
              </Col>
              <Col span={8} className={styles.text}>
                {detail.telephone}
              </Col>
            </Row>
            <Row>
              <Col span={4} className={styles.label}>
                <span>{t('Client_county')} {t('Client_city')}</span>
              </Col>
              <Col span={8} className={styles.text}>
                {detail.county} {detail.city}
              </Col>
              <Col span={4} className={styles.label}>
                <span>{t('Client_post_code')}</span>
              </Col>
              <Col span={8} className={styles.text}>
                {detail.post_code}
              </Col>
            </Row>
            <Row>
              <Col span={4} className={styles.label}>
                <span>{t('Client_address')}</span>
              </Col>
              <Col className={styles.text}>
                {detail.address_line1} {detail.address_line2} {detail.address_line3}
              </Col>
            </Row>
            <Row>
              <Col span={4} className={styles.label}>
                {t('Client_total_delivery_fee')}
              </Col>
              <Col span={8} className={styles.text}>
                {detail['company-currency_code']} {detail.total_delivery_fee}
              </Col>
              <Col span={4} className={styles.label}>
                <span>{t('Client_create_time')}</span>
              </Col>
              <Col span={8} className={styles.text}>
                {detail.create_time}
              </Col>
            </Row>
          </Col>
          <Col className={styles.operationCol} span={4}>
            <p>
              <Button
                type="primary"
                className={styles.panelButton}
                onClick={() => printBarcode(detail.id)}
              >
                {t('Client_print_package_barcode')}
              </Button>
            </p>
            <p className={`${styles.operation} ${styles.warehouseOperation}`} onClick={() => showWarehouseAddress(detail.id)}>
              {t('Client_view_warehouse_address')}
            </p>
            {
              showCancel ? (
                <p className={`${styles.operation} ${styles.cancelOperation}`} onClick={() => cancelOrder(pagination, detail)}>
                  {t('Client_cancel_order')}
                </p>
              ) : null
            }
            {
              showRemove ? (
                <p className={`${styles.operation} ${styles.deleteOperation}`} onClick={() => removeOrder(pagination, detail)}>
                  {t('Client_delete_order')}
                </p>
              ) : null
            }
          </Col>
        </Row>
      </div>
    );
  }
}

OrderCollapseContent.propTypes = {
  detail: React.PropTypes.object.isRequired,
  pagination: React.PropTypes.object.isRequired,
  showWarehouseAddress: React.PropTypes.func.isRequired,
  showRemove: React.PropTypes.bool,
  showCancel: React.PropTypes.bool,
  removeOrder: React.PropTypes.func.isRequired,
  cancelOrder: React.PropTypes.func.isRequired,
  t: React.PropTypes.func.isRequired,
  printBarcode: React.PropTypes.func.isRequired,
};

export default translate()(OrderCollapseContent);
