/*
 *
 * WarehouseAddressCard
 *
 */

import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import styles from './styles.css';
import { loadWarehouseAddress } from 'containers/App/actions';
import {
  selectWarehouseAddress,
  selectLoadingWarehouseAddress,
} from 'containers/App/selectors';
import { createSelector } from 'reselect';
import { Card, Row, Col, Spin } from 'antd';

export class WarehouseAddressCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { t, warehouseAddress, loadingWarehouseAddress } = this.props;
    return (
      <Spin spinning={loadingWarehouseAddress}>
        <Card className={styles.card}>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_contact')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.contact}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_address')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.address_line1} {warehouseAddress.address_line2} {warehouseAddress.address_line3}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_city')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.city}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_county')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.county}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_country_or_area')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress['country-name']}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_post_code')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.post_code}</Col>
          </Row>
          <Row>
            <Col span={3} className={styles.label}>{t('Client_telephone')}</Col>
            <Col span={16} offset={2} className={styles.content}>{warehouseAddress.telephone}</Col>
          </Row>
        </Card>
      </Spin>
    );
  }
}

WarehouseAddressCard.propTypes = {
  t: React.PropTypes.func,
  orderId: React.PropTypes.string,
  loadWarehouseAddress: React.PropTypes.func,
  warehouseAddress: React.PropTypes.object,
  loadingWarehouseAddress: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectLoadingWarehouseAddress(),
  selectWarehouseAddress(),
  (loadingWarehouseAddress, warehouseAddress) => ({
    loadingWarehouseAddress,
    warehouseAddress: warehouseAddress || {},
  })
);

function mapDispatchToProps(dispatch) {
  return {
    loadWarehouseAddress: (orderId) => dispatch(loadWarehouseAddress(orderId)),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(WarehouseAddressCard));
