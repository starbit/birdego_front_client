import React from 'react';
import { Row, Col } from 'antd';
// import { translate } from 'react-i18next';
import styles from './styles.css';
import OrderPopover from '../OrderPopover';
import { Icon } from 'react-fa';

/**
 * OrderCollapseHeader is a part of the Collapse in OrderList.
 * It displays order info and supports printing barcode.
 */
class OrderCollapseHeader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { order, toggle, showTrace, tracePlaceholder } = this.props;
    let traceInfo = null;

    if (showTrace) {
      traceInfo = <OrderPopover order={order} />;
    } else if (tracePlaceholder) {
      traceInfo = <p className={styles.tracePlaceholder}>{tracePlaceholder}</p>;
    }

    return (
      <Row type="flex" align="middle" onClick={toggle} className={styles.panelHeader}>
        <Col span={4} className={styles.orderTime}>{order.create_time}</Col>
        <Col span={4}>{order.from_country}</Col>
        <Col span={2}>
          <Icon name="long-arrow-right" />
        </Col>
        <Col span={4}>{order.to_country}</Col>
        <Col span={7} className={styles.consignee}>{`${order.first_name} ${order.last_name}`}</Col>
        <Col span={3}>
          {traceInfo}
        </Col>
      </Row>
    );
  }
}

OrderCollapseHeader.propTypes = {
  order: React.PropTypes.object.isRequired,
  toggle: React.PropTypes.func.isRequired,
  showTrace: React.PropTypes.bool,
  tracePlaceholder: React.PropTypes.string,
};

export default OrderCollapseHeader;
