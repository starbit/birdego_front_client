/*
 *
 * Signup reducer
 *
 */

import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  DISPLAY_CAPTCHA,
  LOAD_COMPANIES,
  LOAD_COMPANIES_SUCCESS,
  LOAD_COMPANIES_ERROR,
  HIDE_PASSWORD_BAR,
  HIDE_REPASSWORD_BAR,
  SET_REPASSWORD_STRENGTH,
  SET_PASSWORD_STRENGTH,
} from './constants';
import {
  CHANGE_LANGUAGE,
  CHANGE_LANGUAGE_ERROR,
} from 'containers/App/constants';

const initialState = fromJS({
  captchaUrl: '',
  companies: false,
  loadingCompanies: false,
  passwordStrength: 'L',
  rePasswordStrength: 'L',
  passwordBarShow: false,
  rePasswordBarShow: false,
});

function signupReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return state.set('loadingCompanies', true);
    case CHANGE_LANGUAGE_ERROR:
      return state.set('loadingCompanies', false);
    case LOAD_COMPANIES:
      return state.set('loadingCompanies', true);
    case LOAD_COMPANIES_SUCCESS:
      return state
        .set('companies', action.companies)
        .set('loadingCompanies', false);
    case LOAD_COMPANIES_ERROR:
      return state
        .set('companies', false)
        .set('loadingCompanies', false);
    case DISPLAY_CAPTCHA:
      return state.set('captchaUrl', action.url);
    case HIDE_PASSWORD_BAR:
      return state.set('passwordBarShow', false);
    case HIDE_REPASSWORD_BAR:
      return state.set('rePasswordBarShow', false);
    case SET_PASSWORD_STRENGTH:
      return state
        .set('passwordStrength', action.strength)
        .set('passwordBarShow', true);
    case SET_REPASSWORD_STRENGTH:
      return state
        .set('rePasswordStrength', action.strength)
        .set('rePasswordBarShow', true);
    case LOCATION_CHANGE:
      state = initialState; // eslint-disable-line
      return state;
    default:
      return state;
  }
}

export default signupReducer;
