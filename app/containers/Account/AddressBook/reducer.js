/*
 *
 * AddressBook reducer
 *
 */

import { fromJS } from 'immutable';
import {
  OPEN_NEW_ADDRESS_FORM,
  CLOSE_NEW_ADDRESS_FORM,
  LOAD_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_SUCCESS,
  LOAD_ADDRESS_LIST_ERROR,
  CREATE_ADDRESS_SUCCESS,
  EDIT_ADDRESS,
  SAVE_ADDRESS_SUCCESS,
  RESET_ADDRESS_EDITING,
} from './constants';

const initialState = fromJS({
  newAddressFormOpen: false,
  addressEditing: {},
  addresses: false,
  addressListLoading: false,
});

function addressBookReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_NEW_ADDRESS_FORM:
      return state.set('newAddressFormOpen', true);
    case CLOSE_NEW_ADDRESS_FORM:
      return state.set('newAddressFormOpen', false);
    case LOAD_ADDRESS_LIST_SUCCESS:
      return state
        .set('addresses', action.addresses)
        .set('addressListLoading', false);
    case LOAD_ADDRESS_LIST_ERROR:
      return state.set('addressListLoading', false);
    case LOAD_ADDRESS_LIST:
      return state.set('addressListLoading', true);
    case CREATE_ADDRESS_SUCCESS:
      return state.set('newAddressFormOpen', false);
    case EDIT_ADDRESS:
      return state
        .set('newAddressFormOpen', true)
        .set('addressEditing', action.address);
    case SAVE_ADDRESS_SUCCESS:
      return state
        .set('newAddressFormOpen', false)
        .set('addressEditing', {});
    case RESET_ADDRESS_EDITING:
      return state.set('addressEditing', {});
    default:
      return state;
  }
}

export default addressBookReducer;
