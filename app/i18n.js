import i18n from 'i18next';
import localeResources from './locale';

i18n.init({
  lng: localStorage.getItem('clientLang') || 'en_GB',
  resources: localeResources,
  fallbackLng: 'en_GB',
  interpolation: {
    escapeValue: false, // not needed for react!!
  },
});

export default i18n;
