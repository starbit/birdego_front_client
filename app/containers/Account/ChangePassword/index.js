/*
 *
 * ChangePassword
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';
import { Form, Row, Card, Input, Button, message, Col } from 'antd';
import {
  selectPasswordBarShow,
  selectPasswordStrength,
  selectRePasswordStrength,
  selectRePasswordBarShow,
} from './selectors';
import Menu from 'components/Menu';
import {
  changePassword,
  hidePasswordBar,
  hideRePasswordBar,
  setPasswordStrength,
  setRePasswordStrength,
} from './actions';
import { translate } from 'react-i18next';
import { createSelector } from 'reselect';
import classNames from 'classnames/bind';

const FormItem = Form.Item;
const cx = classNames.bind(styles);

class ChangePassword extends React.Component { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error, value) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const newPassword = this.props.form.getFieldValue('password');
        this.props.handleSubmit(newPassword);
        this.props.form.resetFields();
      }
    });
  };

  checkPass = (rule, value, callback) => {
    const { validateFields, getFieldValue } = this.props.form;
    this.getPassStrength(value, 'password');

    if (value && getFieldValue('rePassword')) {
      validateFields(['rePassword'], { force: true });
    }
    callback();
  };

  checkPass2 = (rule, value, callback) => {
    const checkPass2Timeout = this.checkPass2Timeout;
    this.getPassStrength(value, 'rePassword');

    if (checkPass2Timeout) {
      clearTimeout(checkPass2Timeout);
    }

    this.checkPass2Timeout = setTimeout(() => {
      const { t } = this.props;
      const { getFieldValue } = this.props.form;
      if (value && value !== getFieldValue('password')) {
        callback(t('Client_hint_passwords_not_match'));
      } else {
        callback();
      }
    }, 500);
  };

  getPassStrength = (value, type) => {
    if (value) {
      let strength;
      if (value.length < 6) {
        return;
      }
      if (/^\d+$/.test(value)) {
        strength = 'L';
      } else if (/^[0-9a-zA-Z]+$/.test(value)) {
        strength = 'M';
      } else {
        strength = 'H';
      }
      if (type === 'password') {
        this.props.setPasswordStrength(strength);
      } else {
        this.props.setRePasswordStrength(strength);
      }
    } else if (type === 'password') {
      this.props.hidePasswordBar();
    } else {
      this.props.hideRePasswordBar();
    }
  }

  renderPassStrengthBar = (type) => {
    const { t } = this.props;
    const strength = type === 'password' ? this.props.passwordStrength : this.props.rePasswordStrength;
    const classSet = cx({
      pwdStrength: true,
      pwdStrengthLow: strength === 'L',
      pwdStrengthMedium: strength === 'M',
      pwdStrengthHigh: strength === 'H',
    });
    const level = {
      L: t('Client_low'),
      M: t('Client_medium'),
      H: t('Client_high'),
    };

    return (
      <div>
        <ul className={classSet}>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem1}`}></li>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem2}`}></li>
          <li className={`${styles.pwdStrengthItem} ${styles.pwdStrengthItem3}`}></li>
          <span className="form-text">
            {level[strength]}
          </span>
        </ul>
      </div>
    );
  }

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };

    const passwordProps = getFieldProps('password', {
      validate: [{
        rules: [
          { type: 'string', min: 6, max: 16, message: t('Client_hint_password_invalid_length') },
        ],
        trigger: ['onChange', 'onBlur'],
      }, {
        rules: [
          { required: true, whitespace: true, message: t('Client_hint_required') },
          { validator: this.checkPass },
        ],
        trigger: ['onBlur', 'onChange'],
      }],
    });

    const rePasswordProps = getFieldProps('rePassword', {
      validate: [{
        rules: [
          { required: true, whitespace: true, message: t('Client_hint_required') },
          { validator: this.checkPass2 },
        ],
        trigger: ['onChange', 'onBlur'],
      }],
    });

    return (
      <div>
        <aside className={styles.antLayoutSider}>
          <Menu selected="2" />
        </aside>
        <div className={styles.antLayoutContent}>
          <Row type="flex" justify="center" className={styles.cardWrapper}>
            <Card title={t('Client_change_password')} className={styles.card}>
              <Form horizontal onSubmit={this.handleSubmit}>
                <Row>
                  <Col span="16">
                    <FormItem
                      {...formItemLayout}
                      hasFeedback
                      label={t('Client_new_password')}
                    >
                      <Input
                        {...passwordProps}
                        type="password"
                        autoComplete="off"
                      />
                    </FormItem>
                  </Col>
                  <Col span="7" offset="1">
                    {this.props.passwordBarShow ? this.renderPassStrengthBar('password') : null}
                  </Col>
                </Row>
                <Row>
                  <Col span="16">
                    <FormItem
                      {...formItemLayout}
                      hasFeedback
                      label={t('Client_confirm_new_password')}
                    >
                      <Input
                        {...rePasswordProps}
                        type="password"
                        autoComplete="off"
                      />
                    </FormItem>
                  </Col>
                  <Col span="7" offset="1">
                    {this.props.rePasswordBarShow ? this.renderPassStrengthBar('rePassword') : null}
                  </Col>
                </Row>
                <Row type="flex" justify="start">
                  <Col span={16}>
                    <Col span={16} offset={8}>
                      <FormItem>
                        <Button type="primary" htmlType="submit">{t('Client_save_change')}</Button>
                      </FormItem>
                    </Col>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Row>
        </div>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  form: React.PropTypes.object,
  t: React.PropTypes.func,
  passwordBarShow: React.PropTypes.bool,
  rePasswordBarShow: React.PropTypes.bool,
  passwordStrength: React.PropTypes.string,
  rePasswordStrength: React.PropTypes.string,
  setPasswordStrength: React.PropTypes.func,
  setRePasswordStrength: React.PropTypes.func,
  hidePasswordBar: React.PropTypes.func,
  hideRePasswordBar: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectPasswordBarShow(),
  selectRePasswordBarShow(),
  selectPasswordStrength(),
  selectRePasswordStrength(),
  (passwordBarShow, rePasswordBarShow, passwordStrength, rePasswordStrength) => ({ passwordBarShow, rePasswordBarShow, passwordStrength, rePasswordStrength })
);

function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (newPassword) => dispatch(changePassword(newPassword)),
    hidePasswordBar: () => dispatch(hidePasswordBar()),
    hideRePasswordBar: () => dispatch(hideRePasswordBar()),
    setPasswordStrength: (strength) => dispatch(setPasswordStrength(strength)),
    setRePasswordStrength: (strength) => dispatch(setRePasswordStrength(strength)),
    dispatch,
  };
}

const newChangePassword = Form.create({})(ChangePassword);

export { newChangePassword as ChangePassword };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newChangePassword));
