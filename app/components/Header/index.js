/*
 *
 * Header
 *
 */

import React from 'react';
import styles from './styles.css';
import { Row, Col } from 'antd';
import UserStatus from 'containers/UserStatus';
import A from '../A';

const Header = ({ changeLanguage, currentLanguage }) => (
  <div className={styles.header}>
    <Row type="flex" justify="space-between">
      <Col className={styles.lang}>
        <A
          active={currentLanguage === 'zh_CN'}
          onClick={() => { if (currentLanguage !== 'zh_CN') changeLanguage('zh_CN'); }}
        >
          中文
        </A>
        <span className={styles.separator}>|</span>
        <A
          active={currentLanguage === 'en_GB'}
          onClick={() => { if (currentLanguage !== 'en_GB') changeLanguage('en_GB'); }}
        >
          English
        </A>
      </Col>
      <Col className={styles.account}>
        <UserStatus />
      </Col>
    </Row>
  </div>
);

Header.propTypes = {
  changeLanguage: React.PropTypes.func,
  currentLanguage: React.PropTypes.string,
};

export default Header;
