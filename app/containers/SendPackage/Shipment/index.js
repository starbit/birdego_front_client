/*
 *
 * Shipment
 *
 */

import React from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { selectWarehouses, selectFromCountryLoading } from './selectors';
import { selectFromCountryIso, selectShipmentPayload } from '../selectors';
import { selectCountries, selectLoadingCountries } from 'containers/App/selectors';
import { loadWarehouses } from './actions';
import { setStep, addPackage, removePackage, changeShipmentForm } from '../actions';
import { loadCountries } from 'containers/App/actions';
import styles from './styles.css';
import StepsBar from 'components/StepsBar';
import { Form, Input, Select, Row, Col, Alert, Button, Spin, message } from 'antd';
import { browserHistory } from 'react-router';
import { translate } from 'react-i18next';

const FormItem = Form.Item;
const Option = Select.Option;

class Shipment extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadCountries();
    this.props.loadWarehouses();
    this.props.setStep(1);
  }

  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        this.props.handleSubmit();
      }
    });
  };

  checkCountryRequired = (rule, value, callback) => {
    const { t } = this.props;
    if (!value) {
      callback(t('Client_hint_required'));
    }
    callback();
  };

  caseInsensitiveFilterOption = (inputValue, option) =>
    (option.props.children && option.props.children.toLowerCase().includes(inputValue.toLowerCase()));

  render() {
    const { t } = this.props;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    const { getFieldProps } = this.props.form;
    const fromCountries = this.props.warehouses.map(country => <Option key={country.iso} value={country.iso}>{country.printable_name}</Option>);
    const toCountries = this.props.countries.filter(country => country.iso !== this.props.fromCountryIso).map(country => <Option key={country.iso} value={country.iso}>{country.printable_name}</Option>);

    const packageCount = this.props.shipmentPayload.get('packages').count();
    const packageIds = [];
    // lame workaround for packageKeys
    for (let i = 0; i < packageCount; i++) {
      packageIds.push(i);
    }
    return (
      <div>
        <StepsBar current={0} />
        <Form horizontal className={styles.form} onSubmit={this.handleSubmit}>
          <Row>
            <Col span={17} className={styles.content}>
              <Row>
                <Col span={6}>
                  <p className={styles.label}>
                    <span className="ant-form-item-required" />
                    {t('Client_from')}
                  </p>
                  <FormItem
                    label=""
                    {...formItemLayout}
                  >
                    <Spin spinning={this.props.fromCountryLoading} size="small">
                      <Select
                        {...getFieldProps('from_country_iso', { rules: [{ validator: this.checkCountryRequired }] })}
                        labelInValue
                        size="large"
                        showSearch
                        filterOption={this.caseInsensitiveFilterOption}
                        placeholder={t('Client_select_a_country')}
                      >
                        {fromCountries}
                      </Select>
                    </Spin>
                  </FormItem>
                </Col>
                <Col span={6} offset={1}>
                  <p className={styles.label}>
                    <span className="ant-form-item-required" />
                    {t('Client_to')}
                  </p>
                  <FormItem
                    label=""
                    {...formItemLayout}
                  >
                    <Spin spinning={this.props.toCountryLoading} size="small">
                      <Select
                        {...getFieldProps('to_country_iso', { rules: [{ validator: this.checkCountryRequired }] })}
                        labelInValue
                        showSearch
                        size="large"
                        filterOption={this.caseInsensitiveFilterOption}
                        placeholder={t('Client_select_a_country')}
                      >
                        {toCountries}
                      </Select>
                    </Spin>
                  </FormItem>
                </Col>
              </Row>
            </Col>
            <Col span={4} offset={1}>
              <Alert
                message=""
                description={t('Client_hint_from_and_to')}
                type="success"
              />
            </Col>
          </Row>

          <Row type="flex" align="middle">
            <Col span={17} className={styles.content}>
              <p className={styles.label}>
                <span className="ant-form-item-required" />
                {t('Client_package_details')}
              </p>
              {packageIds.map((i) =>
                <Row key={i} type="flex" justify="space-between">
                  <Col span="3">
                    <FormItem
                      label=""
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps(`packages[${i}].quantity`, { rules: [{ required: true, message: t('Client_hint_required') }] })}
                        placeholder={t('Client_quantity')}
                      />
                    </FormItem>
                  </Col>
                  <Col span="3">
                    <FormItem
                      label=""
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps(`packages[${i}].weight`, { rules: [{ required: true, message: t('Client_hint_required') }] })}
                        placeholder={t('Client_weight')}
                        addonAfter="kg"
                      />
                    </FormItem>
                  </Col>
                  <Col span="3">
                    <FormItem
                      label=""
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps(`packages[${i}].length`, { rules: [{ required: true, message: t('Client_hint_required') }] })}
                        placeholder={t('Client_length')}
                        addonAfter="cm"
                      />
                    </FormItem>
                  </Col>
                  <Col span="1" className={styles.times}>
                    <span>&#215;</span>
                  </Col>
                  <Col span="3">
                    <FormItem
                      label=""
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps(`packages[${i}].width`, { rules: [{ required: true, message: t('Client_hint_required') }] })}
                        placeholder={t('Client_width')}
                        addonAfter="cm"
                      />
                    </FormItem>
                  </Col>
                  <Col span="1" className={styles.times}>
                    <span>&#215;</span>
                  </Col>
                  <Col span="3">
                    <FormItem
                      label=""
                      {...formItemLayout}
                    >
                      <Input
                        {...getFieldProps(`packages[${i}].depth`, { rules: [{ required: true, message: t('Client_hint_required') }] })}
                        placeholder={t('Client_height')}
                        addonAfter="cm"
                      />
                    </FormItem>
                  </Col>
                  <Col span="3">
                    {(() => {
                      if (i > 0) {
                        return <Button type="primary" className={styles.removeButton} onClick={() => this.props.removePackage(i)}>-</Button>;
                      }
                      return undefined;
                    })()}
                  </Col>
                </Row>
              )}
              <Button type="primary" onClick={this.props.addPackage}>+</Button>
            </Col>
            <Col span={4} offset={1}>
              <Alert
                message=""
                description={t('Client_hint_package_details')}
                type="success"
              />
            </Col>
          </Row>

          <Row className={styles.submitButton}>
            <Col span={15}>
              <Button type="primary" size="large" htmlType="submit">
                {t('Client_get_quotes')}
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

Shipment.propTypes = {
  loadCountries: React.PropTypes.func,
  loadWarehouses: React.PropTypes.func,
  warehouses: React.PropTypes.array,
  countries: React.PropTypes.array,
  fromCountryIso: React.PropTypes.string,
  fromCountryLoading: React.PropTypes.bool,
  toCountryLoading: React.PropTypes.bool,
  toCountry: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  handleSubmit: React.PropTypes.func,
  form: React.PropTypes.object,
  shipmentPayload: React.PropTypes.instanceOf(Immutable.Map),
  addPackage: React.PropTypes.func,
  removePackage: React.PropTypes.func,
  setStep: React.PropTypes.func,
  t: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCountries(),
  selectWarehouses(),
  selectFromCountryIso(),
  selectFromCountryLoading(),
  selectLoadingCountries(),
  selectShipmentPayload(),
  (countries, warehouses, fromCountryIso, fromCountryLoading, toCountryLoading, shipmentPayload) => ({
    countries: countries || [],
    warehouses: warehouses || [],
    fromCountryIso,
    fromCountryLoading,
    toCountryLoading,
    shipmentPayload,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    loadCountries: () => dispatch(loadCountries()),
    loadWarehouses: () => dispatch(loadWarehouses()),
    addPackage: () => dispatch(addPackage()),
    removePackage: (key) => dispatch(removePackage(key)),
    handleSubmit: () => {
      browserHistory.push('/client/quotes');
    },
    setStep: (step) => dispatch(setStep(step)),
    dispatch,
  };
}

const newShipment = Form.create({
  onFieldsChange: (props, fields) => {
    const updatedPayload = Immutable.Map({ packages: [] }).mergeDeep(props.shipmentPayload).toJS();

    // extracts changed field and put it in updatedPayload
    for (const name in fields) {
      if (name.lastIndexOf('packages') >= 0) {
        const idx = name['9'];
        const key = name.split('.')[1];
        updatedPayload.packages[idx] = updatedPayload.packages[idx] || {};
        updatedPayload.packages[idx][key] = { value: fields[name].value };
      } else {
        updatedPayload[name] = { value: fields[name].value };
      }
    }

    props.dispatch(changeShipmentForm(updatedPayload));
  },
  mapPropsToFields: (props) => {
    const map = {};

    // convert packages which is a List to 'plain' input field values
    if (props.shipmentPayload.get('packages')) {
      const packages = props.shipmentPayload.get('packages').toJS();
      for (const idx in packages) {
        const pkg = packages[idx];
        for (const key in pkg) {
          map[`packages[${idx}].${key}`] = pkg[key];
        }
      }
    }

    const fromCountryIso = props.shipmentPayload.toJS().from_country_iso;
    if (fromCountryIso && fromCountryIso.value) {
      delete fromCountryIso.value.label;
    }

    const toCountryIso = props.shipmentPayload.toJS().to_country_iso;
    if (toCountryIso && toCountryIso.value) {
      delete toCountryIso.value.label;
    }

    return {
      from_country_iso: fromCountryIso,
      to_country_iso: toCountryIso,
      ...map,
    };
  },
})(Shipment);

export { newShipment as Shipment };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newShipment));
