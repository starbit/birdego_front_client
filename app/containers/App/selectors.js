import { createSelector } from 'reselect';

/**
 * Direct selector to the global state domain
 */
const selectGlobalDomain = () => (state) => state.get('global');

/**
 * Other specific selectors
 */
const selectCurrentLanguage = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('lang')
);

const selectTrackPackageFormOpen = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('trackPackageFormOpen')
);

const selectCurrentUser = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('currentUser')
);

const selectCallbackRoute = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('callbackRoute')
);

const selectConfig = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('config')
);

const selectCountries = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('countries')
);

const selectLoadingCountries = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loadingCountries')
);

const selectRequireAuth = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('requireAuth')
);

const selectSiteName = () => createSelector(
  selectConfig(),
  (config) => config.siteName
);

const selectClientInfo = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('clientInfo')
);

const selectConfigLoadingStatus = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loadingConfig')
);

const selectLoadingWarehouseAddress = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('loadingWarehouseAddress')
);

const selectWarehouseAddress = () => createSelector(
  selectGlobalDomain(),
  (substate) => substate.get('warehouseAddress')
);

// selectLocationState expects a plain JS object for the routing state
const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectLocationState,
  selectGlobalDomain,
  selectCurrentUser,
  selectConfig,
  selectCallbackRoute,
  selectSiteName,
  selectClientInfo,
  selectConfigLoadingStatus,
  selectCurrentLanguage,
  selectRequireAuth,
  selectCountries,
  selectLoadingCountries,
  selectTrackPackageFormOpen,
  selectLoadingWarehouseAddress,
  selectWarehouseAddress,
};
