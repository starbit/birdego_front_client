/*
 *
 * Signup actions
 *
 */

import {
  SIGNUP,
  LOAD_CAPTCHA,
  DISPLAY_CAPTCHA,
  LOAD_COMPANIES,
  LOAD_COMPANIES_SUCCESS,
  LOAD_COMPANIES_ERROR,
  SIGNUP_SUCCESS,
  SIGNUP_ERROR,
  HIDE_PASSWORD_BAR,
  HIDE_REPASSWORD_BAR,
  SET_PASSWORD_STRENGTH,
  SET_REPASSWORD_STRENGTH,
} from './constants';

export function signup(formData) {
  return {
    type: SIGNUP,
    formData,
  };
}

export function loadCaptcha() {
  return {
    type: LOAD_CAPTCHA,
  };
}

export function displayCaptcha(url) {
  return {
    type: DISPLAY_CAPTCHA,
    url,
  };
}

export function signupSuccess() {
  return {
    type: SIGNUP_SUCCESS,
  };
}

export function signupError() {
  return {
    type: SIGNUP_ERROR,
  };
}

export function loadCompanies() {
  return {
    type: LOAD_COMPANIES,
  };
}

export function loadCompaniesSuccess(companies) {
  return {
    type: LOAD_COMPANIES_SUCCESS,
    companies,
  };
}

export function loadCompaniesError() {
  return {
    type: LOAD_COMPANIES_ERROR,
  };
}

export function setPasswordStrength(strength) {
  return {
    type: SET_PASSWORD_STRENGTH,
    strength,
  };
}

export function setRePasswordStrength(strength) {
  return {
    type: SET_REPASSWORD_STRENGTH,
    strength,
  };
}

export function hidePasswordBar() {
  return {
    type: HIDE_PASSWORD_BAR,
  };
}

export function hideRePasswordBar() {
  return {
    type: HIDE_REPASSWORD_BAR,
  };
}
