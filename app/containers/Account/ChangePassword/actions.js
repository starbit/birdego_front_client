/*
 *
 * ChangePassword actions
 *
 */

import {
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  HIDE_PASSWORD_BAR,
  HIDE_REPASSWORD_BAR,
  SET_PASSWORD_STRENGTH,
  SET_REPASSWORD_STRENGTH,
} from './constants';

export function changePassword(newPassword) {
  return {
    type: CHANGE_PASSWORD,
    newPassword,
  };
}

export function changePasswordSuccess() {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
  };
}

export function changePasswordError() {
  return {
    type: CHANGE_PASSWORD_ERROR,
  };
}

export function setPasswordStrength(strength) {
  return {
    type: SET_PASSWORD_STRENGTH,
    strength,
  };
}

export function setRePasswordStrength(strength) {
  return {
    type: SET_REPASSWORD_STRENGTH,
    strength,
  };
}

export function hidePasswordBar() {
  return {
    type: HIDE_PASSWORD_BAR,
  };
}

export function hideRePasswordBar() {
  return {
    type: HIDE_REPASSWORD_BAR,
  };
}
