import { take, call, put, race } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_RECORDS } from './constants';
import { loadRecordsSuccess, loadRecordsError } from './actions';
import request from 'utils/request';

// Bootstrap sagas
export default [
  getRecords,
];

export function* getRecords() {
  while (true) {
    const watcher = yield race({
      loadRecords: take(LOAD_RECORDS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/client-statement?limit=-1';

    const result = yield call(request, requestURL);

    if (result.success) {
      yield put(loadRecordsSuccess(result.data.list));
    } else {
      yield put(loadRecordsError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
