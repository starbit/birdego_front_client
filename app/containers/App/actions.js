/*
 *
 * Global actions
 *
 */

import {
  CHANGE_LANGUAGE,
  CHANGE_LANGUAGE_SUCCESS,
  CHANGE_LANGUAGE_ERROR,
  SAVE_CALLBACK_ROUTE,
  CLEAR_CALLBACK_ROUTE,
  LOAD_GLOBAL_CONFIG,
  LOAD_CONFIG_SUCCESS,
  LOAD_CONFIG_ERROR,
  LOGOUT,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  LOAD_CLIENT_INFO,
  LOAD_CLIENT_INFO_SUCCESS,
  LOAD_CLIENT_INFO_ERROR,
  SET_REQUIRE_AUTH,
  LOAD_COUNTRIES,
  LOAD_COUNTRIES_SUCCESS,
  LOAD_COUNTRIES_ERROR,
  SHOW_TRACK_PACKAGE_FORM,
  HIDE_TRACK_PACKAGE_FORM,
  LOAD_WAREHOUSE_ADDRESS,
  LOAD_WAREHOUSE_ADDRESS_SUCCESS,
  LOAD_WAREHOUSE_ADDRESS_ERROR,
} from './constants';

export function changeLanguage(lang) {
  return {
    type: CHANGE_LANGUAGE,
    lang,
  };
}

export function changeLanguageSuccess(lang) {
  return {
    type: CHANGE_LANGUAGE_SUCCESS,
    lang,
  };
}

export function changeLanguageError() {
  return {
    type: CHANGE_LANGUAGE_ERROR,
  };
}

export function showTrackPackageForm() {
  return {
    type: SHOW_TRACK_PACKAGE_FORM,
  };
}

export function hideTrackPackageForm() {
  return {
    type: HIDE_TRACK_PACKAGE_FORM,
  };
}

export function setRequireAuth(requireAuth) {
  return {
    type: SET_REQUIRE_AUTH,
    requireAuth,
  };
}

export function saveCallbackRoute(callbackRoute) {
  return {
    type: SAVE_CALLBACK_ROUTE,
    callbackRoute,
  };
}

export function clearCallbackRoute() {
  return {
    type: CLEAR_CALLBACK_ROUTE,
  };
}

export function loadGlobalConfig() {
  return {
    type: LOAD_GLOBAL_CONFIG,
  };
}

export function loadConfigSuccess(currentUser, config) {
  return {
    type: LOAD_CONFIG_SUCCESS,
    currentUser,
    config,
  };
}

export function loadConfigError() {
  return {
    type: LOAD_CONFIG_ERROR,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}

export function logoutError() {
  return {
    type: LOGOUT_ERROR,
  };
}

export function loadClientInfo(id) {
  return {
    type: LOAD_CLIENT_INFO,
    id,
  };
}

export function loadClientInfoSuccess(info) {
  return {
    type: LOAD_CLIENT_INFO_SUCCESS,
    info,
  };
}

export function loadClientInfoError() {
  return {
    type: LOAD_CLIENT_INFO_ERROR,
  };
}

export function loadCountries() {
  return {
    type: LOAD_COUNTRIES,
  };
}

export function loadCountriesSuccess(countries) {
  return {
    type: LOAD_COUNTRIES_SUCCESS,
    countries,
  };
}

export function loadCountriesError() {
  return {
    type: LOAD_COUNTRIES_ERROR,
  };
}

export function loadWarehouseAddress(orderId) {
  return {
    type: LOAD_WAREHOUSE_ADDRESS,
    orderId,
  };
}

export function loadWarehouseAddressSuccess(address) {
  return {
    type: LOAD_WAREHOUSE_ADDRESS_SUCCESS,
    address,
  };
}

export function loadWarehouseAddressError() {
  return {
    type: LOAD_WAREHOUSE_ADDRESS_ERROR,
  };
}
