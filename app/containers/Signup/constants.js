/*
 *
 * Signup constants
 *
 */

export const SIGNUP = 'Client/Signup/SIGNUP';
export const SIGNUP_SUCCESS = 'Client/Signup/SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'Client/Signup/SIGNUP_ERROR';

export const LOAD_CAPTCHA = 'Client/Signup/LOAD_CAPTCHA';
export const DISPLAY_CAPTCHA = 'Client/Signup/LOAD_CAPTCHA';

export const LOAD_COMPANIES = 'Client/Signup/LOAD_COMPANIES';
export const LOAD_COMPANIES_SUCCESS = 'Client/Signup/LOAD_COMPANIES_SUCCESS';
export const LOAD_COMPANIES_ERROR = 'Client/Signup/LOAD_COMPANIES_ERROR';

export const HIDE_PASSWORD_BAR = 'Client/Signup/HIDE_PASSWORD_BAR';
export const HIDE_REPASSWORD_BAR = 'Client/Signup/HIDE_REPASSWORD_BAR';

export const SET_PASSWORD_STRENGTH = 'Client/Signup/SET_PASSWORD_STRENGTH';
export const SET_REPASSWORD_STRENGTH = 'Client/Signup/SET_REPASSWORD_STRENGTH';
