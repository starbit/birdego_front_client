import { take, call, put, race, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOGIN } from './constants';
import { loginSuccess, loginError } from './actions';
import { loadGlobalConfig, clearCallbackRoute } from 'containers/App/actions';
import request from 'utils/request';
import { selectCallbackRoute } from 'containers/App/selectors';
import { browserHistory } from 'react-router';
import i18n from 'i18next';
import { selectLocationState } from 'containers/App/selectors';
import lodash from 'lodash';
import { message } from 'antd';

// Bootstrap sagas
export default [
  login,
];

// Individual exports for testing
export function* login() {
  while (true) {
    const watcher = yield race({
      login: take(LOGIN),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.login.formData;
    const requestURL = '/public/login';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    const callbackRoute = yield select(selectCallbackRoute());
    const previousRoute = (yield select(selectLocationState())).previousPathname;
    const invalidPreviousRoute = ['/client/login', '/client/signup'];

    if (result.success) {
      yield put(loadGlobalConfig());
      yield put(loginSuccess());
      message.success(i18n.t('Client_login_success'));

      if (callbackRoute) {
        yield put(clearCallbackRoute());
        browserHistory.replace(callbackRoute);
      } else if (previousRoute && !lodash.includes(invalidPreviousRoute, previousRoute)) {
        browserHistory.replace(previousRoute);
      } else {
        browserHistory.replace('/client');
      }
    } else {
      yield put(loginError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
