/*
 *
 * Munu
 *
 */

import React from 'react';
import styles from './styles.css';
import { Menu, Icon } from 'antd';
import { translate } from 'react-i18next';
import { Link } from 'react-router';

const CustomMenu = ({ selected, t }) => (
  <Menu
    className={styles.menu}
    selectedKeys={[selected]}
    mode="inline"
  >
    <Menu.Item key="1">
      <Link to="/client/account/info">
        <Icon type="user" />{t('Client_personal_info')}
      </Link>
    </Menu.Item>
    <Menu.Item key="2">
      <Link to="/client/account/password">
        <Icon type="lock" />{t('Client_change_password')}
      </Link>
    </Menu.Item>
    <Menu.Item key="3">
      <Link to="/client/account/address-book">
        <Icon type="book" />{t('Client_address_book')}
      </Link>
    </Menu.Item>
    <Menu.Item key="4">
      <Link to="/client/account/payment-records">
        <Icon type="pay-circle-o" />{t('Client_payment_records')}
      </Link>
    </Menu.Item>
  </Menu>
);

CustomMenu.propTypes = {
  selected: React.PropTypes.string,
  t: React.PropTypes.func,
};

export default translate()(CustomMenu);
