/*
 *
 * Info
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';
import {
  Form,
  Row,
  Card,
  Input,
  Button,
  Select,
  Spin,
  message,
} from 'antd';
import request from 'utils/request';
import { translate } from 'react-i18next';
import {
  selectClientInfo,
  selectCurrentUser,
  selectSiteName,
  selectConfig,
  selectCountries,
  selectLoadingCountries,
} from 'containers/App/selectors';
import { createSelector } from 'reselect';
import Menu from 'components/Menu';
import {
  saveInfo,
} from './actions';
import {
  loadCountries,
} from 'containers/App/actions';
import HorizontalLine from 'components/HorizontalLine';

const FormItem = Form.Item;
const Option = Select.Option;

class Info extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.hasSetFormInfo = false;
  }

  componentDidMount() {
    this.props.loadCountries();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.info && !this.hasSetFormInfo) {
      nextProps.form.setFieldsValue(nextProps.info);
      this.hasSetFormInfo = true;
    }
  }

  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFieldsAndScroll((error, value) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        this.props.handleSubmit(formData);
      }
    });
  };

  checkEmailExist = (rule, value, callback) => {
    const { t } = this.props;
    if (value && value !== this.props.info.email) {
      request(`/public/check-email?email=${value}`).then((response) => {
        if (response.exist) {
          callback(t('Client_email_exist'));
        } else {
          callback();
        }
      }, () => {
        console.log('check email failed');
      });
    } else {
      callback();
    }
  };

  caseInsensitiveFilterOption = (inputValue, option) =>
    (option.props.children && option.props.children.toLowerCase().includes(inputValue.toLowerCase()));

  render() {
    const { t } = this.props;
    const countries = this.props.countries.map(country => <Option key={country.iso} value={country.iso}>{country.printable_name}</Option>);
    const { getFieldProps } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    const emailProps = getFieldProps('email', {
      validate: [{
        rules: [
          { required: true, message: t('Client_hint_required') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { type: 'email', message: t('Client_hint_valid_email') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { validator: this.checkEmailExist },
        ],
        trigger: 'onBlur',
      }],
    });
    return (
      <div>
        <aside className={styles.antLayoutSider}>
          <Menu selected="1" />
        </aside>
        <div className={styles.antLayoutContent}>
          <Spin spinning={!this.props.info || !this.hasSetFormInfo}>
            <Row type="flex" justify="center" className={styles.cardWrapper}>
              <Card title={t('Client_personal_info')} className={styles.card}>
                <Form horizontal onSubmit={this.handleSubmit}>
                  <FormItem
                    {...formItemLayout}
                    label={t('Client_username')}
                  >
                    <Input
                      value={this.props.currentUser.username}
                      disabled
                    />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label={t('Client_site')}
                  >
                    <Input
                      value={this.props.siteName}
                      disabled
                    />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label={t('Client_balance')}
                  >
                    <Input
                      addonBefore={this.props.config.currencyCode}
                      value={this.props.info.balance}
                      disabled
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_nickname')}
                  >
                    <Input
                      {...getFieldProps('name', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_email')}
                  >
                    <Input
                      type="email"
                      {...emailProps}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_telephone')}
                  >
                    <Input
                      {...getFieldProps('telephone', { rules: [{ type: 'string', max: 20, message: t('Client_hint_telephone_invalid_length') }] })}
                    />
                  </FormItem>

                  <div className={styles.line}>
                    <HorizontalLine lineStyle="dotted" />
                  </div>

                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_sender_contact')}
                  >
                    <Input
                      {...getFieldProps('contact', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_post_code')}
                  >
                    <Input
                      {...getFieldProps('post_code', { rules: [
                        { type: 'string', max: 10, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_address_line1')}
                  >
                    <Input
                      {...getFieldProps('address_line1', { rules: [
                        { type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_address_line2')}
                  >
                    <Input
                      {...getFieldProps('address_line2', { rules: [
                        { type: 'string', max: 100, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_address_line3')}
                  >
                    <Input
                      {...getFieldProps('address_line3', { rules: [
                        { type: 'string', max: 100, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_city')}
                  >
                    <Input
                      {...getFieldProps('city', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_county')}
                  >
                    <Input
                      {...getFieldProps('county', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label={t('Client_country_or_area')}
                  >
                    <Spin spinning={this.props.loadingCountries} size="small">
                      <Select
                        {...getFieldProps('country_iso')}
                        size="large"
                        showSearch
                        filterOption={this.caseInsensitiveFilterOption}
                        placeholder={t('Client_select_a_country')}
                      >
                      {countries}
                      </Select>
                    </Spin>
                  </FormItem>

                  <div className={styles.line}>
                    <HorizontalLine lineStyle="dotted" />
                  </div>

                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_return_contact')}
                  >
                    <Input
                      {...getFieldProps('return_name', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_address')}
                  >
                    <Input
                      {...getFieldProps('return_address_line1', { rules: [
                        { type: 'string', max: 100, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_city')}
                  >
                    <Input
                      {...getFieldProps('return_city', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    hasFeedback
                    {...formItemLayout}
                    label={t('Client_county')}
                  >
                    <Input
                      {...getFieldProps('return_county', { rules: [
                        { type: 'string', max: 50, message: t('Client_hint_invalid_length') },
                      ] })}
                    />
                  </FormItem>
                  <FormItem
                    {...formItemLayout}
                    label={t('Client_country_or_area')}
                  >
                    <Spin spinning={this.props.loadingCountries} size="small">
                      <Select
                        {...getFieldProps('return_country_iso')}
                        size="large"
                        showSearch
                        filterOption={this.caseInsensitiveFilterOption}
                        placeholder={t('Client_select_a_country')}
                      >
                      {countries}
                      </Select>
                    </Spin>
                  </FormItem>

                  <FormItem>
                    <Button type="primary" htmlType="submit">{t('Client_save_change')}</Button>
                  </FormItem>
                </Form>
              </Card>
            </Row>
          </Spin>
        </div>
      </div>
    );
  }
}

Info.propTypes = {
  form: React.PropTypes.object,
  info: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  currentUser: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  siteName: React.PropTypes.string,
  handleSubmit: React.PropTypes.func,
  loadCountries: React.PropTypes.func,
  countries: React.PropTypes.array,
  t: React.PropTypes.func,
  config: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  loadingCountries: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectClientInfo(),
  selectCurrentUser(),
  selectSiteName(),
  selectCountries(),
  selectConfig(),
  selectLoadingCountries(),
  (info, currentUser, siteName, countries, config, loadingCountries) => ({
    info,
    currentUser,
    siteName,
    countries: countries || [],
    config,
    loadingCountries,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (formData) => dispatch(saveInfo(formData)),
    loadCountries: () => dispatch(loadCountries()),
    dispatch,
  };
}

const newInfo = Form.create({})(Info);

export { newInfo as Info };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newInfo));
