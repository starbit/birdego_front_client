import { createSelector } from 'reselect';
import { selectCountries } from 'containers/App/selectors';

/**
 * Direct selector to the sendPackage state domain
 */
const selectSendPackageDomain = () => (state) => state.get('sendPackage');

/**
 * Other specific selectors
 */
const selectStep = () => createSelector(
  selectSendPackageDomain(),
  (substate) => substate.get('step')
);
/**
 * Default selector used by SendPackage
 */

const selectSendPackage = () => createSelector(
  selectSendPackageDomain(),
  (substate) => substate.toJS()
);

/**
 * Form part 1/3: Shipment
 */
const selectShipmentPayload = () => createSelector(
  selectSendPackageDomain(),
  (substate) => substate.get('shipmentPayload')
);

const selectPackages = () => createSelector(
  selectShipmentPayload(),
  (form) => form.get('packages')
);

const selectFromCountry = () => createSelector(
  selectShipmentPayload(),
  (substate) => substate.getIn(['from_country_iso', 'value'])
);

const selectFromCountryName = () => createSelector(
  selectFromCountry(),
  selectCountries(),
  (from, countries) => {
    let fromCountryName = null;

    if (from) {
      if (countries) { // use countries to look up from country name
        fromCountryName = countries.find(country => (country.iso === from.get('key'))).printable_name;
      } else { // use form label as a second option
        fromCountryName = from.get('label');
      }
    }

    return fromCountryName;
  }
);

const selectFromCountryIso = () => createSelector(
  selectFromCountry(),
  (substate) => (substate ? substate.get('key') : null)
);

const selectToCountry = () => createSelector(
  selectShipmentPayload(),
  (substate) => substate.getIn(['to_country_iso', 'value'])
);

const selectToCountryName = () => createSelector(
  selectToCountry(),
  selectCountries(),
  (toCountry, countries) => {
    let toCountryName = null;

    if (toCountry) {
      if (countries) { // use countries to look up to country name
        toCountryName = countries.find(country => (country.iso === toCountry.get('key'))).printable_name;
      } else { // use form label as a second option
        toCountryName = toCountry.get('label');
      }
    }

    return toCountryName;
  }
);

const selectToCountryIso = () => createSelector(
  selectToCountry(),
  (substate) => (substate ? substate.get('key') : null)
);

const selectDeliveryService = () => createSelector(
  selectSendPackageDomain(),
  (substate) => (substate.get('deliveryService') ? substate.get('deliveryService').toJS() : null)
);

const selectOrderPayload = () => createSelector(
  selectSendPackageDomain(),
  (substate) => substate.get('orderPayload')
);

export default selectSendPackage;
export {
  selectSendPackageDomain,
  selectStep,
  selectShipmentPayload,
  selectPackages,
  selectFromCountry,
  selectFromCountryName,
  selectFromCountryIso,
  selectToCountryName,
  selectToCountryIso,
  selectDeliveryService,
  selectOrderPayload,
};
