export default {
  // % Begin Client app %
  Client_username: 'Username',
  Client_site: 'Site',
  Client_balance: 'Balance',
  Client_send_package: 'Send package',
  Client_slogan: 'One-stop transport service',
  Client_homepage: 'Home page',
  Client_quotes: 'Quotes',
  Client_shipment: 'Shipment',
  Client_order: 'Order',
  Client_from: 'From',
  Client_to: 'To',
  Client_select_a_country: 'Select a country or area',
  Client_package_details: 'Package Details',
  Client_quantity: 'QTY',
  Client_weight: 'Weight',
  Client_length: 'Length',
  Client_width: 'Width',
  Client_height: 'Height',
  Client_size: 'Size',
  Client_hint_from_and_to: 'From and to',
  Client_hint_package_details: 'Your package details',
  Client_get_quotes: 'Get Quotes',
  Client_carrier: 'Carrier',
  Client_delivery_time: 'Delivery time',
  Client_estimate_price: 'Estimate price',
  Client_previous_step: 'Previous',
  Client_next_step: 'Next',
  Client_day: 'days',
  Client_login_and_continue: 'Login and continue',
  Client_packages: 'Packages',
  Client_delivery_service_info: 'Delivery Service',
  Client_consignee_info: 'Consignee Info',
  Client_delivery_info: 'Delivery Info',
  Client_address_line1: 'Address line 1',
  Client_address_line2: 'Address line 2',
  Client_address_line3: 'Address line 3',
  Client_post_code: 'Post code',
  Client_telephone: 'Telephone',
  Client_create_order: 'Create order',
  Client_signup: 'Sign Up',
  Client_select_a_site: 'Select a site',
  Client_go_to_signup: 'Sign up',
  Client_captcha: 'Captcha',
  Client_captcha_image: 'Captcha image',
  Client_nickname: 'Nickname',
  Client_contact: 'Contact',
  Client_sender_contact: 'Sender',
  Client_return_contact: 'Return receiver',
  Client_city: 'City',
  Client_county: 'County/Province',
  Client_account_basic: 'Basic',
  Client_sender_address_info: 'Address',
  Client_return_address_info: 'Return address',
  Client_top_up: 'Top Up',
  Client_top_up_result: 'Top up result',
  Client_top_up_success: 'Congratulations! Top up success!',
  Client_top_up_fail: 'Sorry, top up failed',
  Client_amount: 'Amount',
  Client_orders: 'My Orders',
  Client_order_preparing: 'Preparing',
  Client_order_processing: 'Processing',
  Client_order_finished: 'Finished',
  Client_print_package_barcode: 'Print barcode',
  Client_account: 'My Account',
  Client_logging_in: 'Logging in...',
  Client_please_wait: 'Please wait...',
  Client_signing_up: 'Signing up...',
  Client_submitting: 'Submitting...',
  Client_delete: 'Delete',
  Client_cancel: 'Cancel',
  Client_deleting: 'Deleting...',
  Client_cancelling: 'Cancelling...',
  Client_reload: 'Reload',
  Client_jumping_to_paypal: 'Redirecting to PayPal...',
  Client_account_settings: 'Account Settings',
  Client_personal_info: 'Personal Info',
  Client_change_password: 'Change Password',
  Client_payment_records: 'Payment Records',
  Client_save_change: 'Update',
  Client_consignee: 'Consignee',
  Client_address: 'Address',
  Client_new_password: 'New password',
  Client_confirm_password: 'Confirm password',
  Client_confirm_new_password: 'Confirm password',
  Client_hint_required: 'Required',
  Client_hint_passwords_not_match: 'Passwords not match',
  Client_hint_please_input_valid_amount: 'Please input valid amount',
  Client_hint_valid_email: 'Please input a valid email',
  Client_how_to_use_our_service: 'How to use our service',
  Client_compare_and_choose: 'Compare and choose delivery service',
  Client_intermediate_warehouse: 'Intermediate warehouses',
  Client_around_the_clock_tracking: 'Around the clock tracking',
  Client_hint_please_check_the_form: 'Please check the form',
  Client_email: 'Email',
  Client_country: 'Country',
  Client_country_or_area: 'Country/Area',
  Client_type: 'Type',
  Client_update_time: 'Update time',
  Client_note: 'Note',
  Client_login: 'Login',
  Client_password: 'Password',
  Client_total_delivery_fee: 'Delivery Fee',
  Client_create_time: 'Create time',
  Client_logout: 'Logout',
  Client_first_name: 'First Name',
  Client_last_name: 'Last Name',
  Client_status_PREPARING: 'Preparing',
  Client_status_PROCESSING: 'Processing',
  Client_status_FINISHED: 'Finished',
  Client_status_CANCELLED: 'Cancelled',
  Client_status_DELETED: 'Deleted',
  Client_status_PROBLEM: 'Problem',
  Client_no_orders: 'No orders here',
  Client_balance_now: 'Now your balance is',
  Client_send_a_package_now: 'Send a package now',
  Client_please_login_first: 'Please login first',
  Client_username_exist: 'Username has already been taken',
  Client_email_exist: 'Email already exists',
  Client_hint_invalid_username: 'Username may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen',
  Client_hint_invalid_username_length: 'Username should between 3 and 50 characters',
  Client_hint_telephone_invalid_length: 'Telephone may not be longer than 20 charaters',
  Client_hint_invalid_length: 'Too long',
  Client_hint_password_invalid_length: 'Password should between 6 and 16 characters',
  Client_qq: 'QQ',
  Client_hint_qq_invalid_number: 'QQ should be a string of numbers',
  Client_track_package: 'Package tracking',
  Client_please_input_track_number: 'Please input a tracking string',
  Client_track: 'Track',
  Client_private_individual: 'Private individual',
  Client_are_you_professional: 'Are you professional?',
  Client_marketing_feature_1: 'Over 20 carriers accessible via the site',
  Client_marketing_feature_2: 'Self-supporting warehouses in UK, France, Germany',
  Client_marketing_feature_3: 'Lowest price, super fast delivery, real-time package tracking',
  Client_send_a_package_with_birdego: 'Send a package with Bird eGo',
  Client_create_free_account_now: 'Create a free account to get up to 20% off',
  Client_create_account: 'Create an account',
  Client_satisfied_customers: '10,0000 SATISFIED CUSTOMERS!',
  Client_partners: 'Partners',
  Client_company_intro: 'About us',
  Client_full_track: 'Full track',
  Client_support: 'Support',
  Client_all_rights_reserved: 'all rights reserved',
  Client_secure_pay: 'Secure pay',
  Client_step_signup_account: 'Sign up',
  Client_step_add_package: 'Add packages',
  Client_step_choose_carrier: 'Choose carrier',
  Client_step_wait_for_delivery: 'Just wait',
  Client_error_invalid_username_or_password: 'Username or password is invalid.',
  Client_error_request_timeout: 'Request Timeout',
  Client_error_system_error: 'System Error',
  Client_error_missing_required_param: 'Missing required param, please check.',
  Cient_error_invalid_param: 'Invalid param, please check.',
  Client_error_cannot_find_suitable_delivery_service: 'Cannot find suitable delivery service.',
  Client_error_cannot_find_suitable_delivery_service_route_template: 'Cannot find suitable delivery service route template.',
  Client_error_cannot_find_suitable_route_template: 'Cannot find suitable route template.',
  Client_low: 'Low',
  Client_medium: 'Medium',
  Client_high: 'High',
  Client_please_send_to_this_warehouse: 'Please send your package to this address:',
  Client_create_order_success: 'Great! Your order has been created.',
  Client_go_to_orders_page: 'View my orders',
  Client_send_another_package: 'Send another package',
  Client_view_warehouse_address: 'View warehouse address',
  Client_change_password_success: 'Successfully changed password',
  Client_update_info_success: 'Successfully updated account info',
  Client_CONSIGNMENT_FEE: 'Consignment fee',
  Client_CLINET_DISPATCH_FEE: 'Client dispatch fee',
  Client_REFUND: 'Refund',
  Client_ADJUSTMENT: 'Adjustment',
  Client_TOP_UP: 'Top up',
  Client_change_a_carrier: 'Change a carrier',
  Client_from_country_not_supported: 'Sorry, origination country not supported, please change another one',
  Client_to_country_not_supported: 'Sorry, destination country not supported, please change another one',
  Client_package_size_not_supported: 'Sorry, your package sizes are not supported, please modify them',
  Client_cannot_find_suitable_service: 'Sorry, cannot find suitable delivery service, please modify your shipment form',
  Client_trace_order: 'Trace order',
  Client_delete_order: 'Delete order',
  Client_cancel_order: 'Cancel order',
  Client_view_details: 'View details',
  Client_first_warehouse_waiting: 'Waiting for package',
  Client_no_trace_data: 'No trace data',
  Client_trace_steps_pending: 'pending',
  Client_trace_steps_delivering: 'delivering',
  Client_trace_steps_done: 'done',
  Client_trace_timeline_warehouse_waiting: '{{warehouseName}} is waiting for your packages',
  Client_trace_timeline_warehouse_receive: 'Your package is received by {{warehouseName}}',
  Client_trace_timeline_warehouse_dispatched: 'Your package is dispatched from {{warehouseName}}',
  Client_signup_success: 'Successfully signed up, please login',
  Client_login_success: 'Successfully logged in',
  Client_back_to_orders_page: 'Back to my orders',
  Client_address_book: 'Address Book',
  Client_save_address: 'Save address',
  Client_add_new_address: 'Add new address',
  Client_edit: 'Edit',
  Client_select_from_address_book: 'Select from address book',
  Client_delete_address_success: 'Successfully removed address',
  Client_empty_address_list: 'Empty address book',
  Client_create_address_success: 'Successfully created address',
  Client_update_address_success: 'Successfully updated address',
  Client_chose_address_from_address_book: 'Chose an address from the address book',
  Client_using_address_from_address_book: 'Using the address from address book',
  Client_reset: 'Clear form',
  Client_save_to_address_book: 'Would you like to save this address to address book for future use?',
  Client_need_update_address: 'You\'ve modified this address, would you like to save the changes?',
  Client_do_not_use_any_addresses_from_address_book: 'Do not use any addresses from address book',
  Client_page_not_found: 'Oops! Page not found...',
  Client_back_to_home_page: 'Back to home page',
  // % End Client app %
};
