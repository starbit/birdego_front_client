/*
 *
 * ChangePassword reducer
 *
 */

import { fromJS } from 'immutable';
import {
  HIDE_PASSWORD_BAR,
  HIDE_REPASSWORD_BAR,
  SET_PASSWORD_STRENGTH,
  SET_REPASSWORD_STRENGTH,
  CHANGE_PASSWORD_SUCCESS,
} from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = fromJS({
  passwordStrength: 'L',
  rePasswordStrength: 'L',
  passwordBarShow: false,
  rePasswordBarShow: false,
});

function defaultReducer(state = initialState, action) {
  switch (action.type) {
    case HIDE_PASSWORD_BAR:
      return state.set('passwordBarShow', false);
    case HIDE_REPASSWORD_BAR:
      return state.set('rePasswordBarShow', false);
    case SET_PASSWORD_STRENGTH:
      return state
        .set('passwordStrength', action.strength)
        .set('passwordBarShow', true);
    case SET_REPASSWORD_STRENGTH:
      return state
        .set('rePasswordStrength', action.strength)
        .set('rePasswordBarShow', true);
    case CHANGE_PASSWORD_SUCCESS:
    case LOCATION_CHANGE:
      return state
        .set('passwordBarShow', false)
        .set('rePasswordBarShow', false)
        .set('passwordStrength', 'L')
        .set('rePasswordStrength', 'L');
    default:
      return state;
  }
}

export default defaultReducer;
