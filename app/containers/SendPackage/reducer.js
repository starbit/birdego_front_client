/*
 *
 * SendPackge reducer
 *
 */

import { fromJS } from 'immutable';
import { immutableLocalStorage } from 'utils/localStorage';
import {
  SET_STEP,
  ADD_PACKAGE,
  REMOVE_PACKAGE,
  CHANGE_SHIPMENT_FORM,
  CLEAR_SHIPMENT_FORM,
  CHANGE_DELIVERY_SERVICE,
  CLEAR_DELIVERY_SERVICE,
  CHANGE_ORDER_FORM,
  CLEAR_ORDER_FORM,
} from './constants';
const initialState = fromJS({
  step: 0,
  shipmentPayload: JSON.parse(localStorage.getItem('shipmentPayload')) || { packages: [{}] },
  deliveryService: immutableLocalStorage.getItem('quoteSelection') || false,
  orderPayload: immutableLocalStorage.getItem('orderPayload') || {},
});

function sendPackageReducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case SET_STEP:
      return state.set('step', action.step);
    case ADD_PACKAGE:
      newState = state.updateIn(['shipmentPayload', 'packages'], [], packages => packages.push({}));
      immutableLocalStorage.setItem('shipmentPayload', newState.get('shipmentPayload'));
      return newState;
    case REMOVE_PACKAGE:
      newState = state.updateIn(['shipmentPayload', 'packages'], [], packages => packages.delete(action.key));
      immutableLocalStorage.setItem('shipmentPayload', newState.get('shipmentPayload'));
      return newState;
    case CHANGE_SHIPMENT_FORM:
      // Any better Immutable usage suggestions are welcome.
      newState = state.set('shipmentPayload', state.get('shipmentPayload').mergeDeep(action.shipmentPayload));
      immutableLocalStorage.setItem('shipmentPayload', newState.get('shipmentPayload'));
      return newState;
    case CLEAR_SHIPMENT_FORM:
      immutableLocalStorage.removeItem('shipmentPayload');
      return state.set('shipmentPayload', fromJS({ packages: [{}] }));
    case CHANGE_DELIVERY_SERVICE:
      newState = state.set('deliveryService', fromJS(action.deliveryService));
      immutableLocalStorage.setItem('quoteSelection', newState.get('deliveryService'));
      return newState;
    case CLEAR_DELIVERY_SERVICE:
      immutableLocalStorage.removeItem('quoteSelection');
      return state.set('deliveryService', false);
    case CHANGE_ORDER_FORM:
      newState = state.mergeDeepIn(['orderPayload'], action.orderPayload);
      immutableLocalStorage.setItem('orderPayload', newState.get('orderPayload'));
      return newState;
    case CLEAR_ORDER_FORM:
      immutableLocalStorage.removeItem('orderPayload');
      return state.set('orderPayload', fromJS({}));
    default:
      return state;
  }
}

export default sendPackageReducer;
