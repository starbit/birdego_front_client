import React from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import Collapse from 'react-collapse';
import OrderListItemHeader from './OrderListItemHeader';
import OrderListItemContent from './OrderListItemContent';
import { loadOrderDetail } from '../actions';
import { selectDetails, selectLoadingOrder } from '../selectors';
import styles from './styles.css';

class OrderCollapse extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
    };
  }

  onOpenCollpase() {
    const { isOpened } = this.state;
    const detail = this.props.details[this.props.order.id];
    if (!detail && !isOpened) { // not opened yet
      this.props.loadOrderDetail(this.props.order.id);
    }

    this.setState({ isOpened: !isOpened });
  }

  render() {
    const { order, pagination, details, loadingOrder } = this.props;
    const { printBarcode, removeOrder = () => {}, cancelOrder = () => {} } = this.props;
    const { showWarehouseAddress, showTrace, showRemove, showCancel, tracePlaceholder } = this.props;
    const loading = !!loadingOrder[order.id]; // if the detail is being loaded
    const detail = details[order.id]; // the detail data for OrderListItemContent

    return (
      <div className={styles.panel}>
        <OrderListItemHeader
          toggle={() => this.onOpenCollpase()}
          showTrace={showTrace}
          tracePlaceholder={tracePlaceholder}
          order={order}
        />
        <Collapse isOpened={this.state.isOpened}>
          <Spin spinning={loading}>
            <OrderListItemContent
              detail={detail || {}}
              printBarcode={printBarcode}
              pagination={pagination}
              showWarehouseAddress={showWarehouseAddress}
              showRemove={showRemove}
              showCancel={showCancel}
              removeOrder={removeOrder}
              cancelOrder={cancelOrder}
            />
          </Spin>
        </Collapse>
      </div>
    );
  }
}

OrderCollapse.propTypes = {
  order: React.PropTypes.object.isRequired,
  details: React.PropTypes.object,
  loadOrderDetail: React.PropTypes.func,
  loadingOrder: React.PropTypes.object,
  pagination: React.PropTypes.object.isRequired,
  printBarcode: React.PropTypes.func.isRequired,
  showWarehouseAddress: React.PropTypes.func.isRequired,
  showTrace: React.PropTypes.bool,
  tracePlaceholder: React.PropTypes.string,
  showRemove: React.PropTypes.bool,
  showCancel: React.PropTypes.bool,
  removeOrder: React.PropTypes.func,
  cancelOrder: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectDetails(),
  selectLoadingOrder(),
  (details, loadingOrder) => ({ details: details.toJS(), loadingOrder: loadingOrder.toJS() })
);

function mapDispatchToProps(dispatch) {
  return {
    loadOrderDetail: (id) => dispatch(loadOrderDetail(id)),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderCollapse);
