#!/bin/bash

cd `dirname $0`

force=0
if [[ "$1" == "-f" ]]
then
    force=1
    shift 1
fi
key="Client_$1"
en_value="$2"
zh_value="$3"

if [[ -z "$1" || -z "$2" || -z "$3" || -n "$4" ]]
then
    echo "Usage: $0 key en_value zh_value" >&2
    exit 1
fi
en_line="  $key: '$en_value',"
zh_line="  $key: '$zh_value',"

if [[ $force == 0 ]]
then
    if grep " $key:" *_*.js >/dev/null
    then
        echo 'Key exists!'
        grep " $key:" *_*.js
        exit 1
    fi
fi

f=en_GB.js
awk '/ '"$key"':/{c=1; print "'"$en_line"'"; found = 1;}  /% End Client app %/{if (found == 0) {print "'"$en_line"'"}} {if (c != 1) print $0; c = 0}' $f > $f.mod
mv $f.mod $f
f=zh_CN.js
awk '/ '"$key"':/{c=1; print "'"$zh_line"'"; found = 1;}  /% End Client app %/{if (found == 0) {print "'"$zh_line"'"}} {if (c != 1) print $0; c = 0}' $f > $f.mod
mv $f.mod $f
