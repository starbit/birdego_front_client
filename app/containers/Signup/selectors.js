import { createSelector } from 'reselect';

/**
 * Direct selector to the Signup state domain
 */
const selectSignupDomain = () => (state) => state.get('signup');

/**
 * Other specific selectors
 */
const selectCaptchaUrl = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('captchaUrl')
);

const selectCompanies = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('companies')
);

const selectLoadingCompanies = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('loadingCompanies')
);

const selectPasswordStrength = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('passwordStrength')
);

const selectRePasswordStrength = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('rePasswordStrength')
);

const selectPasswordBarShow = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('passwordBarShow')
);

const selectRePasswordBarShow = () => createSelector(
  selectSignupDomain(),
  (substate) => substate.get('rePasswordBarShow')
);

export {
  selectSignupDomain,
  selectCaptchaUrl,
  selectCompanies,
  selectLoadingCompanies,
  selectPasswordBarShow,
  selectPasswordStrength,
  selectRePasswordBarShow,
  selectRePasswordStrength,
};
