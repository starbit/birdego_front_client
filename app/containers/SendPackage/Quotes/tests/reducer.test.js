import expect from 'expect';
import quotesReducer from '../reducer';
import { fromJS } from 'immutable';

describe('quotesReducer', () => {
  it('returns the initial state', () => {
    expect(quotesReducer(undefined, {})).toEqual(fromJS({}));
  });
});
