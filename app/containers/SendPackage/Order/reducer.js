/*
 *
 * Order reducer
 *
 */

import { fromJS } from 'immutable';
import { immutableLocalStorage } from 'utils/localStorage';
import {
  CREATE_ORDER,
  CREATE_ORDER_SUCCESS,
  CREATE_ORDER_ERROR,
  OPEN_ADDRESS_LIST,
  CLOSE_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_SUCCESS,
  CHOOSE_ADDRESS,
  UNCHOOSE_ADDRESS,
} from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = fromJS({
  createSuccess: false,
  addressListOpen: false,
  addresses: false,
  addressChosen: {},
});

function orderReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_ORDER:
      return state;
    case CREATE_ORDER_SUCCESS:
      immutableLocalStorage.removeItem('shipmentPayload');
      return state.set('createSuccess', true);
    case CREATE_ORDER_ERROR:
      return state.set('createSuccess', false);
    case LOCATION_CHANGE:
      state = initialState; // eslint-disable-line
      return state;
    case OPEN_ADDRESS_LIST:
      return state.set('addressListOpen', true);
    case CLOSE_ADDRESS_LIST:
      return state.set('addressListOpen', false);
    case LOAD_ADDRESS_LIST_SUCCESS:
      return state.set('addresses', action.addresses);
    case CHOOSE_ADDRESS:
      return state.set('addressChosen', action.address);
    case UNCHOOSE_ADDRESS:
      return state.set('addressChosen', {});
    default:
      return state;
  }
}

export default orderReducer;
