/*
 *
 * AddressCards
 *
 */

import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { selectAddresses, selectAddressChosen } from './selectors';
import styles from './styles.css';
import { Row, Col } from 'antd';
import { createSelector } from 'reselect';
import { loadAddressList, chooseAddress, unchooseAddress } from './actions';
import classNames from 'classnames/bind';
import HorizontalLine from 'components/HorizontalLine';

const cx = classNames.bind(styles);

class AddressCards extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadAddressList();
  }

  toggleAddress = (address) => {
    if (address.id !== this.props.addressChosen.id) {
      this.props.setFormFields(address);
      this.props.chooseAddress(address);
    } else {
      this.props.resetFormFields();
      this.props.unchooseAddress();
    }
  }

  render() {
    const { t } = this.props;
    const addresses = this.props.addresses.map((address, index) => {
      const classSet = cx({
        addressItemInner: true,
        activeAddressItem: this.props.addressChosen.id === address.id,
      });
      return (
        <Col
          offset={(index + 1) % 3 === 1 ? 0 : 1}
          span={7} key={address.id} className={styles.addressItem}
          onClick={() => this.toggleAddress(address)}
        >
          <div className={classSet}>
            <div className={styles.addressName}>
              {address.first_name}&nbsp;
              {address.last_name}&nbsp;
            </div>
            <HorizontalLine color="#F4F4F4" />
            <div>
              {address.email}&nbsp;
              {address.telephone}&nbsp;
            </div>
            <div>
              {address.country}&nbsp;
              {address.county}&nbsp;
              {address.city}&nbsp;
              {address.post_code}&nbsp;
            </div>
            <div>
              {address.address_line1}&nbsp;
              {address.address_line2}&nbsp;
              {address.address_line3}&nbsp;
            </div>
          </div>
        </Col>
      );
    });

    return (
      <div>
        <Row type="flex" justify="space-between">
          {addresses}
        </Row>
      </div>
    );
  }
}

AddressCards.propTypes = {
  t: React.PropTypes.func,
  form: React.PropTypes.object,
  loadAddressList: React.PropTypes.func,
  addresses: React.PropTypes.array,
  chooseAddress: React.PropTypes.func,
  unchooseAddress: React.PropTypes.func,
  addressChosen: React.PropTypes.object,
  setFormFields: React.PropTypes.func,
  resetFormFields: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectAddresses(),
  selectAddressChosen(),
  (addresses, addressChosen) => ({ addresses: addresses || [], addressChosen })
);

function mapDispatchToProps(dispatch) {
  return {
    chooseAddress: (address) => dispatch(chooseAddress(address)),
    loadAddressList: () => dispatch(loadAddressList()),
    unchooseAddress: () => dispatch(unchooseAddress()),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(AddressCards));
