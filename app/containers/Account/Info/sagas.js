import { take, call, put, race, select } from 'redux-saga/effects';
import { SAVE_INFO } from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';
import i18n from 'i18next';
import request from 'utils/request';
import { loadClientInfoSuccess } from 'containers/App/actions';
import { selectClientInfo } from 'containers/App/selectors';
import { message } from 'antd';

// All sagas to be loaded
export default [
  updateInfo,
];

// Individual exports for testing
export function* updateInfo() {
  while (true) {
    const watcher = yield race({
      saveInfo: take(SAVE_INFO),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.saveInfo.formData;
    const requestURL = '/client';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      result.data.balance = (yield select(selectClientInfo())).balance;
      yield put(loadClientInfoSuccess(result.data));
      message.success(i18n.t('Client_update_info_success'), 3);
    } else {
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
