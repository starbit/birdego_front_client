/* eslint-disable camelcase */
import en_GB from './en_GB';
import zh_CN from './zh_CN';

const locale = {
  en_GB: { translation: en_GB },
  zh_CN: { translation: zh_CN },
};

export default locale;
