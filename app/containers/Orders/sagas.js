import { take, fork, call, put, race } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_ORDER_COUNTS, LOAD_ORDERS, PRINT_BARCODE, LOAD_ORDER_DETAIL, REMOVE_ORDER, CANCEL_ORDER, LOAD_ORDER_TRACE } from './constants';
import {
  loadOrderCounts,
  loadOrderCountsSuccess,
  loadOrderCountsError,
  loadOrders,
  loadOrdersSuccess,
  loadOrderSuccess,
  loadOrderError,
  loadOrdersError,
  removeOrderSuccess,
  removeOrderError,
  cancelOrderSuccess,
  cancelOrderError,
  loadOrderTraceSuccess,
  loadOrderTraceError,
} from './actions';
import request from 'utils/request';
import appConfig from 'config';

// Bootstrap sagas
export default [
  getOrderCounts,
  getOrdersWatcher,
  printBarcode,
  getOrder,
  deleteOrder,
  cancelOrder,
  loadOrderTrace,
];

function* getOrderCounts() {
  while (true) {
    const watcher = yield race({
      loadOrders: take(LOAD_ORDER_COUNTS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/consignment/get-count-by-status';
    const result = yield call(request, requestURL);


    if (result.success) {
      const orderCounts = {
        PREPARING: Number(result.data.PREPARING) || 0,
        FINISHED: Number(result.data.FINISHED) || 0,
        CANCELLED: Number(result.data.CANCELLED) || 0,
        DELETED: Number(result.data.DELETED) || 0,
        PROBLEM: Number(result.data.PROBLEM) || 0,
      };

      // manually add up all statuses that would be consider 'processing'
      orderCounts.PROCESSING = Number(result.data.CLIENT_DISPATCHED) || 0;
      orderCounts.PROCESSING += Number(result.data.LOCAL_WAREHOUSE_RECEIVED) || 0;
      orderCounts.PROCESSING += Number(result.data.LOCAL_WAREHOUSE_CHECKPASSED) || 0;
      orderCounts.PROCESSING += Number(result.data.LOCAL_WAREHOUSE_LOADED) || 0;
      orderCounts.PROCESSING += Number(result.data.TRANSFERING) || 0;

      yield put(loadOrderCountsSuccess(orderCounts));
    } else {
      yield put(loadOrderCountsError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

function* getOrders(pagination) {
  const requestURL = '/consignment'; // ?limit=-1';

  const requestConfig = {
    urlParams: Object.assign({}, pagination),
  };

  delete requestConfig.urlParams.page;

  if (pagination.status === 'PROCESSING') {
    delete requestConfig.urlParams.status;
    /**
     * {
     *   type: 'list',
     *   field: 'status',
     *   value: [
     *     'CLIENT_DISPATCHED',
     *     'LOCAL_WAREHOUSE_RECEIVED',
     *     'LOCAL_WAREHOUSE_CHECKPASSED',
     *     'LOCAL_WAREHOUSE_LOADED',
     *     'TRANSFERING',
     *   ],
     * };
     */
    requestConfig.urlParams.filter
      = '[{"type":"list","field":"status","value":["CLIENT_DISPATCHED","LOCAL_WAREHOUSE_RECEIVED","LOCAL_WAREHOUSE_CHECKPASSED","LOCAL_WAREHOUSE_LOADED","TRANSFERING"]}]';
  }

  const headerFields = [
    'id',
    'create_time',
    'from_country', // 'origination_country-printable_name',
    'to_country', // 'destination_country-printable_name',
    'first_name',
    'last_name',
  ];
  requestConfig.urlParams.selectFields = headerFields.join(',');

  const result = yield call(request, requestURL, requestConfig);

  if (result.success) {
    yield put(loadOrdersSuccess({
      ...pagination,
      total: result.data.total,
    }, result.data.list));
  } else {
    yield put(loadOrdersError(pagination.status));
    console.log(result.message); // eslint-disable-line no-console
  }
}

// Individual exports for testing
export function* getOrdersWatcher() {
  while (true) {
    const watcher = yield race({
      loadOrders: take(LOAD_ORDERS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    yield fork(getOrders, watcher.loadOrders.pagination);
  }
}

export function* getOrder() {
  while (true) {
    const watcher = yield race({
      loadOrder: take(LOAD_ORDER_DETAIL),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/consignment'; // ?id=${watcher.loadOrder.id}`;

    const contentFields = [
      'first_name',
      'last_name',
      'email',
      'telephone',
      'county',
      'city',
      'post_code',
      'address_line1',
      'address_line2',
      'address_line3',
      'total_delivery_fee',
      'create_time',
      'id',
    ];

    const requestConfig = {
      urlParams: {
        id: watcher.loadOrder.id,
        selectFields: contentFields.join(','),
      },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      // console.log(result.data);
      yield put(loadOrderSuccess(result.data));
    } else {
      yield put(loadOrderError(watcher.loadOrder.id));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}


export function* printBarcode() {
  while (true) {
    const watcher = yield race({
      printBarcode: take(PRINT_BARCODE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    // const requestURL = `/printer/consignment-package-by-consignment-id?id=${watcher.printBarcode.id}`;
    // const requestConfig = {
    //   headers: {
    //     Accept: 'text/html',
    //   },
    // };
    // // Use call from redux-saga for easier testing
    // const result = yield call(request, requestURL, requestConfig);

    // // We return an object in a specific format, see applications/client/utils/request.js for more information
    // if (result.err === undefined || result.err === null) {
    //   const win = window.open('', '_blank');
    //   win.document.write(result);
    //   win.focus();
    // } else {
    //   console.log(result.err.response); // eslint-disable-line no-console
    // }
    const url = `http://${appConfig.serverHost}/client/printer/consignment-package-by-consignment-id?id=${watcher.printBarcode.id}`;
    const win = window.open(url, '_blank');
    // console.log(url);
    win.focus();
  }
}

export function* deleteOrder() {
  while (true) {
    const watcher = yield race({
      removeOrder: take(REMOVE_ORDER),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestParams = watcher.removeOrder.order;
    const originalStatus = requestParams.status;
    const requestURL = '/consignment';

    const result = yield call(request, requestURL, {
      method: 'POST',
      body: { id: requestParams.id, status: 'DELETED' },
      feedback: { progress: { mask: true } },
    });

    if (result.success) {
      const { page, start, limit, total, status } = watcher.removeOrder.pagination;

      // console.log(result.data);
      yield put(removeOrderSuccess(originalStatus, watcher.removeOrder.order));
      yield put(loadOrderCounts());
      if (total === start + 1) {
        yield put(loadOrders({
          page: page - 1 || 1,
          start: start - limit < 0 ? 0 : start - limit,
          limit,
          status,
        }));
      } else {
        yield put(loadOrders({ page, start, limit, total, status }));
      }
    } else {
      yield put(removeOrderError(watcher.removeOrder.order));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* cancelOrder() {
  while (true) {
    const watcher = yield race({
      cancelOrder: take(CANCEL_ORDER),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestParams = watcher.cancelOrder.order;
    const originalStatus = requestParams.status;
    const requestURL = '/consignment';

    const result = yield call(request, requestURL, {
      method: 'POST',
      body: { id: requestParams.id, status: 'CANCELLED' },
      feedback: { progress: { mask: true } },
    });

    if (result.success) {
      const { page, start, limit, total, status } = watcher.cancelOrder.pagination;
      yield put(cancelOrderSuccess(originalStatus, watcher.cancelOrder.order));
      yield put(loadOrderCounts());
      if (total === start + 1) {
        yield put(loadOrders({
          page: page - 1 || 1,
          start: start - limit < 0 ? 0 : start - limit,
          limit,
          status,
        }));
      } else {
        yield put(loadOrders({ page, start, limit, total, status }));
      }
    } else {
      yield put(cancelOrderError(watcher.loadOrder));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

function* loadOrderTrace() {
  while (true) {
    const watcher = yield race({
      loadOrderTrace: take(LOAD_ORDER_TRACE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const orderId = watcher.loadOrderTrace.orderId;
    const requestURL = `/route/get-by-consignment-id?consignment_id=${orderId}`;
    const result = yield call(request, requestURL);


    if (result.success) {
      console.log('result', result); // eslint-disable-line no-console
      yield put(loadOrderTraceSuccess(orderId, result.data.list));
    } else {
      yield put(loadOrderTraceError(orderId));
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
