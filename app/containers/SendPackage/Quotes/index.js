/*
 *
 * Quotes
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { selectQuotes, selectLoading } from './selectors';
import { createSelector } from 'reselect';
import { loadQuotes } from './actions';
import StepsBar from 'components/StepsBar';
import { Button, Table, Icon } from 'antd';
import { browserHistory } from 'react-router';
import { translate } from 'react-i18next';
import { selectCurrentUser } from 'containers/App/selectors';
import { saveCallbackRoute } from 'containers/App/actions';
import { setStep, changeDeliveryService } from '../actions';
import { selectDeliveryService } from '../selectors';
import styles from './styles.css';

export class Quotes extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadQuotes();
    this.props.setStep(2);
  }

  onChange = (selectedRowKeys, selectedRows) => {
    this.props.changeDeliveryService(selectedRows[0]);
  };

  submit = () => {
    this.props.submit();
  }

  loginAndSubmit = () => {
    this.props.loginAndSubmit();
  }

  render() {
    const { t } = this.props;
    const columns = [{
      title: t('Client_carrier'),
      dataIndex: 'delivery_service_name',
    }, {
      title: t('Client_delivery_time'),
      dataIndex: 'total_speed',
      render: (text, record) => (
        <span>
          {record.total_speed} {t('Client_day')}
        </span>
      ),
    }, {
      title: t('Client_estimate_price'),
      dataIndex: 'total_fee',
      render: (text, record) => (
        <span>
          {record.currency} {record.total_fee}
        </span>
      ),
    }];
    let button;
    if (this.props.currentUser) {
      button = (
        <Button className={styles.button} type="primary" size="large" onClick={this.submit} disabled={!this.props.deliveryService}>
          {t('Client_next_step')}
          <Icon type="right" />
        </Button>
      );
    } else {
      button = (
        <Button className={styles.button} type="primary" size="large" onClick={this.loginAndSubmit} disabled={!this.props.deliveryService}>
          {t('Client_login_and_continue')}
        </Button>
      );
    }

    const selectedRowKeys = [];
    if (this.props.deliveryService) {
      selectedRowKeys.push(this.props.deliveryService.id);
    }

    return (
      <div>
        <StepsBar current={1} />
        <Table
          columns={columns}
          dataSource={this.props.quotes}
          rowSelection={{ onChange: this.onChange, selectedRowKeys, type: 'radio' }}
          rowKey={record => record.id}
          loading={this.props.loading}
        />
        <Button
          className={styles.button}
          type="ghost"
          size="large"
          onClick={(e) => {
            if (e && e.preventDefault) e.preventDefault();
            browserHistory.push('/client/shipment');
          }}
        >
          <Icon type="left" />
          {t('Client_previous_step')}
        </Button>

        {button}
      </div>
    );
  }
}

Quotes.propTypes = {
  loadQuotes: React.PropTypes.func,
  quotes: React.PropTypes.array,
  submit: React.PropTypes.func,
  loginAndSubmit: React.PropTypes.func,
  deliveryService: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  changeDeliveryService: React.PropTypes.func,
  currentUser: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  setStep: React.PropTypes.func,
  t: React.PropTypes.func,
  loading: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectQuotes(),
  selectDeliveryService(),
  selectCurrentUser(),
  selectLoading(),
  (quotes, deliveryService, currentUser, loading) => ({ quotes: quotes.toJS(), deliveryService, currentUser, loading }),
);

function mapDispatchToProps(dispatch) {
  return {
    loadQuotes: () => dispatch(loadQuotes()),
    changeDeliveryService: (deliveryService) => dispatch(changeDeliveryService(deliveryService)),
    setStep: (step) => dispatch(setStep(step)),
    submit: () => {
      browserHistory.push('/client/create-order');
    },
    loginAndSubmit: () => {
      dispatch(saveCallbackRoute('/client/create-order'));
      browserHistory.push('/client/login');
    },
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Quotes));
