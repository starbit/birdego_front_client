/*
 *
 * NewAddressForm
 *
 */

import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { selectNewAddressFormOpen, selectAddressEditing } from './selectors';
import styles from './styles.css';
import { Row, Col, Form, Button, Input, message, Select, Spin } from 'antd';
import Collapse from 'react-collapse';
import { createSelector } from 'reselect';
import {
  openNewAddressForm,
  closeNewAddressForm,
  createAddress,
  saveAddress,
  resetAddressEditing,
} from './actions';
import { Icon } from 'react-fa';
import {
  loadCountries,
} from 'containers/App/actions';
import {
  selectCountries,
  selectLoadingCountries,
} from 'containers/App/selectors';

const FormItem = Form.Item;
const Option = Select.Option;

class NewAddressForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadCountries();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.addressEditing.id !== this.props.addressEditing.id) {
      nextProps.form.setFieldsValue(nextProps.addressEditing);
    }
  }

  onClickToggle = () => {
    if (this.props.newAddressFormOpen) {
      this.props.closeNewAddressForm();
    } else {
      this.props.openNewAddressForm();
    }
    this.resetForm();
  };

  caseInsensitiveFilterOption = (inputValue, option) =>
    (option.props.children && option.props.children.toLowerCase().includes(inputValue.toLowerCase()));

  resetForm = () => {
    this.props.form.resetFields();
    this.props.form.setFieldsValue(this.props.form.getFieldsValue());
    this.props.resetAddressEditing();
  }

  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFieldsAndScroll((error) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        if (this.props.addressEditing.id) {
          formData.id = this.props.addressEditing.id;
          this.props.saveAddress(formData);
        } else {
          this.props.createAddress(formData);
        }
      }
    });
  };

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    const countries = this.props.countries.map(country => <Option key={country.iso} value={country.iso}>{country.printable_name}</Option>);

    const emailProps = getFieldProps('email', {
      validate: [{
        rules: [
          { required: true, message: t('Client_hint_required') },
        ],
        trigger: 'onBlur',
      }, {
        rules: [
          { type: 'email', message: t('Client_hint_valid_email') },
        ],
        trigger: ['onBlur', 'onChange'],
      }],
    });

    return (
      <div>
        <Button type="primary" onClick={this.onClickToggle} className={styles.addButton} size="large"><Icon className={styles.addIcon} name="plus" />{t('Client_add_new_address')}</Button>
        <Collapse isOpened={this.props.newAddressFormOpen}>
          <Form horizontal onSubmit={this.handleSubmit} className={styles.addForm}>
            <Row>
              <Col span={7}>
                <FormItem
                  label={t('Client_first_name')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('first_name', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 20, message: t('Client_hint_invalid_length') },
                    ] })}
                  />
                </FormItem>
              </Col>
              <Col span={7} offset={2}>
                <FormItem
                  label={t('Client_last_name')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('last_name', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 20, message: t('Client_hint_invalid_length') },
                    ] })}
                  />
                </FormItem>
              </Col>
              <Col span={6} offset={2}>
                <FormItem
                  {...formItemLayout}
                  label={t('Client_country_or_area')}
                >
                  <Spin spinning={this.props.loadingCountries} size="small">
                    <Select
                      {...getFieldProps('to_country_iso')}
                      size="large"
                      showSearch
                      filterOption={this.caseInsensitiveFilterOption}
                      placeholder={t('Client_select_a_country')}
                    >
                      {countries}
                    </Select>
                  </Spin>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={7}>
                <FormItem
                  label={t('Client_county')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('county', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 50, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
              </Col>
              <Col span={7} offset={2}>
                <FormItem
                  label={t('Client_city')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('city', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 50, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
              </Col>
              <Col span={6} offset={2}>
                <FormItem
                  label={t('Client_post_code')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('post_code', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 10, message: t('Client_hint_invalid_length') }] })}
                  />
                </FormItem>
              </Col>
            </Row>
            <FormItem
              label={t('Client_address_line1')}
              {...formItemLayout}
            >
              <Input
                {...getFieldProps('address_line1', { rules: [
                  { required: true, message: t('Client_hint_required') },
                  { type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
              />
            </FormItem>
            <FormItem
              label={t('Client_address_line2')}
              {...formItemLayout}
            >
              <Input
                {...getFieldProps('address_line2', { rules: [{ type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
              />
            </FormItem>
            <FormItem
              label={t('Client_address_line3')}
              {...formItemLayout}
            >
              <Input
                {...getFieldProps('address_line3', { rules: [{ type: 'string', max: 100, message: t('Client_hint_invalid_length') }] })}
              />
            </FormItem>
            <Row>
              <Col span={7}>
                <FormItem
                  label={t('Client_telephone')}
                  {...formItemLayout}
                >
                  <Input
                    {...getFieldProps('telephone', { rules: [
                      { required: true, message: t('Client_hint_required') },
                      { type: 'string', max: 20, message: t('Client_hint_telephone_invalid_length') },
                    ] })}
                  />
                </FormItem>
              </Col>
              <Col span={7} offset={2}>
                <FormItem
                  label={t('Client_email')}
                  {...formItemLayout}
                >
                  <Input
                    type="email"
                    {...emailProps}
                  />
                </FormItem>
              </Col>
            </Row>
            <Button type="primary" size="large" className={styles.resetButton} onClick={this.resetForm}>
              {t('Client_reset')}
            </Button>
            <Button type="primary" size="large" htmlType="submit" className={styles.submitButton}>
              {t('Client_save_address')}
            </Button>
          </Form>
        </Collapse>
      </div>
    );
  }
}

NewAddressForm.propTypes = {
  t: React.PropTypes.func,
  form: React.PropTypes.object,
  newAddressFormOpen: React.PropTypes.bool,
  openNewAddressForm: React.PropTypes.func,
  closeNewAddressForm: React.PropTypes.func,
  createAddress: React.PropTypes.func,
  loadCountries: React.PropTypes.func,
  loadingCountries: React.PropTypes.bool,
  countries: React.PropTypes.array,
  addressEditing: React.PropTypes.object,
  saveAddress: React.PropTypes.func,
  resetAddressEditing: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectNewAddressFormOpen(),
  selectCountries(),
  selectLoadingCountries(),
  selectAddressEditing(),
  (newAddressFormOpen, countries, loadingCountries, addressEditing) => ({
    newAddressFormOpen,
    countries: countries || [],
    loadingCountries,
    addressEditing,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    openNewAddressForm: () => dispatch(openNewAddressForm()),
    closeNewAddressForm: () => dispatch(closeNewAddressForm()),
    createAddress: (formData) => dispatch(createAddress(formData)),
    loadCountries: () => dispatch(loadCountries()),
    saveAddress: (formData) => dispatch(saveAddress(formData)),
    resetAddressEditing: () => dispatch(resetAddressEditing()),
    dispatch,
  };
}

const form = Form.create({})(NewAddressForm);

export { form as NewAddressForm };

export default translate()(connect(mapStateToProps, mapDispatchToProps)(form));
