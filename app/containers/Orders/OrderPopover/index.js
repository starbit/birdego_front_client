import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Spin, Popover } from 'antd';
import { selectOrderTrace, selectLoadingOrderTrace } from '../selectors';
import { loadOrderTrace } from '../actions';
import styles from './styles.css';
import { translate } from 'react-i18next';
import { Icon } from 'react-fa';

class OrderPopover extends React.Component { // eslint-disable-line react/prefer-stateless-function
  loadOrderTrace() {
    const { order, orderTrace } = this.props;
    if (order && order.id && !orderTrace[order.id]) {
      this.props.loadOrderTrace(order.id);
    }
  }

  createBriefTraces(traces = []) {
    const { t } = this.props;
    const briefTraces = [];
    const filteredTraces = traces; // .filter(trace => trace.receive_time);
    let waitingWarehouseAdded = false;

    filteredTraces.forEach((trace, idx) => {
      if (trace.dispatch_time) { // first, try dispatch info
        briefTraces.push(
          <p key={`${idx}trace_dispatch`} className={styles.traceItem}>
            {t(
              'Client_trace_timeline_warehouse_dispatched',
              { warehouseName: trace['warehouse-name'] }
            )}
          </p>
        );
      } else if (trace.receive_time) { // second, try receive info
        briefTraces.push(
          <p key={`${idx}trace_receive`} className={styles.traceItem}>
            {t(
              'Client_trace_timeline_warehouse_receive',
              { warehouseName: trace['warehouse-name'] }
            )}
          </p>
        );
      } else if (!waitingWarehouseAdded) { // third, no info, try add a waiting message
        briefTraces.push(
          <p key={`${idx}trace_wait`} className={styles.traceItem}>
            {
              t(
                'Client_trace_timeline_warehouse_waiting',
                { warehouseName: trace['warehouse-name'] }
              )
            }
          </p>
        );

        waitingWarehouseAdded = true;
      }
    });

    briefTraces.reverse();

    if (briefTraces.length) {
      briefTraces.push(
        <p key="trace_view_more" className={styles.viewMore}>
          <Link to={`/client/orders/${this.props.order.id}/trace`}>{t('Client_view_details')}</Link>
        </p>
      );

      return briefTraces;
    }

    return (<p className={styles.firstChild}>{t('Client_first_warehouse_waiting')}</p>);
  }

  render() {
    const { order, orderTrace, loadingTrace, t } = this.props;
    const orderId = order.id;
    const traces = orderTrace[orderId] || [];

    let content = null;

    if (!traces.length) { // empty trace info TODO: translation
      content = (<p>{t('Client_no_trace_data')}</p>);
    } else {
      content = this.createBriefTraces(traces);
    }

    // wrap it up with a spin
    content = (
      <Spin spinning={loadingTrace}>
        <div className={styles.popoverContainer}>
          {content}
        </div>
      </Spin>
    );

    return (
      <Popover
        content={content}
        placement="bottomLeft"
        onVisibleChange={() => this.loadOrderTrace()}
      >
        <Link
          to={`/client/orders/${orderId}/trace`}
          className={styles.link}
          onClick={(e) => { if (e) e.stopPropagation(); }}
        >
          <Icon name="map-marker" className={styles.icon} />
          {t('Client_trace_order')}
        </Link>
      </Popover>
    ); // TODO: translation
  }
}

OrderPopover.propTypes = {
  order: React.PropTypes.object.isRequired,
  orderTrace: React.PropTypes.object,
  loadingTrace: React.PropTypes.bool,
  loadOrderTrace: React.PropTypes.func,
  t: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectOrderTrace(),
  selectLoadingOrderTrace(),
  (orderTrace, loadingTrace) => ({
    orderTrace: orderTrace.toJS(),
    loadingTrace,
  }),
);

const mapDispatchToProps = (dispatch) => ({
  loadOrderTrace: (orderId) => dispatch(loadOrderTrace(orderId)),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(OrderPopover));
