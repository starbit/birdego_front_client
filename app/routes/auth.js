import { setRequireAuth } from 'containers/App/actions';
import { immutableLocalStorage } from 'utils/localStorage';

export const requireAuth = (store) => () => {
  store.dispatch(setRequireAuth(true));
};

export const checkCreateOrderPageRequirements = (store) => (nextState, replace) => {
  let shipmentPayload;
  const state = store.getState().toJS();

  // first, retrieve shipmentPayload from redux store
  if (state.sendPackage && state.sendPackage.shipmentPayload) {
    shipmentPayload = state.sendPackage.shipmentPayload;
  }
  // second, try local storage
  if (!shipmentPayload) {
    shipmentPayload = JSON.parse(localStorage.getItem('shipmentPayload'));
  }
  // alright, nowhere to be found
  if (!shipmentPayload) {
    shipmentPayload = {};
  }

  const { from_country_iso, to_country_iso, packages } = shipmentPayload;
  if (!from_country_iso || !to_country_iso || !packages || !packages.length) { // eslint-disable-line camelcase
    replace('/client/shipment');
    return;
  }

  // declare it here to save a tiny space when shipmentPayload is not present
  let deliveryService;

  // first, retrieve deliveryService from redux store
  if (state.sendPackage && state.sendPackage.deliveryService) {
    deliveryService = state.sendPackage.deliveryService;
  }
  // second, try local storage
  if (!deliveryService) {
    deliveryService = immutableLocalStorage.getItem('quoteSelection');
  }
  // deliveryService is either an immutable or false, so no toJS()
  if (!deliveryService) {
    replace('/client/quotes');
  }
};

export const checkQuotesPageRequirements = (store) => (nextState, replace) => {
  let shipmentPayload;
  const state = store.getState().toJS();

  // first, retrieve shipmentPayload from redux store
  if (state.sendPackage && state.sendPackage.shipmentPayload) {
    shipmentPayload = state.sendPackage.shipmentPayload;
  }
  // second, try local storage
  if (!shipmentPayload) {
    shipmentPayload = JSON.parse(localStorage.getItem('shipmentPayload'));
  }
  // alright, nowhere to be found
  if (!shipmentPayload) {
    shipmentPayload = {};
  }

  const { from_country_iso, to_country_iso, packages } = shipmentPayload;
  if (!from_country_iso || !to_country_iso || !packages || !packages.length) { // eslint-disable-line camelcase
    replace('/client/shipment');
  }
};
