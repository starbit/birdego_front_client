/*
 *
 * Orders
 *
 */

import React from 'react';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import styles from './styles.css';
import { Row, Button, Tabs, Badge, Icon, Spin, Pagination, Modal } from 'antd';
import { translate } from 'react-i18next';
import {
  selectOrderCounts,
  selectOrders,
  selectPagination,
  selectLoadingOrders,
  selectWarehouseAddressModalShow,
} from '../selectors';
import {
  loadOrderCounts,
  loadOrders,
  printBarcode,
  removeOrder,
  cancelOrder,
  showWarehouseAddressModal,
  hideWarehouseAddressModal,
} from '../actions';
import { loadWarehouseAddress } from 'containers/App/actions';
import OrderListItem from '../OrderListItem';
import WarehouseAddressCard from 'containers/WarehouseAddressCard';

const TabPane = Tabs.TabPane;

import { PAGE_LIMIT } from '../constants';

class OrderList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadOrderCounts();
    this.props.loadOrders({ page: 1, start: 0, limit: PAGE_LIMIT, status: 'PREPARING' });
  }

  loadPage = (page, status) => {
    this.props.loadOrders({
      page,
      start: (page - 1) * PAGE_LIMIT,
      limit: PAGE_LIMIT,
      status,
    });
  }

  showWarehouseAddress = (orderId) => {
    this.props.loadWarehouseAddress(orderId);
    this.props.showWarehouseAddressModal();
  }

  createTab(status) {
    const orderCount = this.props.orderCounts[status] || 0;
    const orders = this.props.orders[status] || [];
    const pagination = this.props.pagination[status];
    const { t } = this.props;

    if (orders.length < 1) {
      return (
        <TabPane tab={<Badge count={orderCount}><span className={styles.tabTitle}>{t(`Client_status_${status}`)}</span></Badge>} key={status}>
          <p className={styles.placeHolder}>
            {t('Client_no_orders')}
            <Button
              className={styles.reloadButton}
              type="primary"
              onClick={() => this.props.loadOrders({ page: 1, start: 0, limit: PAGE_LIMIT, status })}
            >
              {t('Client_reload')}
            </Button>
          </p>
        </TabPane>
      );
    }

    const orderActionsConfig = {
      PREPARING: { trace: false, remove: true, cancel: true, tracePlaceholder: t('Client_first_warehouse_waiting') },
      PROCESSING: { trace: true, remove: false, cancel: false },
      FINISHED: { trace: true, remove: false, cancel: false },
      CANCELLED: { trace: false, remove: false, cancel: false },
      DELETED: { trace: false, remove: false, cancel: false },
      PROBLEM: { trace: true, remove: false, cancel: false },
    };

    const orderList = orders.map((order, idx) => (
      <OrderListItem
        order={order}
        pagination={pagination}
        key={idx}
        printBarcode={this.props.printBarcode}
        showWarehouseAddress={this.showWarehouseAddress}
        showTrace={orderActionsConfig[status].trace}
        tracePlaceholder={orderActionsConfig[status].tracePlaceholder}
        showRemove={orderActionsConfig[status].remove}
        showCancel={orderActionsConfig[status].cancel}
        removeOrder={this.props.removeOrder}
        cancelOrder={this.props.cancelOrder}
      />
    ));

    return (
      <TabPane tab={<Badge count={orderCount}><span className={styles.tabTitle}>{t(`Client_status_${status}`)}</span></Badge>} key={status}>
        <Row>
          <Button
            type="ghost"
            onClick={() => this.props.loadOrders({ page: 1, start: 0, limit: PAGE_LIMIT, status })}
          >
            <Icon type="reload" />
          </Button>
        </Row>
        <br />
        <div>
          {orderList}
        </div>
        <br />
        <Pagination current={pagination.page} pageSize={pagination.limit} total={pagination.total} onChange={(targetPage) => this.loadPage(targetPage, status)} />
      </TabPane>
    );
  }

  render() {
    const { t } = this.props;
    return (
      <div>
        <Spin spinning={this.props.loadingOrders}>
          <div className={styles.listCard}>
            <Tabs defaultActiveKey="Client_status_PREPARING" onTabClick={(tabKey) => this.props.loadOrders({ page: 1, start: 0, limit: PAGE_LIMIT, status: tabKey })}>
              {this.createTab('PREPARING')}
              {this.createTab('PROCESSING')}
              {this.createTab('FINISHED')}
              {this.createTab('PROBLEM')}
              {this.createTab('CANCELLED')}
              {this.createTab('DELETED')}
            </Tabs>
          </div>
          <Modal
            title={t('Client_view_warehouse_address')}
            visible={this.props.warehouseAddressModalShow}
            onOk={this.props.hideWarehouseAddressModal}
            onCancel={this.props.hideWarehouseAddressModal}
            closable={false}
            width={'60%'}
          >
            <WarehouseAddressCard />
          </Modal>
        </Spin>
      </div>
    );
  }
}

OrderList.propTypes = {
  loadOrderCounts: React.PropTypes.func,
  orderCounts: React.PropTypes.object,
  loadOrders: React.PropTypes.func,
  orders: React.PropTypes.object,
  pagination: React.PropTypes.object,
  printBarcode: React.PropTypes.func,
  t: React.PropTypes.func,
  loadingOrders: React.PropTypes.bool,
  removeOrder: React.PropTypes.func,
  cancelOrder: React.PropTypes.func,
  loadWarehouseAddress: React.PropTypes.func,
  showWarehouseAddressModal: React.PropTypes.func,
  hideWarehouseAddressModal: React.PropTypes.func,
  warehouseAddressModalShow: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectOrderCounts(),
  selectOrders(),
  selectPagination(),
  selectLoadingOrders(),
  selectWarehouseAddressModalShow(),
  (orderCounts, orders, pagination, loadingOrders, warehouseAddressModalShow) => ({
    orderCounts: orderCounts.toJS(),
    orders: orders.toJS(),
    pagination: pagination.toJS(),
    loadingOrders,
    warehouseAddressModalShow,
  })
);

function mapDispatchToProps(dispatch) {
  return {
    loadOrderCounts: () => dispatch(loadOrderCounts()),
    loadOrders: (pagination) => dispatch(loadOrders(pagination)),
    printBarcode: (id) => dispatch(printBarcode(id)),
    loadWarehouseAddress: (orderId) => dispatch(loadWarehouseAddress(orderId)),
    showWarehouseAddressModal: () => dispatch(showWarehouseAddressModal()),
    hideWarehouseAddressModal: () => dispatch(hideWarehouseAddressModal()),
    removeOrder: (pagination, order) => dispatch(removeOrder(pagination, order)),
    cancelOrder: (pagination, order) => dispatch(cancelOrder(pagination, order)),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(OrderList));
