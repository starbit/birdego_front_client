/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import styles from './styles.css';
import { Row, Button, Icon } from 'antd';
import Img from 'components/Img';
import NotFoundImage from './404.svg';
import { translate } from 'react-i18next';
import { Link } from 'react-router';

function NotFound(props) {
  const { t } = props;

  const homeUrl = '/client';

  return (
    <div className={styles.notFound}>
      <Row type="flex" align="middle" justify="center">
        <Img alt="404" src={NotFoundImage} className={styles.image} />
      </Row>
      <div className={styles.content}>
        <Button type="ghost" size="large" className={styles.button}>
          <Link to={homeUrl}>
            {t('Client_page_not_found')}{t('Client_back_to_home_page')}
            <Icon type="arrow-right" />
          </Link>
        </Button>
      </div>
    </div>
  );
}

NotFound.propTypes = {
  t: React.PropTypes.func,
};

export default translate()(NotFound);
