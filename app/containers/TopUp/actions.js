/*
 *
 * Top up actions
 *
 */

import {
  TOP_UP,
  TOP_UP_ERROR,
  TOP_UP_SUCCESS,
} from './constants';

export function topUp(formData) {
  return {
    type: TOP_UP,
    formData,
  };
}

export function topUpError() {
  return {
    type: TOP_UP_ERROR,
  };
}

export function topUpSuccess() {
  return {
    type: TOP_UP_SUCCESS,
  };
}
