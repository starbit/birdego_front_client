import { createSelector } from 'reselect';

/**
 * Direct selector to the ChangePassword state domain
 */
const selectChangePasswordDomain = () => (state) => state.get('changePassword');

/**
 * Other specific selectors
 */
const selectPasswordStrength = () => createSelector(
  selectChangePasswordDomain(),
  (substate) => substate.get('passwordStrength')
);

const selectRePasswordStrength = () => createSelector(
  selectChangePasswordDomain(),
  (substate) => substate.get('rePasswordStrength')
);

const selectPasswordBarShow = () => createSelector(
  selectChangePasswordDomain(),
  (substate) => substate.get('passwordBarShow')
);

const selectRePasswordBarShow = () => createSelector(
  selectChangePasswordDomain(),
  (substate) => substate.get('rePasswordBarShow')
);

export {
  selectChangePasswordDomain,
  selectPasswordStrength,
  selectRePasswordStrength,
  selectPasswordBarShow,
  selectRePasswordBarShow,
};
