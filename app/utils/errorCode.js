/**
 * Error Code Wiki:
 * https://birdsystem.atlassian.net/wiki/pages/viewpage.action?pageId=39387273
 *
 */
import i18n from 'i18next';
import { browserHistory } from 'react-router';

export const ERROR_LEVEL_IGNORE = 'ignore';
export const ERROR_LEVEL_WARNING = 'warning';
export const ERROR_LEVEL_ERROR = 'error';
export const ERROR_LEVEL_FATAL = 'fatal';

const handlerMap = {
  // Missing required param [%s].
  100101: (data) => errorAssessment(false, data.message || i18n.t('Client_error_missing_required_param'), ERROR_LEVEL_ERROR),
  // Missing required param [%s].
  100102: (data) => errorAssessment(false, data.message || i18n.t('Client_error_missing_required_param'), ERROR_LEVEL_ERROR),
  // Invalid param [%s].
  100103: (data) => errorAssessment(false, data.message || i18n.t('Cient_error_invalid_param'), ERROR_LEVEL_ERROR),
  // System error,please try again later.
  100104: () => ignore,
  // Cannot find %s [%s].
  100105: () => ignore,
  // Please choose record.
  100106: () => ignore,
  // You do not have sufficient privilege to access this.
  100107: () => ignore,
  // Unidentified action to log. Please contact system developer.
  100108: () => ignore,
  // Method not allowed
  100109: () => ignore,
  // Username or password or apikey is invalid.
  100110: () => errorAssessment(false, i18n.t('Client_error_invalid_username_or_password'), ERROR_LEVEL_ERROR),
  // Please login first
  100111: () => {
    browserHistory.push('/client/login');
    return errorAssessment(false, i18n.t('Client_please_login_first'), ERROR_LEVEL_ERROR, 5);
  },
  // Cannot find suitable delivery service.
  200101: () => {
    browserHistory.push('/client/shipment');
    return errorAssessment(false, i18n.t('Client_error_cannot_find_suitable_delivery_service'), ERROR_LEVEL_ERROR);
  },
  // Cannot find suitable delivery service.
  200102: () => {
    browserHistory.push('/client/shipment');
    return errorAssessment(false, i18n.t('Client_error_cannot_find_suitable_delivery_service'), ERROR_LEVEL_ERROR);
  },
  // Cannot find suitable delivery service. (Why they are identical is beyond me.)
  200103: () => {
    browserHistory.push('/client/shipment');
    return errorAssessment(false, i18n.t('Client_error_cannot_find_suitable_delivery_service'), ERROR_LEVEL_ERROR);
  },
};

/**
 * Assessment templates - ignore
 *
 */
function ignore() {
  return errorAssessment(false, '', ERROR_LEVEL_IGNORE); // I have no messages to delivery, please ignore.
}

function defaultHandler(code) {
  if (code && code.length > 0) {
    if (code[0] === '1') { // general purpose error code
      return errorAssessment(false, i18n.t('Client_error_system_error'), ERROR_LEVEL_ERROR);
    } else if (code[0] === '2') { // business error code
      return errorAssessment(false, i18n.t('Client_error_system_error'), ERROR_LEVEL_ERROR);
    }

    return errorAssessment(false, i18n.t('Client_error_system_error'), ERROR_LEVEL_ERROR);
  }

  return errorAssessment(false, i18n.t('Client_error_system_error'), ERROR_LEVEL_FATAL);
}

/**
 *  The data structure of handler's return value.
 *
 */

function errorAssessment(success, message, level, duration) {
  return {
    success,
    message,
    level,
    duration,
  };
}

export default function errorCodeHandler(data) {
  const handler = handlerMap[data.errCode] || defaultHandler;

  return handler(data);
}
