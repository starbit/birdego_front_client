import expect from 'expect';
import addressBookReducer from '../reducer';
import { fromJS } from 'immutable';

describe('addressBookReducer', () => {
  it('returns the initial state', () => {
    expect(addressBookReducer(undefined, {})).toEqual(fromJS({}));
  });
});
