import { take, call, put, race } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { SIGNUP, LOAD_CAPTCHA, LOAD_COMPANIES } from './constants';
import { displayCaptcha, loadCompaniesError, loadCompaniesSuccess, signupSuccess, signupError } from './actions';
import request from 'utils/request';
import { browserHistory } from 'react-router';
import { CHANGE_LANGUAGE_SUCCESS } from 'containers/App/constants';
import i18n from 'i18next';
import { message } from 'antd';

// Bootstrap sagas
export default [
  signup,
  getCaptcha,
  getCompaniesWatcher,
];

// Individual exports for testing
export function* signup() {
  while (true) {
    const watcher = yield race({
      signup: take(SIGNUP),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.signup.formData;
    const requestURL = '/public/register';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(signupSuccess());
      message.success(i18n.t('Client_signup_success'), 5);
      browserHistory.push('/client/login');
    } else {
      yield put(signupError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getCaptcha() {
  while (true) {
    const watcher = yield race({
      loadCaptcha: take(LOAD_CAPTCHA),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/public/captcha';
    const requestConfig = {
      headers: {
        Accept: 'image/png',
      },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.err === undefined || result.err === null) {
      const imageURL = URL.createObjectURL(result);
      yield put(displayCaptcha(imageURL));
    } else {
      console.log(result.err.response); // eslint-disable-line no-console
    }
  }
}

export function* getCompaniesWatcher() {
  let watcher = yield race({
    loadCompanies: take(LOAD_COMPANIES),
    stop: take(LOCATION_CHANGE), // stop watching if user leaves page
  });

  if (watcher.stop) return;

  yield call(getCompanies);

  while (true) {
    watcher = yield race({
      reload: take(CHANGE_LANGUAGE_SUCCESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    yield call(getCompanies);
  }
}

export function* getCompanies() {
  const requestURL = '/company?limit=-1';

  const result = yield call(request, requestURL);

  if (result.success) {
    yield put(loadCompaniesSuccess(result.data.list));
  } else {
    yield put(loadCompaniesError());
    console.log(result.message); // eslint-disable-line no-console
  }
}
