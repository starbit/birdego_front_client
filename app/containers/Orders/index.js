/*
 *
 * Orders
 *
 */

import React from 'react';

const Orders = ({ children }) => (
  <div>
    {children}
  </div>
);

Orders.propTypes = {
  children: React.PropTypes.node,
};

export default Orders;
