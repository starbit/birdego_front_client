import { take, call, put, race } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_WAREHOUSES } from './constants';
import { CHANGE_LANGUAGE } from 'containers/App/constants';
import {
  loadWarehousesSuccess,
  loadWarehousesError,
} from './actions';
import request from 'utils/request';

// Bootstrap sagas
export default [
  getWarehousesWatcher,
];

export function* getWarehousesWatcher() {
  let watcher = yield race({
    loadWarehouses: take(LOAD_WAREHOUSES),
    stop: take(LOCATION_CHANGE), // stop watching if user leaves page
  });

  if (watcher.stop) return;

  yield call(getWarehouses);

  while (true) {
    watcher = yield race({
      reload: take(CHANGE_LANGUAGE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const locale = watcher.reload.lang;
    yield call(getWarehouses, locale);
  }
}

export function* getWarehouses(locale = undefined) {
  let requestURL = '/country/get-list-by-warehouse?limit=-1';
  if (locale) {
    requestURL += `&locale=${locale}`;
  }

  const result = yield call(request, requestURL);

  if (result.success) {
    yield put(loadWarehousesSuccess(result.data.list));
  } else {
    console.log(result.message); // eslint-disable-line no-console
    yield put(loadWarehousesError(result.message));
  }
}
