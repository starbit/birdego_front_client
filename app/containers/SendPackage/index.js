/*
 *
 * SendPackage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import selectSendPackage from './selectors';

export class SendPackage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

SendPackage.propTypes = {
  children: React.PropTypes.node,
};

const mapStateToProps = selectSendPackage();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SendPackage);
