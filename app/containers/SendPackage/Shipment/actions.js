/*
 *
 * Shipment actions
 *
 */

import {
  LOAD_WAREHOUSES,
  LOAD_WAREHOUSES_SUCCESS,
  LOAD_WAREHOUSES_ERROR,
} from './constants';

export function loadWarehouses() {
  return {
    type: LOAD_WAREHOUSES,
  };
}

export function loadWarehousesSuccess(warehouses) {
  return {
    type: LOAD_WAREHOUSES_SUCCESS,
    warehouses,
  };
}

export function loadWarehousesError(error) {
  return {
    type: LOAD_WAREHOUSES_ERROR,
    error,
  };
}
