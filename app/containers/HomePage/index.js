/*
 *
 * HomePage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import selectHomePage from './selectors';
import styles from './styles.css';
import { Row, Col, Carousel, Icon, Button } from 'antd';
import Img from 'components/Img';
import StepOneImage from './step1.svg';
import StepTwoImage from './step2.svg';
import StepThreeImage from './step3.svg';
import { translate } from 'react-i18next';
import { Link } from 'react-router';
import StepOne from './step1.png';
import StepTwo from './step2.png';
import StepThree from './step3.png';
import StepFour from './step4.png';
import LeftToRightLine from './line_lt.png';
import RightToLeftLine from './line_gt.png';

export class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    setTimeout(() => window.dispatchEvent(new Event('resize')), 1);
    // window.dispatchEvent(new Event('resize')); // this doesn't work
  }
  render() {
    const { t } = this.props;

    return (
      <div>
        <Carousel
          className={styles.marketing}
        >
          <div className={styles.marketing1}>
            <Row type="flex" justify="center" className={styles.tab}>
              <Col span={14}>
                <Row type="flex" justify="space-between">
                  <Col span={14} className={styles.headerLeft}>
                    <span>{t('Client_private_individual')}</span>
                  </Col>
                  <Col className={styles.headerRight}>
                    <Button size="large" className={styles.switchButton}>{t('Client_are_you_professional')}</Button>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col span={14} className={styles.contentWrapper}>
                <Row type="flex" align="middle">
                  <Col span={15} offset={1} className={styles.content}>
                    <p className={styles.banner}>{t('Client_send_a_package_with_birdego')}</p>
                    <p className={styles.feature}><Icon type="check" className={styles.checkIcon} />{t('Client_marketing_feature_1')}</p>
                    <p className={styles.feature}><Icon type="check" className={styles.checkIcon} />{t('Client_marketing_feature_2')}</p>
                    <p className={styles.feature}><Icon type="check" className={styles.checkIcon} />{t('Client_marketing_feature_3')}</p>
                    <Button type="primary" size="large" className={styles.quoteButton}>
                      <Link to="/client/shipment">{t('Client_get_quotes')}</Link>
                    </Button>
                  </Col>
                  <Col span={5} className={styles.content}>
                    <Row type="flex" align="middle" justify="center">
                      <Col>
                        <Img src={StepOneImage} alt="step 1" className={styles.badge} />
                      </Col>
                    </Row>
                    <Row type="flex" align="middle" justify="center">
                      <Col>
                        <p className={styles.satisfiedCustomer}>{t('Client_satisfied_customers')}</p>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col span={14}>
                <Row type="flex" align="middle" className={styles.signupWrapper}>
                  <Col span={15} offset={1} className={styles.signupContent}>
                    {t('Client_create_free_account_now')}
                  </Col>
                  <Col span={5} className={styles.content}>
                    <Button type="primary" size="large" className={styles.signupButton}>
                      <Link to="/client/signup">{t('Client_create_account')}</Link>
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className={styles.marketing2}>
          </div>
        </Carousel>
        <div className={styles.stepWrapper}>
          <Row type="flex" align="middle" justify="center">
            <Col span={10}>
              <Img alt="step 1" src={StepOne} className={styles.stepImg} />
            </Col>
            <Col span={10} offset={2}>
              <p className={styles.stepNumber}>1</p>
              <p className={styles.stepTitle}>{t('Client_step_signup_account')}</p>
              <p className={styles.stepDesc}>blabla blabla blabla</p>
            </Col>
          </Row>
          <Row type="flex" justify="center">
            <Col>
              <Img alt="line" src={LeftToRightLine} />
            </Col>
          </Row>
          <Row type="flex" align="middle" justify="center">
            <Col span={10}>
              <p className={styles.stepNumber}>2</p>
              <p className={styles.stepTitle}>{t('Client_step_add_package')}</p>
              <p className={styles.stepDesc}>blabla blabla blabla</p>
            </Col>
            <Col span={10} offset={2}>
              <Img alt="step 2" src={StepTwo} className={styles.stepImg} />
            </Col>
          </Row>
          <Row type="flex" justify="center">
            <Col>
              <Img alt="line" src={RightToLeftLine} />
            </Col>
          </Row>
          <Row type="flex" align="middle" justify="center">
            <Col span={10}>
              <Img alt="step 3" src={StepThree} className={styles.stepImg} />
            </Col>
            <Col span={10} offset={2}>
              <p className={styles.stepNumber}>3</p>
              <p className={styles.stepTitle}>{t('Client_step_choose_carrier')}</p>
              <p className={styles.stepDesc}>blabla blabla blabla</p>
            </Col>
          </Row>
          <Row type="flex" justify="center" justify="center">
            <Col>
              <Img alt="line" src={LeftToRightLine} />
            </Col>
          </Row>
          <Row type="flex" align="middle" justify="center">
            <Col span={10}>
              <p className={styles.stepNumber}>4</p>
              <p className={styles.stepTitle}>{t('Client_step_wait_for_delivery')}</p>
              <p className={styles.stepDesc}>blabla blabla blabla</p>
            </Col>
            <Col span={10} offset={2}>
              <Img alt="step 4" src={StepFour} className={styles.stepImg} />
            </Col>
          </Row>
        </div>
        <div className={styles.advantageWrapper}>
          <Row type="flex" justify="space-between">
            <Col span={4}>
              <Img src={StepOneImage} alt="1" className={styles.stepImage} />
              <p className={styles.stepContent}>{t('Client_compare_and_choose')}</p>
            </Col>
            <Col span={4}>
              <Img src={StepTwoImage} alt="2" className={styles.stepImage} />
              <p className={styles.stepContent}>{t('Client_intermediate_warehouse')}</p>
            </Col>
            <Col span={4}>
              <Img src={StepThreeImage} alt="3" className={styles.stepImage} />
              <p className={styles.stepContent}>{t('Client_around_the_clock_tracking')}</p>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  t: React.PropTypes.func,
};

const mapStateToProps = selectHomePage();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(HomePage));

