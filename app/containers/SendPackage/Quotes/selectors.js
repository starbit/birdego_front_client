import { createSelector } from 'reselect';

/**
 * Direct selector to the quotes state domain
 */
const selectQuotesDomain = () => (state) => state.get('quotes');

/**
 * Other specific selectors
 */
const selectQuotes = () => createSelector(
  selectQuotesDomain(),
  (substate) => substate.get('quotes')
);
const selectLoading = () => createSelector(
  selectQuotesDomain(),
  (substate) => substate.get('loading')
);

export {
  selectQuotesDomain,
  selectQuotes,
  selectLoading,
};
