import { take, race, put, select, call } from 'redux-saga/effects';
import {
  CREATE_ORDER,
  LOAD_ADDRESS_LIST,
  SAVE_ADDRESS,
  CREATE_ADDRESS,
} from './constants';
import { clearShipmentForm, clearDeliveryService, clearOrderForm } from '../actions';
import {
  createOrderSuccess,
  createOrderError,
  loadAddressListSuccess,
  loadAddressListError,
  createAddressSuccess,
  createAddressError,
  saveAddressSuccess,
  saveAddressError,
} from './actions';
import { LOCATION_CHANGE } from 'react-router-redux';
import { CHANGE_LANGUAGE } from 'containers/App/constants';
import { selectPackages, selectToCountryIso, selectDeliveryService } from '../selectors';
import request from 'utils/request';
import { loadCountries, loadWarehouseAddress } from 'containers/App/actions';
import { message } from 'antd';
import i18n from 'i18next';

// All sagas to be loaded
export default [
  addOrder,
  reloadCountries,
  getAddressListWatcher,
  addAddress,
  updateAddress,
];

// Individual exports for testing
export function* addOrder() {
  while (true) {
    const watcher = yield race({
      createOrder: take(CREATE_ORDER),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/consignment/create-from-quote';
    const deliveryService = yield select(selectDeliveryService());
    const packages = (yield select(selectPackages())).map(
      pkg => pkg.set('width', pkg.getIn(['width', 'value']))
        .set('length', pkg.getIn(['length', 'value']))
        .set('depth', pkg.getIn(['depth', 'value']))
        .set('weight', pkg.getIn(['weight', 'value']))
        .set('quantity', pkg.getIn(['quantity', 'value']))
    ).toJS();
    const toCountryIso = (yield select(selectToCountryIso()));
    const formData = watcher.createOrder.formData;
    const requestConfig = {
      method: 'POST',
      body: {
        packages,
        to_country_iso: toCountryIso,
        delivery_service_route_template_id: deliveryService.id,
        ...formData,
      },
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(createOrderSuccess(result.data));
      yield put(loadWarehouseAddress(result.data.id));
      yield put(clearShipmentForm());
      yield put(clearDeliveryService());
      yield put(clearOrderForm());
    } else {
      console.log(result.message); // eslint-disable-line no-console
      yield put(createOrderError(result.message));
    }
  }
}

function* reloadCountries() {
  while (true) {
    const watcher = yield race({
      reload: take(CHANGE_LANGUAGE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    // const expectedLanguage = watcher.reload.lang;

    // someday might load countries with locale info
    yield put(loadCountries());
  }
}

export function* getAddressListWatcher() {
  while (true) {
    const watcher = yield race({
      loadAddressList: take(LOAD_ADDRESS_LIST),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) return;

    yield call(getAddressList);
  }
}

export function* getAddressList() {
  const toCountryIso = yield select(selectToCountryIso());
  const requestURL = `/address-book?limit=-1&to_country_iso=${toCountryIso}`;

  const result = yield call(request, requestURL);

  if (result.success) {
    yield put(loadAddressListSuccess(result.data.list));
  } else {
    console.log(result.message); // eslint-disable-line no-console
    yield put(loadAddressListError(result.message));
  }
}

export function* addAddress() {
  while (true) {
    const watcher = yield race({
      createAddress: take(CREATE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.createAddress.formData;
    const requestURL = '/address-book';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(createAddressSuccess(result.data));
      message.success(i18n.t('Client_create_address_success'), 3);
    } else {
      yield put(createAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* updateAddress() {
  while (true) {
    const watcher = yield race({
      saveAddress: take(SAVE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.saveAddress.formData;
    const requestURL = '/address-book';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(saveAddressSuccess(result.data));
      message.success(i18n.t('Client_update_address_success'), 3);
    } else {
      yield put(saveAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
