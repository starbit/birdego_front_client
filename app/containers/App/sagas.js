import { take, call, put, race, select } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  LOAD_GLOBAL_CONFIG,
  LOGOUT,
  LOAD_CLIENT_INFO,
  CHANGE_LANGUAGE,
  LOAD_COUNTRIES,
  LOAD_WAREHOUSE_ADDRESS,
} from './constants';
import {
  setRequireAuth,
  loadConfigSuccess,
  logoutSuccess,
  loadClientInfo,
  loadClientInfoSuccess,
  changeLanguage,
  changeLanguageSuccess,
  changeLanguageError,
  loadConfigError,
  loadClientInfoError,
  logoutError,
  loadCountriesSuccess,
  loadCountriesError,
  loadWarehouseAddressSuccess,
  loadWarehouseAddressError,
} from './actions';
import request from 'utils/request';
import { browserHistory } from 'react-router';
import i18n from 'i18next';
import {
  selectRequireAuth,
  selectCountries,
  selectCurrentLanguage,
} from './selectors';
import { message } from 'antd';

// Bootstrap sagas
export default [
  getGlobalConfig,
  getInfo,
  logout,
  updateLocale,
  getCountriesWatcher,
  getWarehouseAddress,
];

// Individual exports for testing
export function* getGlobalConfig() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadGlobalConfig: take(LOAD_GLOBAL_CONFIG),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/public/global-config';

    const result = yield call(request, requestURL);

    if (result.success) {
      const config = result.config;
      const requireAuth = yield select(selectRequireAuth());

      const currentUser = config.userHash === '{}' ? false : config.userHash;
      delete config.userHash;
      yield put(loadConfigSuccess(currentUser, config));

      const currentLanguage = yield select(selectCurrentLanguage());
      if (config.locale && config.locale !== currentLanguage) {
        yield put(changeLanguage(config.locale));
      }

      if (currentUser) {
        yield put(loadClientInfo(currentUser.client_id));
      } else if (requireAuth) {
        yield put(setRequireAuth(false));
        message.error(i18n.t('Client_please_login_first', 5));
        browserHistory.push('/client/login');
      }
    } else {
      yield put(loadConfigError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* updateLocale() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      changeLanguage: take(CHANGE_LANGUAGE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const expectedLanguage = watcher.changeLanguage.lang;

    const requestURL = `/public/change-locale?locale=${expectedLanguage}`;

    const result = yield call(request, requestURL);

    if (result.success) {
      yield put(changeLanguageSuccess(expectedLanguage));
    } else {
      yield put(changeLanguageError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getInfo() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadClientInfo: take(LOAD_CLIENT_INFO),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = `/client?id=${watcher.loadClientInfo.id}`;

    const result = yield call(request, requestURL);

    if (result.success) {
      yield put(loadClientInfoSuccess(result.data));
    } else {
      yield put(loadClientInfoError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

// Individual exports for testing
export function* logout() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      logout: take(LOGOUT),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/public/logout';
    const requestConfig = {
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(logoutSuccess());
      browserHistory.push('/client');
    } else {
      yield put(logoutError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* getCountriesWatcher() {
  let watcher = yield race({
    loadCountries: take(LOAD_COUNTRIES),
    stop: take(LOCATION_CHANGE), // stop watching if user leaves page
  });

  if (watcher.stop) return;

  yield call(getCountries);

  while (true) { // eslint-disable-line
    watcher = yield race({
      loadCountries: take(LOAD_COUNTRIES),
      reload: take(CHANGE_LANGUAGE),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    if (watcher.loadCountries) {
      const countries = yield select(selectCountries());

      if (!countries) {
        yield call(getCountries);
      } else {
        yield put(loadCountriesSuccess(countries));
      }
    }

    if (watcher.reload) {
      const locale = watcher.reload.lang;
      yield call(getCountries, locale);
    }
  }
}

export function* getCountries(locale = undefined) {
  let requestURL = '/country?limit=-1';
  if (locale) {
    requestURL += `&locale=${locale}`;
  }

  const result = yield call(request, requestURL);

  if (result.success) {
    yield put(loadCountriesSuccess(result.data.list));
  } else {
    yield put(loadCountriesError());
    console.log(result.message); // eslint-disable-line no-console
  }
}

export function* getWarehouseAddress() {
  while (true) { // eslint-disable-line
    const watcher = yield race({
      loadWarehouseAddress: take(LOAD_WAREHOUSE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = `/warehouse/shipping-address?consignment_id=${watcher.loadWarehouseAddress.orderId}`;

    const result = yield call(request, requestURL);

    if (result.success) {
      yield put(loadWarehouseAddressSuccess(result.data));
    } else {
      yield put(loadWarehouseAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
