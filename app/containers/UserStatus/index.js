/*
 *
 * UserStatus
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Link } from 'react-router';
import { Dropdown, Menu, Icon, Spin } from 'antd';
import styles from './styles.css';
import { selectCurrentUser, selectSiteName, selectConfigLoadingStatus } from 'containers/App/selectors';
import { logout } from 'containers/App/actions';
import { translate } from 'react-i18next';

export class UserStatus extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { t } = this.props;
    let links;
    if (this.props.loadingConfig) {
      links = (<Spin size="small" />);
    } else if (this.props.currentUser) {
      const menu = (
        <Menu>
          <Menu.Item>
            <Link to="/client/account">{t('Client_account')}</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/client/orders">{t('Client_orders')}</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/client/top-up">{t('Client_top_up')}</Link>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item>
            <a onClick={this.props.logout}>{t('Client_logout')}</a>
          </Menu.Item>
        </Menu>
      );
      links = (
        <div>
          <span>{this.props.siteName}</span>
          <span className={styles.orders}><Link to="/client/orders">{t('Client_orders')}</Link></span>
          <Dropdown overlay={menu} trigger={['click']}>
            <span className={styles.account}>
              <span className={styles.userName}>{this.props.currentUser.username}</span>
              <Icon className={styles.icon} type="down" />
            </span>
          </Dropdown>
        </div>
      );
    } else {
      links = (
        <div>
          <Link to="/client/signup">{t('Client_signup')}</Link>
          <span className={styles.separator}>|</span>
          <Link to="/client/login">{t('Client_login')}</Link>
        </div>
      );
    }

    return (
      <div>
        {links}
      </div>
    );
  }
}

UserStatus.propTypes = {
  currentUser: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  logout: React.PropTypes.func,
  siteName: React.PropTypes.string,
  loadingConfig: React.PropTypes.bool,
  t: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectCurrentUser(),
  selectSiteName(),
  selectConfigLoadingStatus(),
  (currentUser, siteName, loadingConfig) => ({ currentUser, siteName, loadingConfig })
);

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(UserStatus));
