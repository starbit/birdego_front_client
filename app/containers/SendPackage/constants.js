/*
 *
 * SendPackage constants
 *
 */
export const SET_STEP = 'Client/SendPackage/SET_STEP';

// form data 1/3 - shipment
export const ADD_PACKAGE = 'Client/SendPackage/Shipment/ADD_PACKAGE';
export const REMOVE_PACKAGE = 'Client/SendPackage/Shipment/REMOVE_PACKAGE';

export const CHANGE_SHIPMENT_FORM = 'Client/SendPackage/Shipment/CHANGE_SHIPMENT_FORM';
export const CLEAR_SHIPMENT_FORM = 'Client/SendPackage/Shipment/CLEAR_SHIPMENT_FORM';

// form data 2/3 - quotes
export const CHANGE_DELIVERY_SERVICE = 'Client/SendPackage/Quotes/CHANGE_DELIVERY_SERVICE';
export const CLEAR_DELIVERY_SERVICE = 'Client/SendPackage/Quotes/CLEAR_DELIVERY_SERVICE';

// form data 3/3 - create order
export const CHANGE_ORDER_FORM = 'Client/SendPackage/Order/CHANGE_ORDER_FORM';
export const CLEAR_ORDER_FORM = 'Client/SendPackage/Order/CLEAR_ORDER_FORM';
