import { createSelector } from 'reselect';

/**
 * Direct selector to the Account state domain
 */
const selectAccountDomain = () => (state) => state.get('account');

/**
 * Other specific selectors
 */

/**
 * Default selector
 */
const selectAccount = () => createSelector(
  selectAccountDomain(),
  (substate) => substate.toJS()
);

export default selectAccount;
export {
  selectAccountDomain,
};
