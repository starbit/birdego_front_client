/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */
import React from 'react';

// Import the CSS reset, which HtmlWebpackPlugin transfers to the build folder
import 'sanitize.css/sanitize.css';

import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import enUS from 'antd/lib/locale-provider/en_US';

import { changeLanguage, loadGlobalConfig, showTrackPackageForm, hideTrackPackageForm } from './actions';
import { selectCurrentLanguage, selectTrackPackageFormOpen } from './selectors';

import TrackPackageAffix from 'components/TrackPackageAffix';
import Footer from 'components/Footer';
import Header from 'components/Header';
import NavBar from 'components/NavBar';

import { LocaleProvider } from 'antd';

import 'antd/dist/antd.css';
import styles from './styles.css';

class App extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadGlobalConfig();
  }
  render() {
    let antdLang = null;
    if (this.props.currentLanguage === 'en_GB') {
      antdLang = enUS;
    }
    return (
      <div className={styles.wrapper}>
        <LocaleProvider locale={antdLang}>
          <div>
            <Header changeLanguage={this.props.changeLanguage} currentLanguage={this.props.currentLanguage} />

            <NavBar />

            <div className={styles.content}>
              {this.props.children}
            </div>

            <Footer />

            <TrackPackageAffix
              showTrackPackageForm={this.props.showTrackPackageForm}
              hideTrackPackageForm={this.props.hideTrackPackageForm}
              trackPackageFormOpen={this.props.trackPackageFormOpen}
            />

          </div>
        </LocaleProvider>
      </div>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.node,
  changeLanguage: React.PropTypes.func,
  loadGlobalConfig: React.PropTypes.func,
  currentLanguage: React.PropTypes.string,
  showTrackPackageForm: React.PropTypes.func,
  hideTrackPackageForm: React.PropTypes.func,
  trackPackageFormOpen: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectCurrentLanguage(),
  selectTrackPackageFormOpen(),
  (currentLanguage, trackPackageFormOpen) => ({ currentLanguage, trackPackageFormOpen })
);

function mapDispatchToProps(dispatch) {
  return {
    changeLanguage: (lang) => dispatch(changeLanguage(lang)),
    showTrackPackageForm: () => dispatch(showTrackPackageForm()),
    hideTrackPackageForm: () => dispatch(hideTrackPackageForm()),
    loadGlobalConfig: () => dispatch(loadGlobalConfig()),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
