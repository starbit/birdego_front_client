/*
 *
 * Orders actions
 *
 */

import {
  LOAD_ORDER_COUNTS,
  LOAD_ORDER_COUNTS_SUCCESS,
  LOAD_ORDER_COUNTS_ERROR,
  LOAD_ORDERS,
  LOAD_ORDERS_SUCCESS,
  LOAD_ORDERS_ERROR,
  PRINT_BARCODE,
  LOAD_ORDER_SUCCESS,
  LOAD_ORDER_ERROR,
  LOAD_ORDER_DETAIL,
  REMOVE_ORDER,
  REMOVE_ORDER_SUCCESS,
  REMOVE_ORDER_ERROR,
  CANCEL_ORDER,
  CANCEL_ORDER_SUCCESS,
  CANCEL_ORDER_ERROR,
  SHOW_WAREHOUSE_ADDRESS_MODAL,
  HIDE_WAREHOUSE_ADDRESS_MODAL,
  LOAD_ORDER_TRACE,
  LOAD_ORDER_TRACE_SUCCESS,
  LOAD_ORDER_TRACE_ERROR,
} from './constants';

export function loadOrderCounts() {
  return {
    type: LOAD_ORDER_COUNTS,
  };
}

export function loadOrderCountsSuccess(orderCounts) {
  return {
    type: LOAD_ORDER_COUNTS_SUCCESS,
    orderCounts,
  };
}

export function loadOrderCountsError() {
  return {
    type: LOAD_ORDER_COUNTS_ERROR,
  };
}

export function loadOrders(pagination) {
  return {
    type: LOAD_ORDERS,
    pagination,
  };
}

export function loadOrdersSuccess(pagination, orders) {
  return {
    type: LOAD_ORDERS_SUCCESS,
    pagination,
    orders,
  };
}

export function loadOrdersError(status) {
  return {
    type: LOAD_ORDERS_ERROR,
    status,
  };
}

export function loadOrderDetail(id) {
  return {
    type: LOAD_ORDER_DETAIL,
    id,
  };
}

export function loadOrderSuccess(order) {
  return {
    type: LOAD_ORDER_SUCCESS,
    order,
  };
}

export function loadOrderError(id) {
  return {
    type: LOAD_ORDER_ERROR,
    id,
  };
}

export function printBarcode(id) {
  return {
    type: PRINT_BARCODE,
    id,
  };
}

export function removeOrder(pagination, order) {
  return {
    type: REMOVE_ORDER,
    pagination,
    order,
  };
}

export function removeOrderSuccess(originalStatus, order) {
  return {
    type: REMOVE_ORDER_SUCCESS,
    originalStatus,
    order,
  };
}

export function removeOrderError(order) {
  return {
    type: REMOVE_ORDER_ERROR,
    order,
  };
}

export function cancelOrder(pagination, order) {
  return {
    type: CANCEL_ORDER,
    pagination,
    order,
  };
}

export function cancelOrderSuccess(originalStatus, order) {
  return {
    type: CANCEL_ORDER_SUCCESS,
    originalStatus,
    order,
  };
}

export function cancelOrderError(order) {
  return {
    type: CANCEL_ORDER_ERROR,
    order,
  };
}

export function showWarehouseAddressModal() {
  return {
    type: SHOW_WAREHOUSE_ADDRESS_MODAL,
  };
}

export function hideWarehouseAddressModal() {
  return {
    type: HIDE_WAREHOUSE_ADDRESS_MODAL,
  };
}

export function loadOrderTrace(orderId) {
  return {
    type: LOAD_ORDER_TRACE,
    orderId,
  };
}

export function loadOrderTraceSuccess(orderId, orderTrace) {
  return {
    type: LOAD_ORDER_TRACE_SUCCESS,
    orderId,
    orderTrace,
  };
}

export function loadOrderTraceError() {
  return {
    type: LOAD_ORDER_TRACE_ERROR,
  };
}
