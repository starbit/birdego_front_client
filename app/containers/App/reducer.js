/*
 *
 * Global reducer
 *
 */

import { fromJS } from 'immutable';
import i18next from 'i18next';

import {
  CHANGE_LANGUAGE,
  CHANGE_LANGUAGE_ERROR,
  SAVE_CALLBACK_ROUTE,
  CLEAR_CALLBACK_ROUTE,
  LOAD_GLOBAL_CONFIG,
  LOAD_CONFIG_SUCCESS,
  LOAD_CLIENT_INFO_SUCCESS,
  SET_REQUIRE_AUTH,
  LOAD_CLIENT_INFO_ERROR,
  LOAD_CONFIG_ERROR,
  LOAD_COUNTRIES,
  LOAD_COUNTRIES_SUCCESS,
  LOAD_COUNTRIES_ERROR,
  SHOW_TRACK_PACKAGE_FORM,
  HIDE_TRACK_PACKAGE_FORM,
  LOAD_WAREHOUSE_ADDRESS,
  LOAD_WAREHOUSE_ADDRESS_SUCCESS,
  LOAD_WAREHOUSE_ADDRESS_ERROR,
} from './constants';

const initialState = fromJS({
  lang: localStorage.getItem('clientLang') || 'en_GB',
  callbackRoute: false,
  requireAuth: false,
  config: false,
  loadingConfig: false,
  currentUser: false,
  clientInfo: false,
  countries: false,
  loadingCountries: false,
  trackPackageFormOpen: false,
  loadingWarehouseAddress: false,
  warehouseAddress: false,
});

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_TRACK_PACKAGE_FORM:
      return state.set('trackPackageFormOpen', true);
    case HIDE_TRACK_PACKAGE_FORM:
      return state.set('trackPackageFormOpen', false);
    case SET_REQUIRE_AUTH:
      return state.set('requireAuth', action.requireAuth);
    case CHANGE_LANGUAGE:
      i18next.changeLanguage(action.lang);
      localStorage.setItem('clientLang', action.lang);
      return state
        .set('lang', action.lang)
        .set('loadingCountries', true);
    case CHANGE_LANGUAGE_ERROR:
      return state.set('loadingCountries', false);
    case SAVE_CALLBACK_ROUTE:
      return state.set('callbackRoute', action.callbackRoute);
    case CLEAR_CALLBACK_ROUTE:
      return state.set('callbackRoute', false);
    case LOAD_GLOBAL_CONFIG:
      return state.set('loadingConfig', true);
    case LOAD_CONFIG_SUCCESS:
      return state.set('currentUser', action.currentUser)
        .set('config', action.config)
        .set('loadingConfig', false);
    case LOAD_CONFIG_ERROR:
      return state
        .set('currentUser', false)
        .set('config', false)
        .set('loadingConfig', false);
    case LOAD_CLIENT_INFO_SUCCESS:
      return state.set('clientInfo', action.info);
    case LOAD_CLIENT_INFO_ERROR:
      return state.set('clientInfo', false);
    case LOAD_COUNTRIES:
      return state.set('loadingCountries', true);
    case LOAD_COUNTRIES_SUCCESS:
      return state
        .set('countries', action.countries)
        .set('loadingCountries', false);
    case LOAD_COUNTRIES_ERROR:
      return state
        .set('countries', false)
        .set('loadingCountries', false);
    case LOAD_WAREHOUSE_ADDRESS:
      return state
        .set('loadingWarehouseAddress', true)
        .set('warehouseAddress', false);
    case LOAD_WAREHOUSE_ADDRESS_SUCCESS:
      return state
        .set('warehouseAddress', action.address)
        .set('loadingWarehouseAddress', false);
    case LOAD_WAREHOUSE_ADDRESS_ERROR:
      return state.set('loadingWarehouseAddress', false);
    default:
      return state;
  }
}

export default globalReducer;
