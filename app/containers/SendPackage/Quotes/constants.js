/*
 *
 * Quotes constants
 *
 */

export const LOAD_QUOTES = 'Client/SendPackage/Quotes/LOAD_QUOTES';
export const LOAD_QUOTES_SUCCESS = 'Client/SendPackage/Quotes/LOAD_QUOTES_SUCCESS';
export const LOAD_QUOTES_ERROR = 'Client/SendPackage/Quotes/LOAD_QUOTES_ERROR';
