/*
 *
 * Order actions
 *
 */

import {
  CREATE_ORDER,
  CREATE_ORDER_SUCCESS,
  CREATE_ORDER_ERROR,
  OPEN_ADDRESS_LIST,
  CLOSE_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_ERROR,
  LOAD_ADDRESS_LIST_SUCCESS,
  LOAD_ADDRESS_LIST,
  CHOOSE_ADDRESS,
  UNCHOOSE_ADDRESS,
  SAVE_ADDRESS,
  SAVE_ADDRESS_ERROR,
  SAVE_ADDRESS_SUCCESS,
  CREATE_ADDRESS,
  CREATE_ADDRESS_SUCCESS,
  CREATE_ADDRESS_ERROR,
} from './constants';

export function createOrder(formData) {
  return {
    type: CREATE_ORDER,
    formData,
  };
}

export function createOrderSuccess(order) {
  return {
    type: CREATE_ORDER_SUCCESS,
    order,
  };
}

export function createOrderError(error) {
  return {
    type: CREATE_ORDER_ERROR,
    error,
  };
}

export function openAddressList() {
  return {
    type: OPEN_ADDRESS_LIST,
  };
}

export function closeAddressList() {
  return {
    type: CLOSE_ADDRESS_LIST,
  };
}

export function loadAddressList() {
  return {
    type: LOAD_ADDRESS_LIST,
  };
}

export function loadAddressListSuccess(addresses) {
  return {
    type: LOAD_ADDRESS_LIST_SUCCESS,
    addresses,
  };
}

export function loadAddressListError() {
  return {
    type: LOAD_ADDRESS_LIST_ERROR,
  };
}

export function chooseAddress(address) {
  return {
    type: CHOOSE_ADDRESS,
    address,
  };
}

export function unchooseAddress() {
  return {
    type: UNCHOOSE_ADDRESS,
  };
}

export function createAddress(formData) {
  return {
    type: CREATE_ADDRESS,
    formData,
  };
}

export function createAddressSuccess() {
  return {
    type: CREATE_ADDRESS_SUCCESS,
  };
}

export function createAddressError() {
  return {
    type: CREATE_ADDRESS_ERROR,
  };
}

export function saveAddress(formData) {
  return {
    type: SAVE_ADDRESS,
    formData,
  };
}

export function saveAddressSuccess(address) {
  return {
    type: SAVE_ADDRESS_SUCCESS,
    address,
  };
}

export function saveAddressError() {
  return {
    type: SAVE_ADDRESS_ERROR,
  };
}
