/*
 *
 * AddressBook constants
 *
 */

export const OPEN_NEW_ADDRESS_FORM = 'Client/Account/AddressBook/OPEN_NEW_ADDRESS_FORM';
export const CLOSE_NEW_ADDRESS_FORM = 'Client/Account/AddressBook/CLOSE_NEW_ADDRESS_FORM';

export const LOAD_ADDRESS_LIST = 'Client/Account/AddressBook/LOAD_ADDRESS_LIST';
export const LOAD_ADDRESS_LIST_SUCCESS = 'Client/Account/AddressBook/LOAD_ADDRESS_LIST_SUCCESS';
export const LOAD_ADDRESS_LIST_ERROR = 'Client/Account/AddressBook/LOAD_ADDRESS_LIST_ERROR';

export const REMOVE_ADDRESS = 'Client/Account/AddressBook/REMOVE_ADDRESS';
export const REMOVE_ADDRESS_SUCCESS = 'Client/Account/AddressBook/REMOVE_ADDRESS_SUCCESS';
export const REMOVE_ADDRESS_ERROR = 'Client/Account/AddressBook/REMOVE_ADDRESS_ERROR';

export const CREATE_ADDRESS = 'Client/Account/AddressBook/CREATE_ADDRESS';
export const CREATE_ADDRESS_SUCCESS = 'Client/Account/AddressBook/CREATE_ADDRESS_SUCCESS';
export const CREATE_ADDRESS_ERROR = 'Client/Account/AddressBook/CREATE_ADDRESS_ERROR';

export const SAVE_ADDRESS = 'Client/Account/AddressBook/SAVE_ADDRESS';
export const SAVE_ADDRESS_SUCCESS = 'Client/Account/AddressBook/SAVE_ADDRESS_SUCCESS';
export const SAVE_ADDRESS_ERROR = 'Client/Account/AddressBook/SAVE_ADDRESS_ERROR';

export const EDIT_ADDRESS = 'Client/Account/AddressBook/EDIT_ADDRESS';
export const RESET_ADDRESS_EDITING = 'Client/Account/AddressBook/RESET_ADDRESS_EDITING';
