/**
 * A link to a certain page, an anchor tag
 */

import React, { PropTypes } from 'react';

import styles from './styles.css';

function A({ ...props }) {
  const { children, href, target, active, ...rest } = props;
  const className = props.className || styles.link;
  let activeClass = '';
  if (active) {
    activeClass = styles.active;
  }
  return (
    <a
      href={href}
      target={target}
      className={`${className} ${activeClass}`}
      {...rest}
    >
      {children}
    </a>
  );
}

A.propTypes = {
  className: PropTypes.string,
  active: React.PropTypes.bool,
  href: PropTypes.string,
  target: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default A;
