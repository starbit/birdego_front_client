/*
 *
 * PaymentRecords
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';
import { Form, Table, Icon } from 'antd';
import { translate } from 'react-i18next';
import {
  selectRecords,
  selectLoading,
} from './selectors';
import { loadRecords } from './actions';
import { createSelector } from 'reselect';
import Menu from 'components/Menu';

class PaymentRecords extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.loadRecords();
  }
  render() {
    const { t } = this.props;
    const columns = [{
      title: t('Client_amount'),
      dataIndex: 'amount',
    }, {
      title: t('Client_balance'),
      dataIndex: 'balance',
    }, {
      title: t('Client_type'),
      dataIndex: 'type',
      render: (text) => <span>{t(`Client_${text}`)}</span>,
    }, {
      title: t('Client_update_time'),
      dataIndex: 'update_time',
    }, {
      title: t('Client_note'),
      dataIndex: 'note',
    }, {
      title: (<Icon className={styles.reloadIcon} type="reload" onClick={this.props.loadRecords} />),
    }];
    return (
      <div>
        <aside className={styles.antLayoutSider}>
          <Menu selected="4" />
        </aside>
        <div className={styles.antLayoutContent}>
          <Table
            columns={columns}
            dataSource={this.props.records}
            rowKey={record => record.id}
            className={styles.table}
            loading={this.props.loading}
          />
        </div>
      </div>
    );
  }
}

PaymentRecords.propTypes = {
  loadRecords: React.PropTypes.func,
  records: React.PropTypes.array,
  t: React.PropTypes.func,
  loading: React.PropTypes.bool,
};

const mapStateToProps = createSelector(
  selectRecords(),
  selectLoading(),
  (records, loading) => ({ records: records.toJS(), loading })
);

function mapDispatchToProps(dispatch) {
  return {
    loadRecords: () => dispatch(loadRecords()),
    dispatch,
  };
}

const newPaymentRecords = Form.create({})(PaymentRecords);

export { newPaymentRecords as PaymentRecords };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newPaymentRecords));

