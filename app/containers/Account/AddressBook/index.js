/*
 *
 * AddressBook
 *
 */

import React from 'react';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import selectAddressBook from './selectors';
import styles from './styles.css';
import Menu from 'components/Menu';
import NewAddressForm from './NewAddressForm';
import AddressList from './AddressList';

class AddressBook extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={styles.addressBook}>
        <aside className={styles.antLayoutSider}>
          <Menu selected="3" />
        </aside>
        <div className={styles.antLayoutContent}>
          <NewAddressForm />
          <AddressList />
        </div>
      </div>
    );
  }
}

AddressBook.propTypes = {
  t: React.PropTypes.func,
};

const mapStateToProps = selectAddressBook();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(AddressBook));
