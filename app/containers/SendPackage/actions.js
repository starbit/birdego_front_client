/*
 *
 * SendPackage actions
 *
 */
import {
  SET_STEP,
  ADD_PACKAGE,
  REMOVE_PACKAGE,
  CHANGE_SHIPMENT_FORM,
  CLEAR_SHIPMENT_FORM,
  CHANGE_DELIVERY_SERVICE,
  CLEAR_DELIVERY_SERVICE,
  CHANGE_ORDER_FORM,
  CLEAR_ORDER_FORM,
} from './constants';

export function setStep(step) {
  return {
    type: SET_STEP,
    step,
  };
}

export function addPackage() {
  return {
    type: ADD_PACKAGE,
  };
}

export function removePackage(key) {
  return {
    type: REMOVE_PACKAGE,
    key,
  };
}

export function changeShipmentForm(shipmentPayload) {
  return {
    type: CHANGE_SHIPMENT_FORM,
    shipmentPayload,
  };
}

export function clearShipmentForm() {
  return {
    type: CLEAR_SHIPMENT_FORM,
  };
}

export function changeDeliveryService(deliveryService) {
  return {
    type: CHANGE_DELIVERY_SERVICE,
    deliveryService,
  };
}

export function clearDeliveryService() {
  return {
    type: CLEAR_DELIVERY_SERVICE,
  };
}


export function changeOrderForm(orderPayload) {
  return {
    type: CHANGE_ORDER_FORM,
    orderPayload,
  };
}

export function clearOrderForm() {
  return {
    type: CLEAR_ORDER_FORM,
  };
}
