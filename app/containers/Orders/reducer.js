/*
 *
 * Orders reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_ORDER_COUNTS_SUCCESS,
  LOAD_ORDERS_SUCCESS,
  LOAD_ORDERS_ERROR,
  LOAD_ORDER_SUCCESS,
  LOAD_ORDER_ERROR,
  LOAD_ORDERS,
  LOAD_ORDER_DETAIL,
  REMOVE_ORDER_SUCCESS,
  CANCEL_ORDER_SUCCESS,
  SHOW_WAREHOUSE_ADDRESS_MODAL,
  HIDE_WAREHOUSE_ADDRESS_MODAL,
  LOAD_ORDER_TRACE,
  LOAD_ORDER_TRACE_SUCCESS,
  LOAD_ORDER_TRACE_ERROR,
} from './constants';

const initialState = fromJS({
  orderCounts: fromJS({}),
  orders: fromJS({}),
  pagination: fromJS({}),
  details: fromJS({}),
  loadingOrders: false,
  loadingOrder: fromJS({}),
  warehouseAddressModalShow: false,
  traces: fromJS({}),
  loadingTrace: false,
});

function ordersReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_ORDER_COUNTS_SUCCESS:
      return state.set('orderCounts', fromJS(action.orderCounts));
    case LOAD_ORDERS:
      return state.set('loadingOrders', true);
    case LOAD_ORDER_DETAIL:
      return state.setIn(['loadingOrder', action.id], true);
    case LOAD_ORDERS_SUCCESS:
      return state
        .setIn(['orders', action.pagination.status], fromJS(action.orders))
        .setIn(['pagination', action.pagination.status], fromJS(action.pagination))
        .set('loadingOrders', false);
    case LOAD_ORDERS_ERROR:
      return state
        .setIn(['orders', action.status], undefined)
        .setIn(['pagination', action.status], undefined)
        .set('loadingOrders', false);
    case LOAD_ORDER_SUCCESS:
      return state
        .setIn(['details', action.order.id], action.order)
        .setIn(['loadingOrder', action.order.id], false);
    case LOAD_ORDER_ERROR:
      return state
        .setIn(['details', action.id], false)
        .setIn(['loadingOrder', action.id], false);
    case REMOVE_ORDER_SUCCESS:
      return state;
        // .setIn(['orders', action.originalStatus], state.getIn(['orders', action.originalStatus]).filter(order => order.id !== action.order.id))
        // .setIn(['pagination', action.DELETED, 'total'], state.getIn(['pagination', action.DELETED, 'total']) + 1);
    case CANCEL_ORDER_SUCCESS:
      return state;
      //   .setIn()
      //   .setIn();
    case SHOW_WAREHOUSE_ADDRESS_MODAL:
      return state.set('warehouseAddressModalShow', true);
    case HIDE_WAREHOUSE_ADDRESS_MODAL:
      return state.set('warehouseAddressModalShow', false);
    case LOAD_ORDER_TRACE:
      return state.set('loadingTrace', true);
    case LOAD_ORDER_TRACE_SUCCESS:
      return state
        .setIn(['traces', action.orderId], fromJS(action.orderTrace))
        .set('loadingTrace', false);
    case LOAD_ORDER_TRACE_ERROR:
      return state
        .setIn(['traces', action.orderId], false)
        .set('loadingTrace', false); // TODO: delete the record
    default:
      return state;
  }
}

export default ordersReducer;
