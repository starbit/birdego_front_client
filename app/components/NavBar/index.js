/**
*
* NavBar
*
*/

import React from 'react';
import { Row, Col, Icon } from 'antd';
import styles from './styles.css';
import { Link } from 'react-router';
import { translate } from 'react-i18next';
import Img from 'components/Img';
import LogoImage from './logo.svg';

function NavBar(props) {
  const { t } = props;
  return (
    <div className={styles.navBar}>
      <Row type="flex" align="middle" justify="space-between">
        <Col className={styles.logo}>
          <Link to="/client">
            <Img alt="Bird eGo" src={LogoImage} className={styles.image} />
          </Link>
          <span className={styles.separator}>|</span>
          <span className={styles.slogan}>{t('Client_slogan')}</span>
        </Col>
        <Col className={styles.shipment}>
          <Link to="/client/shipment">
            <Icon type="shopping-cart" className={styles.icon} />
            {t('Client_send_package')}
          </Link>
        </Col>
      </Row>
    </div>
  );
}

NavBar.propTypes = {
  t: React.PropTypes.func,
};

export default translate()(NavBar);
