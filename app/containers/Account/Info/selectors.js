import { createSelector } from 'reselect';

/**
 * Direct selector to the Info state domain
 */
const selectAccountInfoDomain = () => (state) => state.get('accountInfo');

/**
 * Other specific selectors
 */

export {
  selectAccountInfoDomain,
};
