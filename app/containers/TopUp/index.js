/*
 *
 * Top up
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import styles from './styles.css';
import { Form, Input, Row, Button, Card, message } from 'antd';
import { translate } from 'react-i18next';
import { topUp } from './actions';
import { selectConfig } from 'containers/App/selectors';

const FormItem = Form.Item;
const currencyRegex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;

class TopUp extends React.Component { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (evt) => {
    const { t } = this.props;
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    this.props.form.validateFields((error, value) => {
      if (error) {
        message.error(t('Client_hint_please_check_the_form'));
      } else {
        const formData = this.props.form.getFieldsValue();
        this.props.handleSubmit(formData);
      }
    });
  };
  checkCurrency = (rule, value, callback) => {
    const { t } = this.props;
    if (!(currencyRegex.test(value) && value > '0.00')) {
      callback(new Error(t('Client_hint_please_input_valid_amount')));
    } else {
      callback();
    }
  };
  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    return (
      <div className={styles.login}>
        <Row type="flex" justify="center">
          <Card title={t('Client_top_up')} className={styles.card}>
            <Form horizontal onSubmit={this.handleSubmit} className={styles.form}>
              <FormItem
                {...formItemLayout}
                label={t('Client_amount')}
              >
                <Input
                  {...getFieldProps('amount', { rules: [{ required: true, message: t('Client_hint_required') }, {
                    validator: this.checkCurrency }] })}
                  addonBefore={this.props.config.currencyCode}
                />
              </FormItem>
              <FormItem>
                <Button type="primary" htmlType="submit">{t('Client_top_up')}</Button>
              </FormItem>
            </Form>
          </Card>
        </Row>
      </div>
    );
  }
}

TopUp.propTypes = {
  form: React.PropTypes.object,
  handleSubmit: React.PropTypes.func,
  config: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  t: React.PropTypes.func,
};

const mapStateToProps = createSelector(
  selectConfig(),
  (config) => ({ config })
);

function mapDispatchToProps(dispatch) {
  return {
    handleSubmit: (formData) => {
      dispatch(topUp(formData));
    },
    dispatch,
  };
}

const newTopUp = Form.create({})(TopUp);

export { newTopUp as TopUp };
export default translate()(connect(mapStateToProps, mapDispatchToProps)(newTopUp));

