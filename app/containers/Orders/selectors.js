import { createSelector } from 'reselect';

/**
 * Direct selector to the Orders state domain
 */
const selectOrdersDomain = () => (state) => state.get('orders');

/**
 * Other specific selectors
 */
const selectOrderCounts = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('orderCounts')
);

const selectOrders = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('orders')
);

const selectPagination = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('pagination')
);

const selectLoadingOrders = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('loadingOrders')
);

const selectLoadingOrder = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('loadingOrder')
);

const selectDetails = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('details')
);

const selectWarehouseAddressModalShow = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('warehouseAddressModalShow')
);

const selectOrderTrace = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('traces')
);

const selectLoadingOrderTrace = () => createSelector(
  selectOrdersDomain(),
  (substate) => substate.get('loadingTrace')
);

export {
  selectOrdersDomain,
  selectOrderCounts,
  selectOrders,
  selectPagination,
  selectDetails,
  selectLoadingOrder,
  selectLoadingOrders,
  selectWarehouseAddressModalShow,
  selectOrderTrace,
  selectLoadingOrderTrace,
};
