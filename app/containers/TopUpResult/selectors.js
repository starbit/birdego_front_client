import { createSelector } from 'reselect';

/**
 * Direct selector to the TopUpResult state domain
 */
const selectTopUpResultDomain = () => (state) => state.get('topUpResult');

/**
 * Other specific selectors
 */

export {
  selectTopUpResultDomain,
};
