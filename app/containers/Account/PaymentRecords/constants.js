/*
 *
 * PaymentRecords constants
 *
 */

export const LOAD_RECORDS = 'Client/Account/PaymentRecords/LOAD_RECORDS';
export const LOAD_RECORDS_SUCCESS = 'Client/Account/PaymentRecords/LOAD_RECORDS_SUCCESS';
export const LOAD_RECORDS_ERROR = 'Client/Account/PaymentRecords/LOAD_RECORDS_ERROR';
