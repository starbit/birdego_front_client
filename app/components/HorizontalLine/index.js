/**
*
* HorizontalLine
*
*/

import React, { PropTypes } from 'react';
import styles from './styles.css';

function HorizontalLine(props) {
  return (
    <div className={styles.horizontalLine} style={{ borderColor: props.color, borderStyle: props.lineStyle }}>
    </div>
  );
}

HorizontalLine.propTypes = {
  color: PropTypes.string,
  lineStyle: PropTypes.string,
};
export default HorizontalLine;
