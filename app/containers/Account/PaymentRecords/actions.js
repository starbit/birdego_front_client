/*
 *
 * PaymentRecords actions
 *
 */

import {
  LOAD_RECORDS,
  LOAD_RECORDS_SUCCESS,
  LOAD_RECORDS_ERROR,
} from './constants';

export function loadRecords() {
  return {
    type: LOAD_RECORDS,
  };
}

export function loadRecordsSuccess(records) {
  return {
    type: LOAD_RECORDS_SUCCESS,
    records,
  };
}

export function loadRecordsError() {
  return {
    type: LOAD_RECORDS_ERROR,
  };
}
