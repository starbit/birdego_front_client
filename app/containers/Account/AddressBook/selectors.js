import { createSelector } from 'reselect';

/**
 * Direct selector to the addressBook state domain
 */
const selectAddressBookDomain = () => (state) => state.get('addressBook');

/**
 * Other specific selectors
 */
const selectNewAddressFormOpen = () => createSelector(
  selectAddressBookDomain(),
  (substate) => substate.get('newAddressFormOpen')
);

const selectAddresses = () => createSelector(
  selectAddressBookDomain(),
  (substate) => substate.get('addresses')
);

const selectAddressEditing = () => createSelector(
  selectAddressBookDomain(),
  (substate) => substate.get('addressEditing')
);

const selectAddressListLoading = () => createSelector(
  selectAddressBookDomain(),
  (substate) => substate.get('addressListLoading')
);

/**
 * Default selector used by AddressBook
 */

const selectAddressBook = () => createSelector(
  selectAddressBookDomain(),
  (substate) => substate.toJS()
);

export default selectAddressBook;
export {
  selectAddressBookDomain,
  selectNewAddressFormOpen,
  selectAddresses,
  selectAddressListLoading,
  selectAddressEditing,
};
