/*
 *
 * Shipment constants
 *
 */

export const LOAD_WAREHOUSES = 'Client/SendPackage/Shipment/LOAD_WAREHOUSES';
export const LOAD_WAREHOUSES_SUCCESS = 'Client/SendPackage/Shipment/LOAD_WAREHOUSES_SUCCESS';
export const LOAD_WAREHOUSES_ERROR = 'Client/SendPackage/Shipment/LOAD_WAREHOUSES_ERROR';
