import expect from 'expect';
import orderReducer from '../reducer';
import { fromJS } from 'immutable';

describe('orderReducer', () => {
  it('returns the initial state', () => {
    expect(orderReducer(undefined, {})).toEqual(fromJS({}));
  });
});
