import { createSelector } from 'reselect';

/**
 * Direct selector to the TopUp state domain
 */
const selectTopUpDomain = () => (state) => state.get('topUp');

/**
 * Other specific selectors
 */

export {
  selectTopUpDomain,
};
