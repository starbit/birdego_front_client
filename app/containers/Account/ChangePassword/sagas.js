import { take, call, race, select, put } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { CHANGE_PASSWORD } from './constants';
import request from 'utils/request';
import i18n from 'i18next';
import { selectCurrentUser } from 'containers/App/selectors';
import { changePasswordSuccess, changePasswordError } from './actions';
import { message } from 'antd';

// Bootstrap sagas
export default [
  updatePassword,
];

// Individual exports for testing
export function* updatePassword() {
  while (true) {
    const watcher = yield race({
      changePassword: take(CHANGE_PASSWORD),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;
    const currentUser = yield select(selectCurrentUser());
    const formData = {
      password: watcher.changePassword.newPassword,
      id: currentUser.id,
    };
    const requestURL = '/client-user';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(changePasswordSuccess());
      message.success(i18n.t('Client_change_password_success'), 3);
    } else {
      yield put(changePasswordError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
