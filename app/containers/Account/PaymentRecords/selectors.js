import { createSelector } from 'reselect';

/**
 * Direct selector to the PaymentRecords state domain
 */
const selectPaymentRecordsDomain = () => (state) => state.get('paymentRecords');

/**
 * Other specific selectors
 */
const selectRecords = () => createSelector(
  selectPaymentRecordsDomain(),
  (substate) => substate.get('records')
);

const selectLoading = () => createSelector(
  selectPaymentRecordsDomain(),
  (substate) => substate.get('loading')
);

export {
  selectPaymentRecordsDomain,
  selectRecords,
  selectLoading,
};
