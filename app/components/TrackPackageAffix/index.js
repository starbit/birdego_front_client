/**
*
* TrackPackageAffix
*
*/
/* eslint-disable */

import React from 'react';
import { translate } from 'react-i18next';
import { Affix, Button, Icon, Form, Input, Row, Col } from 'antd';
import styles from './styles.css';

class TrackPackageAffix extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { t } = this.props;
    const { getFieldProps } = this.props.form;

    let content = (
      <Button className={styles.collapseAffixWrapper} onClick={this.props.showTrackPackageForm}>
        <Icon type="search" className={styles.searchIcon} />
        <span>{t('Client_track_package')}</span>
      </Button>
    );

    if (this.props.trackPackageFormOpen) {
      content = (
        <div className={styles.fullAffixWrapper}>
          <Row type="flex" justify="space-between" align="middle">
            <Col span={8} offset={8} className={styles.searchWrapper}>
              <Input
                {...getFieldProps('secretString')}
                size="large"
                addonAfter={(<span className={styles.searchButton}>{t('Client_track')}</span>)}
                className={styles.input}
                placeholder={t('Client_please_input_track_number')}
              />
            </Col>
            <Col span={6} offset={2} className={styles.closeIcon}>
              <div onClick={this.props.hideTrackPackageForm}>
                <Icon type="cross" />
              </div>
            </Col>
          </Row>
        </div>
      );
    }

    return (
      <div className={styles.trackPackageAffix}>
        <Affix offsetBottom={30}>
          { content }
        </Affix>
      </div>
    );
  }
}

TrackPackageAffix.propTypes = {
  t: React.PropTypes.func,
  form: React.PropTypes.object,
  hideTrackPackageForm: React.PropTypes.func,
  showTrackPackageForm: React.PropTypes.func,
  trackPackageFormOpen: React.PropTypes.bool,
};

const newTrackPackageAffix = Form.create({})(TrackPackageAffix);

export { newTrackPackageAffix as TrackPackageAffix };
export default translate()(newTrackPackageAffix);
