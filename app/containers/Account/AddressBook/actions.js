/*
 *
 * AddressBook actions
 *
 */

import {
  OPEN_NEW_ADDRESS_FORM,
  CLOSE_NEW_ADDRESS_FORM,
  LOAD_ADDRESS_LIST,
  LOAD_ADDRESS_LIST_SUCCESS,
  LOAD_ADDRESS_LIST_ERROR,
  REMOVE_ADDRESS,
  REMOVE_ADDRESS_SUCCESS,
  REMOVE_ADDRESS_ERROR,
  CREATE_ADDRESS,
  CREATE_ADDRESS_ERROR,
  CREATE_ADDRESS_SUCCESS,
  SAVE_ADDRESS,
  SAVE_ADDRESS_SUCCESS,
  SAVE_ADDRESS_ERROR,
  EDIT_ADDRESS,
  RESET_ADDRESS_EDITING,
} from './constants';

export function openNewAddressForm() {
  return {
    type: OPEN_NEW_ADDRESS_FORM,
  };
}

export function closeNewAddressForm() {
  return {
    type: CLOSE_NEW_ADDRESS_FORM,
  };
}

export function loadAddressList() {
  return {
    type: LOAD_ADDRESS_LIST,
  };
}

export function loadAddressListSuccess(addresses) {
  return {
    type: LOAD_ADDRESS_LIST_SUCCESS,
    addresses,
  };
}

export function loadAddressListError() {
  return {
    type: LOAD_ADDRESS_LIST_ERROR,
  };
}

export function removeAddress(id) {
  return {
    type: REMOVE_ADDRESS,
    id,
  };
}

export function removeAddressSuccess() {
  return {
    type: REMOVE_ADDRESS_SUCCESS,
  };
}

export function removeAddressError() {
  return {
    type: REMOVE_ADDRESS_ERROR,
  };
}

export function createAddress(formData) {
  return {
    type: CREATE_ADDRESS,
    formData,
  };
}

export function createAddressSuccess() {
  return {
    type: CREATE_ADDRESS_SUCCESS,
  };
}

export function createAddressError() {
  return {
    type: CREATE_ADDRESS_ERROR,
  };
}

export function saveAddress(formData) {
  return {
    type: SAVE_ADDRESS,
    formData,
  };
}

export function saveAddressSuccess(address) {
  return {
    type: SAVE_ADDRESS_SUCCESS,
    address,
  };
}

export function saveAddressError() {
  return {
    type: SAVE_ADDRESS_ERROR,
  };
}

export function editAddress(address) {
  return {
    type: EDIT_ADDRESS,
    address,
  };
}

export function resetAddressEditing() {
  return {
    type: RESET_ADDRESS_EDITING,
  };
}
