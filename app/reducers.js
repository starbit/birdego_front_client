/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOGOUT_SUCCESS } from 'containers/App/constants';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
  previousPathname: null,
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
        previousPathname: state.getIn(['locationBeforeTransitions', 'pathname']) || '/client',
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers) {
  const appReducer = combineReducers({
    route: routeReducer,
    ...asyncReducers,
  });

  return (state, action) => {
    let newState = state;
    if (action.type === LOGOUT_SUCCESS) {
      newState = undefined; // Reset the state when user logs out
      localStorage.clear(); // Clear localStorage in case a state get its initial value from it
    }
    return appReducer(newState, action);
  };
}
