import { take, race, put, select, call } from 'redux-saga/effects';
import { LOAD_QUOTES } from './constants';
import { loadQuotesSuccess, loadQuotesError } from './actions';
import { LOCATION_CHANGE } from 'react-router-redux';
import { selectShipmentPayload } from '../selectors';
import request from 'utils/request';
import { browserHistory } from 'react-router';
import { message } from 'antd';
import i18n from 'i18next';

// All sagas to be loaded
export default [
  getQuotes,
];

// Individual exports for testing
export function* getQuotes() {
  while (true) {
    const watcher = yield race({
      loadQuotes: take(LOAD_QUOTES),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = '/public/quotes';
    const formData = (yield select(selectShipmentPayload())); // .toJS();
    const packages = formData.get('packages').map(
      pkg => pkg.set('width', pkg.getIn(['width', 'value']))
        .set('length', pkg.getIn(['length', 'value']))
        .set('depth', pkg.getIn(['depth', 'value']))
        .set('weight', pkg.getIn(['weight', 'value']))
        .set('quantity', pkg.getIn(['quantity', 'value']))
    ).toJS();

    const requestConfig = {
      method: 'POST',
      body: {
        from_country_iso: formData.getIn(['from_country_iso', 'value', 'key']),
        to_country_iso: formData.getIn(['to_country_iso', 'value', 'key']),
        packages,
      },
      customHandler: true,
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(loadQuotesSuccess(result.data.list));
    } else if (!result.error) {
      console.log(result); // eslint-disable-line no-console
      if (result.errCode) {
        switch (result.errCode.toString()) {
          // from country not supported
          case '200104':
            message.error(i18n.t('Client_from_country_not_supported'), 5);
            break;
          // to country not supported
          case '200105':
            message.error(i18n.t('Client_to_country_not_supported'), 5);
            break;
          // package sizes not supported
          case '200106':
            message.error(i18n.t('Client_package_size_not_supported'), 5);
            break;
          case '200107':
            message.error(i18n.t('Client_package_size_not_supported'), 5);
            break;
          default:
            message.error(i18n.t('Client_cannot_find_suitable_service'), 5);
        }
        browserHistory.push('/client/shipment');
      }
    } else {
      yield put(loadQuotesError(result.error));
    }
  }
}
