/**
*
* Footer
*
*/

import React from 'react';
import styles from './styles.css';
import { Row, Col } from 'antd';
import LogoImage from './logo.svg';
import Img from 'components/Img';
import { translate } from 'react-i18next';
import PaypalLogo from './paypal.png';

function Footer(props) {
  const { t } = props;
  return (
    <footer className={styles.footer}>
      <Row type="flex" justify="space-around">
        <Col span={3} offset={2}>
          <Img alt="Bird eGo" src={LogoImage} className={styles.logo} />
        </Col>
        <Col span={3}>
          <p className={styles.sectionTitle}>{t('Client_partners')}</p>
          <p>BLA BLA BLA</p>
          <p>BLA BLA BLA</p>
        </Col>
        <Col span={3}>
          <p className={styles.sectionTitle}>{t('Client_company_intro')}</p>
          <p>BLA BLA BLA</p>
          <p>BLA BLA BLA</p>
        </Col>
        <Col span={3}>
          <p className={styles.sectionTitle}>{t('Client_secure_pay')}</p>
          <Img alt="paypal" src={PaypalLogo} className={styles.paypal} />
        </Col>
        <Col span={3}>
          <p className={styles.sectionTitle}>{t('Client_full_track')}</p>
          <p>BLA BLA BLA</p>
          <p>BLA BLA BLA</p>
        </Col>
        <Col span={3}>
          <p className={styles.sectionTitle}>{t('Client_support')}</p>
          <p>BLA BLA BLA</p>
          <p>BLA BLA BLA</p>
        </Col>
      </Row>
      <p className={styles.copyright}>Copyright &copy; 2016 &nbsp; Bird eGo &nbsp;{t('Client_all_rights_reserved')}</p>
    </footer>
  );
}

Footer.propTypes = {
  t: React.PropTypes.func,
};

export default translate()(Footer);
