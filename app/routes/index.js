import { injectAll } from 'utils/asyncInjectors';
import { requireAuth, checkCreateOrderPageRequirements, checkQuotesPageRequirements } from './auth';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

function createSendPackageRoutes(store) {
  return [
    {
      path: 'shipment',
      name: 'shipment',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/SendPackage/Shipment'),
          System.import('containers/SendPackage/Shipment/reducer'),
          System.import('containers/SendPackage/Shipment/sagas'),
        ]).then(injectAll(store, 'shipment', cb)).catch(errorLoading);
      },
    }, {
      path: 'create-order',
      name: 'createOrder',
      onEnter: checkCreateOrderPageRequirements(store),
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/SendPackage/Order'),
          System.import('containers/SendPackage/Order/reducer'),
          System.import('containers/SendPackage/Order/sagas'),
        ]).then(injectAll(store, 'createOrder', cb)).catch(errorLoading);
      },
    }, {
      path: 'quotes',
      name: 'quotes',
      onEnter: checkQuotesPageRequirements(store),
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/SendPackage/Quotes'),
          System.import('containers/SendPackage/Quotes/reducer'),
          System.import('containers/SendPackage/Quotes/sagas'),
        ]).then(injectAll(store, 'quotes', cb)).catch(errorLoading);
      },
    },
  ];
}

function createAccountRoutes(store) {
  return [
    {
      path: 'info',
      name: 'accountInfo',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Account/Info'),
          System.import('containers/Account/Info/reducer'),
          System.import('containers/Account/Info/sagas'),
        ]).then(injectAll(store, 'accountInfo', cb)).catch(errorLoading);
      },
    },
    {
      path: 'password',
      name: 'changePassword',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Account/ChangePassword'),
          System.import('containers/Account/ChangePassword/reducer'),
          System.import('containers/Account/ChangePassword/sagas'),
        ]).then(injectAll(store, 'changePassword', cb)).catch(errorLoading);
      },
    }, {
      path: 'payment-records',
      name: 'paymentRecords',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Account/PaymentRecords'),
          System.import('containers/Account/PaymentRecords/reducer'),
          System.import('containers/Account/PaymentRecords/sagas'),
        ]).then(injectAll(store, 'paymentRecords', cb)).catch(errorLoading);
      },
    }, {
      path: 'address-book',
      name: 'addressBook',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Account/AddressBook'),
          System.import('containers/Account/AddressBook/reducer'),
          System.import('containers/Account/AddressBook/sagas'),
        ]).then(injectAll(store, 'addressBook', cb)).catch(errorLoading);
      },
    },
  ];
}

function createOrdersRoutes() {
  return [
    {
      path: ':orderId/trace',
      name: 'ordersTrace',
      getComponent: (nextState, cb) => {
        System.import('containers/Orders/OrderTrace')
          .then((component) => cb(null, component.default)).catch(errorLoading);
      },
    },
  ];
}

function createClientRoutes(store) {
  return [
    {
      path: '/',
      name: 'homePage',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/HomePage'),
          System.import('containers/HomePage/reducer'),
          System.import('containers/HomePage/sagas'),
        ]).then(injectAll(store, 'homePage', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client',
      name: 'clientHomePage',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/HomePage'),
          System.import('containers/HomePage/reducer'),
          System.import('containers/HomePage/sagas'),
        ]).then(injectAll(store, 'homePage', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client/',
      name: 'sendPackage',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/SendPackage'),
          System.import('containers/SendPackage/reducer'),
          System.import('containers/SendPackage/sagas'),
        ]).then(injectAll(store, 'sendPackage', cb)).catch(errorLoading);
      },
      childRoutes: createSendPackageRoutes(store),
      indexRoute: { onEnter: (nextState, replace) => replace('/client/shipment') },
    },
    {
      path: '/client/login',
      name: 'login',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Login'),
          System.import('containers/Login/reducer'),
          System.import('containers/Login/sagas'),
        ]).then(injectAll(store, 'login', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client/signup',
      name: 'signup',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Signup'),
          System.import('containers/Signup/reducer'),
          System.import('containers/Signup/sagas'),
        ]).then(injectAll(store, 'signup', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client/top-up',
      name: 'topUp',
      onEnter: requireAuth(store),
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/TopUp'),
          System.import('containers/TopUp/reducer'),
          System.import('containers/TopUp/sagas'),
        ]).then(injectAll(store, 'topUp', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client/top-up/callback',
      onEnter: requireAuth(store),
      name: 'topUpResult',
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/TopUpResult'),
          System.import('containers/TopUpResult/reducer'),
          System.import('containers/TopUpResult/sagas'),
        ]).then(injectAll(store, 'topUpResult', cb)).catch(errorLoading);
      },
    },
    {
      path: '/client/orders',
      name: 'orders',
      onEnter: requireAuth(store),
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Orders'),
          System.import('containers/Orders/reducer'),
          System.import('containers/Orders/sagas'),
        ]).then(injectAll(store, 'orders', cb)).catch(errorLoading);
      },
      childRoutes: createOrdersRoutes(store),
      indexRoute: {
        name: 'orderList',
        getComponent: (nextState, cb) => {
          System.import('containers/Orders/OrderList')
            .then((component) => cb(null, component.default)).catch(errorLoading);
        },
      },
    },
    {
      path: '/client/account',
      name: 'account',
      onEnter: requireAuth(store),
      getComponent: (nextState, cb) => {
        Promise.all([
          System.import('containers/Account'),
          System.import('containers/Account/reducer'),
          System.import('containers/Account/sagas'),
        ]).then(injectAll(store, 'account', cb)).catch(errorLoading);
      },
      childRoutes: createAccountRoutes(store),
      indexRoute: { onEnter: (nextState, replace) => replace('/client/account/info') },
    },
    {
      path: '*',
      name: 'notFound',
      getComponent(nextState, cb) {
        System.import('components/NotFoundPage')
          .then((component) => cb(null, component.default))
          .catch(errorLoading);
      },
    },
  ];
}

export default function createRoutes(store) {
  return {
    getComponent: (nextState, cb) => {
      Promise.all([
        System.import('containers/App'),
        System.import('containers/App/reducer'),
        System.import('containers/App/sagas'),
      ]).then(injectAll(store, 'global', cb)).catch(errorLoading);
    },
    childRoutes: createClientRoutes(store),
  };
}
