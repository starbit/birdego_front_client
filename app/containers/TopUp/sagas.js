import { take, call, race, put } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { TOP_UP } from './constants';
import request from 'utils/request';
import { topUpSuccess, topUpError } from './actions';

// Bootstrap sagas
export default [
  topUp,
];

// Individual exports for testing
export function* topUp() {
  while (true) {
    const watcher = yield race({
      topUp: take(TOP_UP),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.topUp.formData;
    const requestURL = '/client/paypal-topup';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(topUpSuccess());
      const url = result.data.url;
      window.open(url);
    } else {
      yield put(topUpError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
