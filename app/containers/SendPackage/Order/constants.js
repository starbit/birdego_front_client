/*
 *
 * Order constants
 *
 */

export const CREATE_ORDER = 'Client/SendPackage/Order/CREATE_ORDER';
export const CREATE_ORDER_SUCCESS = 'Client/SendPackage/Order/CREATE_ORDER_SUCCESS';
export const CREATE_ORDER_ERROR = 'Client/SendPackage/Order/CREATE_ORDER_ERROR';

export const OPEN_ADDRESS_LIST = 'Client/SendPackage/Order/OPEN_ADDRESS_LIST';
export const CLOSE_ADDRESS_LIST = 'Client/SendPackage/Order/CLOSE_ADDRESS_LIST';

export const LOAD_ADDRESS_LIST = 'Client/SendPackage/Order/LOAD_ADDRESS_LIST';
export const LOAD_ADDRESS_LIST_SUCCESS = 'Client/SendPackage/Order/LOAD_ADDRESS_LIST_SUCCESS';
export const LOAD_ADDRESS_LIST_ERROR = 'Client/SendPackage/Order/LOAD_ADDRESS_LIST_ERROR';

export const CHOOSE_ADDRESS = 'Client/SendPackage/Order/CHOOSE_ADDRESS';
export const UNCHOOSE_ADDRESS = 'Client/SendPackage/Order/UNCHOOSE_ADDRESS';

export const CREATE_ADDRESS = 'Client/SendPackage/Order/CREATE_ADDRESS';
export const CREATE_ADDRESS_SUCCESS = 'Client/SendPackage/Order/CREATE_ADDRESS_SUCCESS';
export const CREATE_ADDRESS_ERROR = 'Client/SendPackage/Order/CREATE_ADDRESS_ERROR';

export const SAVE_ADDRESS = 'Client/SendPackage/Order/SAVE_ADDRESS';
export const SAVE_ADDRESS_SUCCESS = 'Client/SendPackage/Order/SAVE_ADDRESS_SUCCESS';
export const SAVE_ADDRESS_ERROR = 'Client/SendPackage/Order/SAVE_ADDRESS_ERROR';
