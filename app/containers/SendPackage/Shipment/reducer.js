/*
 *
 * Shipment reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_WAREHOUSES,
  LOAD_WAREHOUSES_SUCCESS,
  LOAD_WAREHOUSES_ERROR,
} from './constants';
import {
  CHANGE_LANGUAGE,
  CHANGE_LANGUAGE_ERROR,
} from 'containers/App/constants';

const initialState = fromJS({
  fromCountryLoading: false,
  warehouses: false,
});

function shipmentReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return state.set('fromCountryLoading', true);
    case CHANGE_LANGUAGE_ERROR:
      return state.set('fromCountryLoading', false);
    case LOAD_WAREHOUSES:
      return state
        .set('fromCountryLoading', true)
        .set('warehouses', false);
    case LOAD_WAREHOUSES_SUCCESS:
      return state
        .set('warehouses', action.warehouses)
        .set('fromCountryLoading', false);
    case LOAD_WAREHOUSES_ERROR:
      return state.set('fromCountryLoading', false);
    default:
      return state;
  }
}

export default shipmentReducer;
