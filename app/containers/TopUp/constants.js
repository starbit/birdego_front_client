/*
 *
 * Top up constants
 *
 */

export const TOP_UP = 'Client/TopUp/TOP_UP';
export const TOP_UP_SUCCESS = 'Client/TopUp/TOP_UP_SUCCESS';
export const TOP_UP_ERROR = 'Client/TopUp/TOP_UP_ERROR';
