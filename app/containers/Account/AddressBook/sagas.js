import { take, call, put, race } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_ADDRESS_LIST, REMOVE_ADDRESS, SAVE_ADDRESS, CREATE_ADDRESS } from './constants';
import {
  loadAddressListSuccess,
  loadAddressListError,
  removeAddressSuccess,
  removeAddressError,
  createAddressSuccess,
  createAddressError,
  saveAddressSuccess,
  saveAddressError,
  loadAddressList,
} from './actions';
import request from 'utils/request';
import { message } from 'antd';
import i18n from 'i18next';

// Bootstrap sagas
export default [
  getAddressListWatcher,
  deleteAddress,
  addAddress,
  updateAddress,
];

export function* getAddressListWatcher() {
  while (true) {
    const watcher = yield race({
      loadAddressList: take(LOAD_ADDRESS_LIST),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) return;

    yield call(getAddressList);
  }
}

export function* getAddressList() {
  const requestURL = '/address-book?limit=-1';

  const result = yield call(request, requestURL);

  if (result.success) {
    yield put(loadAddressListSuccess(result.data.list));
  } else {
    console.log(result.message); // eslint-disable-line no-console
    yield put(loadAddressListError(result.message));
  }
}

export function* deleteAddress() {
  while (true) {
    const watcher = yield race({
      removeAddress: take(REMOVE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const requestURL = `/address-book?id=${watcher.removeAddress.id}`;
    const requestConfig = {
      method: 'DELETE',
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(removeAddressSuccess());
      message.success(i18n.t('Client_delete_address_success'), 3);
      yield put(loadAddressList());
    } else {
      yield put(removeAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* addAddress() {
  while (true) {
    const watcher = yield race({
      createAddress: take(CREATE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.createAddress.formData;
    const requestURL = '/address-book';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(createAddressSuccess(result.data));
      message.success(i18n.t('Client_create_address_success'), 3);
      yield put(loadAddressList());
    } else {
      yield put(createAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}

export function* updateAddress() {
  while (true) {
    const watcher = yield race({
      saveAddress: take(SAVE_ADDRESS),
      stop: take(LOCATION_CHANGE), // stop watching if user leaves page
    });

    if (watcher.stop) break;

    const formData = watcher.saveAddress.formData;
    const requestURL = '/address-book';
    const requestConfig = {
      method: 'POST',
      body: formData,
      feedback: { progress: { mask: true } },
    };

    const result = yield call(request, requestURL, requestConfig);

    if (result.success) {
      yield put(saveAddressSuccess(result.data));
      message.success(i18n.t('Client_update_address_success'), 3);
      yield put(loadAddressList());
    } else {
      yield put(saveAddressError());
      console.log(result.message); // eslint-disable-line no-console
    }
  }
}
