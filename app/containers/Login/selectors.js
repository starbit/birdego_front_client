import { createSelector } from 'reselect';

/**
 * Direct selector to the Login state domain
 */
const selectloginDomain = () => (state) => state.get('login');

/**
 * Other specific selectors
 */

/**
 * Default selector
 */
const selectLogin = () => createSelector(
  selectloginDomain(),
  (substate) => substate.toJS()
);

export default selectLogin;
export {
  selectloginDomain,
};
