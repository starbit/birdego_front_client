/*
 *
 * Info actions
 *
 */

import {
  SAVE_INFO,
} from './constants';

export function saveInfo(formData) {
  return {
    type: SAVE_INFO,
    formData,
  };
}
