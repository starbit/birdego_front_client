/*
 *
 * Quotes reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_QUOTES,
  LOAD_QUOTES_SUCCESS,
  LOAD_QUOTES_ERROR,
} from './constants';

const initialState = fromJS({
  quotes: fromJS([]),
  loading: false,
});

function quotesReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_QUOTES:
      return state
        .set('loading', true)
        .set('quotes', fromJS([]));
    case LOAD_QUOTES_SUCCESS:
      return state
        .set('quotes', fromJS(action.quotes))
        .set('loading', false)
        .set('deliveryService', fromJS(action.quotes.filter(quote => quote.id === state.getIn(['deliveryService', 'id']))[0]) || false);
    case LOAD_QUOTES_ERROR:
      return state.set('loading', false);
    default:
      return state;
  }
}

export default quotesReducer;
