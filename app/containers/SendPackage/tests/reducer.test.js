import expect from 'expect';
import sendPackageReducer from '../reducer';
import { fromJS } from 'immutable';

describe('sendPackageReducer', () => {
  it('returns the initial state', () => {
    expect(sendPackageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
