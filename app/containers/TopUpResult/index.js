/*
 *
 * TopUpResult
 *
 */

import React from 'react';
import { replace } from 'react-router-redux';
import { connect } from 'react-redux';
import styles from './styles.css';
import { translate } from 'react-i18next';
import { selectClientInfo, selectConfig } from 'containers/App/selectors';
import { createStructuredSelector } from 'reselect';
import { Spin, Button } from 'antd';
import { Link } from 'react-router';

export class TopUpResult extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { t } = this.props;
    let result;
    const query = this.props.location.query;
    if (query.success === 'true') {
      result = t('Client_top_up_success');
    } else {
      result = `${t('Client_top_up_fail')}: ${query.message}`;
    }
    replace({ path: '/top-up/callback', query: {} });
    return (
      <div>
        <div className={styles.box}>
          <div className={styles.content}>
            <div className={styles.result}>{result}</div>
            <Spin spinning={!this.props.info}><div className={styles.balance}><span className={styles.label}>{t('Client_balance_now')}:</span> {this.props.config.currencyCode} {this.props.info.balance}</div></Spin>
            <div className={styles.buttonWrapper}><Button type="primary"><Link to="/client/shipment">{t('Client_send_a_package_now')}</Link></Button></div>
          </div>
        </div>
      </div>
    );
  }
}

TopUpResult.propTypes = {
  location: React.PropTypes.object,
  t: React.PropTypes.func,
  info: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  config: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
};

const mapStateToProps = createStructuredSelector({
  info: selectClientInfo(),
  config: selectConfig(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(TopUpResult));
